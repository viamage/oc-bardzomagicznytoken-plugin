<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

class BalanceFill extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:fill-balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Fill Balance';

    /**
     * @var
     */
    protected $config;
    private $wallets;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //$wallets = Wallet::where('name', 'jakub')->get();
        $transactions = Transaction::where('id', '>', 0)->orderBy('created_at', 'desc')->get();
        $total = $transactions->count();
        $this->output->progressStart($total);
        $this->wallets = [];
        /** @var Transaction $transaction */
        foreach ($transactions as $transaction) {
            if (!isset($this->wallets[$transaction->source_id])){
                   $this->wallets[$transaction->source_id] = $transaction->source->balance;
            }
            if (!isset($this->wallets[$transaction->target_id])){
                $this->wallets[$transaction->target_id] = $transaction->target->balance;
            }
            $wallets[$transaction->source_id] = $this->balanceBeforeTransaction($transaction->source, $transaction);
            $transaction->source_balance = $wallets[$transaction->source_id];
            $this->wallets[$transaction->target_id] = $this->balanceBeforeTransaction($transaction->target, $transaction);
            $transaction->target_balance = $this->wallets[$transaction->target_id];
            $transaction->save();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
    }

    private function balanceBeforeTransaction(Wallet $wallet, Transaction $transaction)
    {
        $balance = $this->wallets[$wallet->id];
        if ($transaction->source_id === $wallet->id) {
            $balance += $transaction->value;
        } else {
            $balance -= $transaction->value;
        }
        return $balance;
    }

}
