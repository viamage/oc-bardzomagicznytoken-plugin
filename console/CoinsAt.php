<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

class CoinsAt extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:coins-at {param}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Debug';

    /**
     * @var
     */
    protected $config;


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $total = 0;
        $date = (new Carbon($this->argument('param')))->endOfDay();
        /** @var WalletRepository $repo */
        $wallets = Wallet::where('is_blacklisted', false)->where('created_at', '<=', $date)->where('name', '!=', 'wizard')->get();
        //$wallets = Wallet::where('name', 'jakub')->get();
        foreach ($wallets as $wallet) {
            if($wallet->name === 'wizard') {
                continue;
            }
            $transactions = Transaction::where(function ($q) use ($wallet) {
                $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
            })->where('created_at', '>=', $date)->orderBy('created_at', 'desc')->get();
            foreach ($transactions as $transaction) {
                $wallet->balance = $this->balanceBeforeTransaction($wallet, $transaction);
            }
        }
        dump('Coins at ' . $date);
        dump('-------------------');
        foreach ($wallets as $wallet) {
            $total += $wallet->balance;
            dump($wallet->name . ': ' . $wallet->balance);
        }
        dd('Total: ' . $total);
    }

    private function balanceBeforeTransaction(Wallet $wallet, Transaction $transaction)
    {
        dump('-------------------');
        dump('Transaction ' . $transaction->hash . ' ' . $transaction->created_at . ' from ' . $transaction->source->name . ' to ' . $transaction->target->name);
        dump('Balance before ' . $wallet->balance);
        $balance = $wallet->balance;
        if ($transaction->source_id === $wallet->id) {
            $balance += $transaction->value;
        } else {
            $balance -= $transaction->value;
        }
        dump('Balance after ' . $balance);
        return $balance;
    }

}
