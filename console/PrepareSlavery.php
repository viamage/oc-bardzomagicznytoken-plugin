<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PrepareSlavery extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'bmc:prepare-slavery';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        /** @var WalletRepository $repo */
        $repo = app()->make(WalletRepository::class);
        $wallets = $repo->getWhitelistedWallets();
        $currentDateTime = Carbon::now();
        foreach ($wallets as $wallet) {
            $lastPayout = $wallet->last_payout_at;
            if (!$lastPayout) {
                $lastPayout = $currentDateTime->clone()->subDays(1);
            }
            // Assign a new random payout hour for the next day
            if ((new Carbon($lastPayout))->format('Y-m-d') !== $currentDateTime->format('Y-m-d')) {
                $wallet->payout_hour = random_int(0, 23);
                $wallet->save(); // Assuming $wallet is an Eloquent model
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
