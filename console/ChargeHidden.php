<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\SlackNotifications\Classes\SlackMessageSender;

class ChargeHidden extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:charge-hidden';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Debug';

    /**
     * @var
     */
    protected $config;


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $wallets = Wallet::where('is_hidden', true)->get();
        $wizard = Wallet::where('name', 'wizard')->first();
        /** @var TransactionManager $tm */
        $tm = app()->make(TransactionManager::class);
        /** @var Wallet $wallet */
        foreach ($wallets as $wallet) {
            $income = $wallet->getIncome(true, false);

            if ($wallet->balance >= (int)($income / 4)) {
                $tm->sendToWallet($wallet, $wizard, (int)($income / 4), 'BMC: Charge Hidden');
                BMCGameEvents::notifyWallet('Zabrałem Ci ' . (int)($income / 4) . ' BMC na wyżywienie dla Twoich ludzi.', $wallet->name);
            } else {
                $this->slavesLeaving($wallet);
            }
        }
    }

    private function slavesLeaving(Wallet $wallet)
    {
        if ($wallet->niggers) {
            $left = random_int(1, $wallet->niggers);
            $wallet->niggers -= $left;
            $wallet->save();
            BMCGameEvents::notifyWallet('Nie miałeś czym karmić niewolników i '
                                        . $left
                                        . ' z nich uciekło', $wallet->name);
            return $left;
        }
        if ($wallet->chinese) {
            $left = random_int(1, $wallet->chinese);
            $wallet->chinese -= $left;
            $wallet->save();
            BMCGameEvents::notifyWallet('Nie miałeś czym karmić niewolników i '
                                        . $left
                                        . ' z nich uciekło', $wallet->name);
            return $left;
        }
    }
}
