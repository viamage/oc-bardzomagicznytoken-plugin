<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\SlackNotifications\Classes\SlackMessageSender;

class Debug extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:debug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Debug';

    /**
     * @var
     */
    protected $config;


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Wallet $w */
        $w = Wallet::where('name', 'jakub')->with('buildings')->first();
        dd($w->getAttackPower(), $w->getDefence());
    }
}
