<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\SlaveTransport;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\SlackNotifications\Classes\SlackMessageSender;

/**
 *
 */
class SlaveTransports extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:slave-transports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Debug';

    /**
     * @var
     */
    protected $config;

    /**
     * @var SlaveryManager
     */
    private $sm;

    /**
     * @param SlaveryManager $sm
     */
    public function __construct(SlaveryManager $sm)
    {
        parent::__construct();
        $this->sm = $sm;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $transports = SlaveTransport::where('created_at', '<', Carbon::now()->subMinutes(SlaveryManager::HIRE_CD))->get();
        foreach ($transports as $transport) {
            if ($transport->type === 'chinese') {
                $this->hireChink($transport);
            }
            if ($transport->type === 'niggers') {
                $this->hireNigger($transport);
            }
        }
    }

    /**
     * @param SlaveTransport $transport
     *
     * @return mixed
     */
    private function hireNigger(SlaveTransport $transport): int
    {
        $wallet = $transport->wallet;
        $amount = $transport->getDeliveredAmount();
        $lost = $transport->amount - $amount;
        if ($wallet->niggers === null) {
            $wallet->niggers = $amount;
        } else {
            $wallet->niggers += $amount;
        }
        if (!$wallet->slave_setup) {
            $wallet->syncSlaveSetup();
        }
        $this->sm->adjustSlaveSetup($wallet, $amount, 'add');
        $wallet->save();
        BMCGameEvents::notifyWallet(
            'Dostawa niewolników. Z ' . $transport->amount
            . ' kupionych murzynów, ' . $lost . ' uciekło po drodze.', $wallet->name
        );
        $transport->delete();
        return $wallet->chinese;
    }

    /**
     * @param SlaveTransport $transport
     *
     * @return int
     */
    private function hireChink(SlaveTransport $transport): int
    {
        $wallet = $transport->wallet;
        $amount = $transport->getDeliveredAmount();
        $lost = $transport->amount - $amount;
        if ($wallet->chinese === null) {
            $wallet->chinese = $amount;
        } else {
            $wallet->chinese += $amount;
        }
        if (!$wallet->chinese_setup) {
            $wallet->syncChinkSetup();
        }
        $this->sm->adjustChinkSetup($wallet, $amount, 'add');
        $wallet->save();
        BMCGameEvents::notifyWallet(
            'Dostawa niewolników. Z ' . $transport->amount
            . ' kupionych chińczyków, ' . $lost . ' uciekło po drodze.', $wallet->name
        );
        $transport->delete();
        return $wallet->chinese;
    }
}
