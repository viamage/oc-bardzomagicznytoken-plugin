<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\SlackNotifications\Classes\SlackMessageSender;

class Refund extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:refund {wallet} {amount} {reason}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Refund';

    /**
     * @var
     */
    protected $config;


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $wallet = Wallet::where('name', $this->argument('wallet'))->first();
        $reason = $this->argument('reason');
        $amount = $this->argument('amount');
        $wizard = Wallet::where('name', 'wizard')->first();
        /** @var TransactionManager $tm */
        $tm = app()->make(TransactionManager::class);
        $tm->sendToWallet($wizard, $wallet, $amount, 'BMC: Refund');

        BMCGameEvents::notifyWallet($amount . ' refunded to your wallet. Reason: ' . $reason, $wallet->name);
    }
}
