<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Jobs\SlaveShip;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\Apparatus\Classes\JobManager;
use Keios\SlackNotifications\Classes\SlackMessageSender;

class ChinkShipArrived extends Command
{

    public const SHIP_CHANCE = 5;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:chink-ship-arrived';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Debug';

    /**
     * @var
     */
    protected $config;


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        /** @var WalletRepository $repo */
        $repo = app()->make(WalletRepository::class);
        $totalChinks = $repo->getWhitelistedWallets()->sum('chinese');
        $settings = Settings::instance();
        $maxNiggers = $settings->get('max_chinese', 50);
        if ($totalChinks < $maxNiggers) {
            $this->info('No chinks needed now');
            return;
        }
        $roll = random_int(1, 100);
        \Log::info('Ship roll: ' . $roll);
        if ($roll > self::SHIP_CHANCE) {
            $this->info('No ship arrived today');
            \Log::info('Today ship roll: ' . $roll);
        } else {
            \Log::info('Today ship roll: ' . $roll . ' and its a win! expect ship later');
            /** @var JobManager $jobManager */
            $jobManager = app()->make(JobManager::class);
            $delay = random_int(0, 23) * 60 * 60;
            $now = Carbon::now();
            $shipAt = $now->addSeconds($delay);
            $this->info('Triggered Ship Job - will arrive at ' . $shipAt->toDateTimeString());
            $jobManager->dispatch(new SlaveShip('chinese'), 'Ship Arrived', [], $delay);
        }
    }
}
