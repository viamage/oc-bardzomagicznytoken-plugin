<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Assault;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\MagicAction;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\SlackNotifications\Classes\SlackMessageSender;

/**
 *
 */
class SlaveAway extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:slave-away';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Slave Away';

    /**
     * @var WalletRepository
     */
    protected $walletRepository;
    /**
     * @var
     */
    protected $settings;
    /**
     * @var SlaveryManager
     */
    protected $sm;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->walletRepository = app(WalletRepository::class);
        $this->settings = Settings::instance();
        $this->sm = app()->make(SlaveryManager::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $wallets = $this->walletRepository->getWhitelistedWallets();
        /** @var Wallet $wallet */
        foreach ($wallets as $wallet) {
            if (!$wallet->niggers && !$wallet->chinese) {
                continue;
            }
            $chance = $this->walletChance($wallet);
            if($wallet->niggers) {
                $this->proceedNiggerEscapes($wallet, $chance);
            }
            if($wallet->chinese) {
                $this->proceedChineseDeath($wallet, $chance);
            }
        }
    }

    /**
     * @param Wallet $wallet
     * @param        $chance
     *
     * @throws \Exception
     */
    private function proceedNiggerEscapes(Wallet $wallet, $chance)
    {

        //    return;
        //}
        $roll = random_int(1, 100);
        if ($roll > $chance) {
            \Log::info('Killing ' .$wallet->name . ' nigger. Roll ' . $roll . ' against ' . $chance);
            $this->sm->killNigger($wallet, false);
            BMCGameEvents::notifyWallet('Jeden z twoich murzynów uciekł dziś w nocy.', $wallet->name);
        }
    }

    /**
     * @param Wallet $wallet
     *
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    private function proceedChineseDeath(Wallet $wallet, int $chance)
    {
        $roll = random_int(1, 100);
        if ($roll > $chance) {
            \Log::info('Killing ' .$wallet->name . ' chinese. Roll ' . $roll . ' against ' . $chance);
            $this->sm->killChink($wallet, false);
            BMCGameEvents::notifyWallet('Jeden z twoich żółtków umarł dziś z przepracowania.', $wallet->name);
        }
    }

    /**
     * @param Wallet $wallet
     *
     * @return int
     */
    private function walletChance(Wallet $wallet, int $chance = 80)
    {
        $since = Carbon::now()->subWeek();
        $chance += MagicAction::where(
            'wallet_id',
            $wallet->id
        )->where('created_at', '>', $since)->count();
        $chance += Transaction::where('type', 'bet')->where(function ($q) use ($wallet) {
            $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
        })->where('created_at', $since)->count();
        $chance += Challenge::where('challenger_id', $wallet->id)->where('created_at', '>', $since)->count();
        $chance += DB::table('jz_bardzomagicznycoin_buildings_wallets')->where('wallet_id', $wallet->id)->count();

        return $chance;
    }
}
