<?php namespace JZ\BardzoMagicznyCoin\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use October\Rain\Database\Collection;

class Slavery extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bmc:slavery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BMC Debug';

    /**
     * @var
     */
    protected $config;


    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function handle(): void
    {
        /** @var WalletRepository $repo */
        $repo = app()->make(WalletRepository::class);
        $wallets = $repo->getWhitelistedWallets();
        /** @var SlaveryManager $slaveryManager */
        $slaveryManager = app()->make(SlaveryManager::class);
        $jewsActive = $this->shouldProcessJews($slaveryManager, $wallets);
        $currentDateTime = Carbon::now();
        $payout = (int)$currentDateTime->format('G');
        $this->commentLog('Running slavery on payout ' . $payout);
        // gain from niggers and count gassed jews
        /** @var Wallet $wallet */
        foreach ($wallets as $wallet) {
            if ($this->shouldBeSkipped($wallet)) {
                continue;
            }

            $nincome = $slaveryManager->gainFromNiggers($wallet);
            $bincome = $slaveryManager->gainFromBuildings($wallet);
            $message = $wallet->name . ': '
                       . $nincome . ' z niewolników i ' . $bincome . ' z budynków (Payout hour: '
                       . $wallet->payout_hour . ')';
            $this->infoLog($message);
            if ($jewsActive) {
                sleep(1);
                $jincome = $slaveryManager->gainFromJew($wallet);
                $this->info($wallet->name . ': ' . $jincome . ' from jew');
            }
            $wallet->last_payout_at = Carbon::now();
            $wallet->save();
        }
    }

    private function processHolocaustEnd($wallets, $slaveryManager): void
    {
        $this->info('All jews are dead. No more gain.');
        $settings = Settings::instance();
        $jewPrice = $settings->get('jew_price', 1000);

        foreach ($wallets as $wallet) {
            if ($wallet->has_jew) {
                $slaveryManager->gasJew($wallet, $jewPrice, 1);
            }
        }
        BMCGameEvents::createEvent('jews_dead', 1, 6000000);
        BMCGameEvents::notifyBmcArena(
            ':fireworks: Udało wam się zabić wszystkich żydów na planecie!' . PHP_EOL .
            'Gratulacje! :fireworks: Epizod żydowski został oficjalnie zakończony.' . PHP_EOL .
            ''
        );
    }

    private function wasPaidToday(Wallet $wallet): bool
    {
        $lastPaid = $wallet->last_payout_at;
        $currentDateTime = new Carbon();
        if ($lastPaid) {
            $lastPaid = new Carbon($lastPaid);
            if ($lastPaid->format('Y-m-d') === $currentDateTime->format('Y-m-d')) {
                return true;
            }
        }
        return false;
    }

    private function noWorkerSlaves(Wallet $wallet): bool
    {
        return $wallet->getWorkPower() === 0 && $wallet->buildings->count() === 0;
    }

    private function shouldProcessJews(SlaveryManager $slaveryManager, Collection $wallets): bool
    {
        $totalGassed = $wallets->sum('gassed_jews');
        // check if holocaust ended
        $allJewsDead = BMCGameEvents::areJewsDead();
        if ($allJewsDead) {
            $this->info('All jews are dead. No more gain.');
            return false;
        }

        // check if holocaust ends now
        if ($totalGassed >= env('JEWS_AMOUNT', 12000000)) {
            $this->processHolocaustEnd($wallets, $slaveryManager);
            return false;
        }

        return true;
    }

    private function shouldBeSkipped(Wallet $wallet): bool
    {
        $currentDateTime = Carbon::now();
        if($wallet->is_hidden){
            $this->comment($wallet->name . ' skipped, is hidden.');
            return true;
        }
        if (!$wallet->niggers && !$wallet->chinese && $wallet->buildings->count() === 0) {
            $this->comment($wallet->name . ' skipped, no niggers, chinks and buildings');
            return true;
        }
        if ($this->wasPaidToday($wallet)) {
            $this->comment($wallet->name . ' skipped, was paid today');
            return true;
        }
        if ($this->noWorkerSlaves($wallet)) {
            $this->comment($wallet->name . ' skipped, no worker slaves');
            return true;
        }
        if ((int)$currentDateTime->format('G') !== $wallet->payout_hour) {
            $this->comment($wallet->name . ' skipped, payout hour is ' . $wallet->payout_hour);
            return true;
        }

        return false;
    }

    public function infoLog($message)
    {
        \Log::info($message);
        $this->info($message);
    }

    public function commentLog($message)
    {
        \Log::info($message);
        $this->comment($message);
    }
}
