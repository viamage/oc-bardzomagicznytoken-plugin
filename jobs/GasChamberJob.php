<?php

namespace JZ\BardzoMagicznyCoin\Jobs;

use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Classes\RequestSender;
use Keios\Apparatus\Contracts\ApparatusQueueJob;

/**
 *
 */
class GasChamberJob implements ApparatusQueueJob
{
    /**
     * @var
     */
    private $jobId;
    /**
     * @var
     */
    private $walletId;
    /**
     * @var
     */
    private $amount;
    /**
     * @var SlaveryManager
     */
    private $slaveryManager;

    /**
     * @param int $id
     */
    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }

    /**
     * @param $walletId
     * @param $amount
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct($walletId, $amount)
    {
        $this->walletId = $walletId;
        $this->amount = $amount;
        $this->slaveryManager = app()->make(SlaveryManager::class);
    }

    /**
     * Job handler. This will be done in background.
     *
     * @param JobManager $jobManager
     */
    public function handle(JobManager $jobManager): void
    {
        $wallet = Wallet::find($this->walletId);

        $wallet->save();

        if (!$wallet) {
            $jobManager->failJob($this->jobId, ['error' => 'Portfel nie istnieje.']);
            return;
        }
        // todo move to SlaveryManager
        $settings = Settings::instance();
        $jewPrice = $settings->get('jew_price', 1000);
        $amount = $this->amount;
        $jobManager->startJob($this->jobId, $amount);
        if (!is_numeric($amount) || $amount < 1) {
            $jobManager->failJob($this->jobId, ['error' => 'Nieprawidłowa ilość żydów.']);
            return;
        }
        if ($amount > 50000) {
            $jobManager->failJob($this->jobId, ['error' => 'Za dużo żydów']);
            return;
        }
        $jewsPrice = $jewPrice * $amount;
        if ($wallet->balance < $jewsPrice) {
            $jobManager->failJob($this->jobId, ['error' => 'Nie stać cię na tyle żydów.']);
            return;
        }
        $wallet->balance -= $jewsPrice;
        $wallet->save();

        $this->slaveryManager->gasJew($wallet, $jewPrice, $amount, false);


        $jobManager->completeJob($this->jobId, ['message' => 'Zatruto żydów.', 'amount' => $amount]);
    }
}
