<?php

namespace JZ\BardzoMagicznyCoin\Jobs;

use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Contracts\ApparatusQueueJob;

class SlaveIncome implements ApparatusQueueJob
{

    private $jobId;

    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }

    public function handle(JobManager $jobManager)
    {
        $jobManager->startJob($this->jobId, 1);

        $jobManager->completeJob($this->jobId);
    }
}
