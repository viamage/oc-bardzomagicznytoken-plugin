<?php namespace JZ\BardzoMagicznyCoin\Models;

use Model;
use October\Rain\Database\Relations\BelongsToMany;

/**
 * Building Model
 * @property $id
 * @property $name
 * @property $attack
 * @property $defence
 * @property $income
 * @property $cost
 * @property $min_chinese
 * @property $min_niggers
 * @property $max_amount
 * @property $is_visible
 * @property $requires_slaves
 * @property $wallets
 * @method BelongsToMany wallets()
 */
class Building extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_bardzomagicznycoin_buildings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'wallets' => [
            Wallet::class,
            'table' => 'jz_bardzomagicznycoin_buildings_wallets',
            'pivot' => ['amount', 'is_active'],
            'timestamps' => true,
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
