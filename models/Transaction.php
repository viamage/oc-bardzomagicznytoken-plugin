<?php namespace JZ\BardzoMagicznyCoin\Models;

use Model;

/**
 * Transaction Model
 * @property int    $id
 * @property string $hash
 * @property int    $source_id
 * @property int    $target_id
 * @property int    $value
 * @property string $type
 * @property string $title
 * @property int    $source_balance
 * @property int    $target_balance
 * @property Wallet $source
 * @property Wallet $target
 */
class Transaction extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'jz_bardzomagicznycoin_transactions';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public $belongsTo = [
        'source' => Wallet::class,
        'target' => Wallet::class
    ];
}
