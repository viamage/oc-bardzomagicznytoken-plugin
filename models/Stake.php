<?php namespace JZ\BardzoMagicznyCoin\Models;

use Model;
use October\Rain\Database\Relations\HasMany;

/**
 * Stake Model
 * @property               $label
 * @property array         $options
 * @property               $win_option
 * @property               $is_open
 * @property               $is_finished
 * @property PlayerStake[] $playerStakes
 * @property int           $wallet_id
 * @property Wallet        $wallet
 * @method HasMany playerStakes()
 */
class Stake extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_bardzomagicznycoin_stakes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [
        'is_finished' => 'boolean',
        'is_open'     => 'boolean'
    ];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = ['options'];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'playerStakes' => PlayerStake::class
    ];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'wallet' => Wallet::class
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getWinningOption()
    {
        if ($this->win_option) {
            foreach ($this->options as $option) {
                if ($option['id'] == $this->win_option) {
                    return $option['label'];
                }
            }
        }
    }

    public function wonAmountForWallet(Wallet $wallet): string
    {
        if ($this->is_finished) {
            $playerStakes = $this->playerStakes->where('wallet_id', $wallet->id);
            $won = 0;
            /** @var PlayerStake $playerStake */
            foreach ($playerStakes as $playerStake) {
                if($playerStake->option === $this->win_option){
                    $won += $playerStake->win_amount - $playerStake->amount;
                } else {
                    $won -= $playerStake->amount;
                }
            }
            if($won >= 0){
                return '+'.$won;
            }
            return $won;
        }
    }
}
