<?php namespace JZ\BardzoMagicznyCoin\Models;

use Model;

/**
 * @property        $id
 * @property        $wallet_id
 * @property        $type
 * @property        $amount
 * @property Wallet $wallet
 * SlaveTransport Model
 */
class SlaveTransport extends Model
{
    use \October\Rain\Database\Traits\Validation;

    private $deliveryChances = [100, 75, 50, 30, 20];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_bardzomagicznycoin_slave_transports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'wallet' => Wallet::class
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function getDeliveredAmount(): int
    {
        $delivered = 0;

        for ($i = 0; $i < $this->amount; $i++) {
            $random = random_int(0, 100);

            if ($random <= $this->deliveryChances[$i]) {
                $delivered++;
            }
        }

        return $delivered;
    }
}
