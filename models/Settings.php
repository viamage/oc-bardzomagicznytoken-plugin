<?php namespace JZ\BardzoMagicznyCoin\Models;

use Model;

class Settings extends Model
{

    public $implement = [
        'System.Behaviors.SettingsModel'
    ];

    public $settingsCode = 'jz_bmc_settings';

    public $settingsFields = 'fields.yaml';


    public function processRawCabalState()
    {
        $state = $this->cabal_state;
        $csv = [];
        $include = false;
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $state) as $line) {
            if (str_contains($line, '|Channel')) {
                $include = true;
            }
            if ($include === true && !str_contains($line, '--:')) {
                $csv[] = str_replace('|', ',', ltrim(rtrim($line, '|'), '|'));
            }
        }
        $lineCount = 1;
        $result = [];
        foreach ($csv as $csvLine) {
            if ($lineCount === 1) {
                $headers = explode(',', $csvLine);
            } else {
                $data = explode(',', $csvLine);
                $entry = [];
                foreach ($data as $key => $value) {
                    $entry[$headers[$key]] = $value;
                }
                $result[] = $entry;
            }
            ++$lineCount;
        }
        $this->cabal_state_json = json_encode($result);

    }
}
