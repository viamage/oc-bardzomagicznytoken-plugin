<?php namespace JZ\BardzoMagicznyCoin\Models;

use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use Model;
use October\Rain\Database\Relations\BelongsTo;
use October\Rain\Database\Relations\BelongsToMany;

/**
 * Assault Model
 * @property          $id
 * @property          $target_id
 * @property          $boss_id
 * @property          $is_finished
 * @property          $result
 * @property          $is_public
 * @property Wallet[] $wallets
 * @property Wallet[] $invited
 * @property Wallet   $target
 * @property Wallet   $boss
 * @method BelongsToMany wallets()
 * @method BelongsTo target()
 */
class Assault extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_bardzomagicznycoin_assaults';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'boss'   => Wallet::class,
        'target' => Wallet::class
    ];
    public $belongsToMany = [
        'wallets' => [
            Wallet::class,
            'table' => 'jz_bardzomagicznycoin_assaults_teams',
            'pivot' => ['value', 'strength']
        ],
        'invited' => [
            Wallet::class,
            'table'    => 'jz_bardzomagicznycoin_assaults_invited',
            'key'      => 'assault_id',
            'otherKey' => 'wallet_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getTotalValue()
    {
        $value = 0;
        foreach ($this->wallets()->with('buildings')->withPivot('value')->get() as $wallet) {
            $value += $wallet->pivot->value;
        }
        return $value;
    }

    public function getChances()
    {
        $chances = 0;
        /** @var Wallet $wallet */
        foreach ($this->wallets()->with('buildings')->withPivot('strength')->get() as $wallet) {
            if ($wallet->id === $this->boss_id) {
                $chances += $wallet->pivot->strength + $wallet->getAttackPower();
            } else {
                $chances += $wallet->pivot->strength + ($wallet->getAttackPower() * 0.5);
            }
        }
        return $chances - $this->calculateProtection();
    }

    public function getMyReward(Wallet $wallet)
    {
        $w = $this->wallets()->with('buildings')->withPivot('value')->where('id', $wallet->id)->first();
        if ($w) {
            return $w->pivot->value;
        }
    }

    public function getTeamString()
    {
        return implode(', ', $this->wallets->pluck('name', 'id')->toArray());
    }

    /**
     * @param Assault $assault
     *
     * @return int
     */
    private function calculateProtection(): int
    {
        return $this->target->getDefence();
    }

    public function getAttackDetails()
    {
        $details = [];
        /** @var Wallet $wallet */
        foreach ($this->wallets()->with('buildings')->withPivot('strength')->get() as $wallet) {
            if($wallet->id === $this->boss_id){
                $power = $wallet->getAttackPower();
            } else {
                $power = $wallet->getAttackPower() * 0.5;
            }
            $details[] = [
                'name'     => $wallet->name,
                'power'    => $wallet->pivot->strength + $power
            ];
        }
        return $details;
    }

}
