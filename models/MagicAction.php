<?php namespace JZ\BardzoMagicznyCoin\Models;

use Model;
use October\Rain\Database\Relations\BelongsTo;

/**
 * MagicAction Model
 * @property        $wallet_id
 * @property Wallet $wallet
 * @property        $created_at
 * @property        $updated_at
 * @method BelongsTo wallet
 */
class MagicAction extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'jz_bardzomagicznycoin_magic_actions';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array hasOne and other relations
     */
    public $belongsTo = [
        'wallet' => Wallet::class
    ];
}
