<?php namespace JZ\BardzoMagicznyCoin\Models;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Support\Facades\DB;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use Model;
use October\Rain\Database\Collection;
use October\Rain\Database\Relations\BelongsToMany;
use October\Rain\Database\Relations\HasMany;
use October\Rain\Exception\ApplicationException;
use System\Helpers\DateTime;

/**
 * Wallet Model
 * @property int                         $id
 * @property string                      $name
 * @property string                      $code
 * @property string                      $hash
 * @property int                         $balance
 * @property boolean                     $is_blacklisted
 * @property boolean                     $is_subscribed
 * @property boolean                     $is_active
 * @property boolean                     $is_bot
 * @property boolean                     $has_jew
 * @property int                         $niggers
 * @property int                         $chinese
 * @property string                      $ui_code
 * @property int                         $gassed_jews
 * @property int                         $niggers_mode @depreciated
 * @property DateTime                    $last_payout_at
 * @property int                         $payout_hour
 * @property bool                        $is_hidden
 * @property Carbon                      $hidden_at
 * @property array                       $slave_setup
 * @property array                       $chinese_setup
 * @property Carbon                      $hired_at
 * @property Wallet|null                 $parent
 * @property Wallet[]                    $subaccounts
 * @property Collection|Building[]       $buildings
 * @property Collection|Transaction[]    $outgoingTransactions
 * @property Collection|Transaction[]    $incomingTransactions
 * @property Collection|SlaveTransport[] $slaveTransports
 * @method BelongsToMany buildings
 * @method HasMany outgoingTransactions
 * @method HasMany incomingTransactions
 */
class Wallet extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'jz_bardzomagicznycoin_wallets';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = ['slave_setup', 'chinese_setup'];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'hidden_at',
        'hired_at'
    ];

    public $belongsTo = [
        'parent' => Wallet::class
    ];

    public $belongsToMany = [
        'events'    => [
            Event::class,
            'table' => 'jz_bardzomagicznycoin_events_wallets'
        ],
        'buildings' => [
            Building::class,
            'table' => 'jz_bardzomagicznycoin_buildings_wallets',
            'pivot' => ['amount', 'is_active']
        ]
    ];

    public $hasOne = [
        'safe' => Safe::class
    ];

    public $hasMany = [
        'subaccounts'          => [
            Wallet::class,
            'key' => 'parent_id'
        ],
        'outgoingTransactions' =>
            [
                Transaction::class,
                'key' => 'source_id'
            ],
        'incomingTransactions' =>
            [
                Transaction::class,
                'key' => 'target_id'
            ],
        'magicActions'         =>
            [
                MagicAction::class,
                'key' => 'wallet_id'
            ],
        'slaveTransports'      => [
            SlaveTransport::class,
            'key' => 'wallet_id'
        ]
    ];

    public function getAllTransactions()
    {
        return Transaction::where('source_id', $this->id)->orWhere('target_id', $this->id)->orderBy('id', 'asc')->get();
    }

    public function beforeSave()
    {
        if (!$this->last_payout_at) {
            $this->last_payout_at = null;
        }
        $this->validateSlaveSetup();
    }

    public function afterCreate()
    {
        $hashids = new Hashids(env('APP_KEY'), 24);
        $this->hash = $hashids->encode($this->id + 1);
        $this->save();
    }

    public function getIncome(bool $totalPossible = true, bool $includeBuildings = true)
    {
        $settings = Settings::instance();
        $niggerIncome = $settings->get('nigger_income', 5);
        $bincome = 0;
        $cincome = 0;
        if ($totalPossible) {
            $nincome = $niggerIncome * $this->niggers;
            $cincome = ($niggerIncome * 2) * $this->chinese;
        } else {
            $nincome = $niggerIncome * $this->getWorkPower();
        }
        if ($includeBuildings) {
            $bincome = $this->getIncomeFromBuildings();
        }
        $jincome = 0;
        if ($this->has_jew) {
            $jewIncome = $this->calculateJewIncome();
            $jincome = floor(($this->balance + $nincome + $bincome) * ($jewIncome / 100));
        }

        return $jincome + $nincome + $bincome + $cincome;
    }

    public function getWorkPower()
    {
        if (!$this->slave_setup) {
            $this->syncSlaveSetup();
        }
        if (!$this->chinese_setup) {
            $this->syncChinkSetup();
        }
        $wp = 0;
        foreach ($this->slave_setup as $entry) {
            if ($entry["mode"] == SlaveryManager::NIGGER_WORK) {
                $wp += $entry['amount'];
            }
        }
        foreach ($this->chinese_setup as $centry) {
            if ($centry["mode"] == SlaveryManager::CHINESE_WORK) {
                $wp += $centry['amount'] * 2;
            }
        }

        return $wp;
    }

    public function getAttackPower()
    {
        if (!$this->slave_setup) {
            $this->syncSlaveSetup();
        }
        $ap = 0;
        foreach ($this->slave_setup as $entry) {
            if ($entry["mode"] == SlaveryManager::NIGGER_ATTACK) {
                $ap += $entry['amount'];
            }
        }
        $mp = $this->getMaintenancePower();
        /** @var Building $building */
        foreach ($this->buildings->sortByDesc('min_chinese') as $building) {
            if (!$building->pivot->is_active) {
//#dump('xxxxx ' . $building->name);
                continue;
            }
//dump($building->name . ' ' . $building->attack);
            $mp -= $building->min_chinese;
            if ($mp >= 0 && $building->attack) {
                $ap += $building->attack;
            }
        }

        // Return null if no matching entry is found
        return $ap;
    }

    public function getDefence()
    {
        if (!$this->slave_setup) {
            $this->syncSlaveSetup();
        }
        $def = 0;
        foreach ($this->slave_setup as $entry) {
//dump($entry);
            if ($entry["mode"] == SlaveryManager::NIGGERS_DEFEND) {
//dump('Niggers: ' . $entry['amount']);
                $def += $entry['amount'];
            }
        }
//dump($def);
        $mp = $this->getMaintenancePower();
//dump('Total MP: '. $mp);
        /** @var Building $building */
        foreach ($this->buildings->sortByDesc('min_chinese')->sortBy('name') as $building) {
            if (!$building->pivot->is_active) {
                continue;
            }
//dump($building->name . ', cost: ' . $building->min_chinese);
//dump($building->pivot->toArray());

            $mp -= $building->min_chinese;
//dump($mp);
//dump('-----');
            if ($mp >= 0 && $building->defence) {
                $def += $building->defence;
            }
        }
        if ($this->is_hidden) {
            $def += 50;
        }

        // Return null if no matching entry is found
        return $def;
    }

    public function getMaintenancePower()
    {
        if (!$this->chinese_setup) {
            $this->syncChinkSetup();
        }
        foreach ($this->chinese_setup as $entry) {
            if ($entry["mode"] == SlaveryManager::CHINESE_MAINTAIN) {
                return $entry['amount'];
            }
        }

        // Return null if no matching entry is found
        return 0;
    }

    public function getAvailableMaintenancePower()
    {
        $mp = $this->getMaintenancePower();
        foreach ($this->buildings()->wherePivot('is_active', true)->get() as $building) {
            $mp -= $building->min_chinese;
        }
        return $mp;
    }

    public function getStaticIncome()
    {
        $settings = Settings::instance();
        $niggerIncome = $settings->get('nigger_income', 5);
        $sincome = $niggerIncome * $this->getWorkPower();
        $bincome = $this->getIncomeFromBuildings();
        if ($this->has_jew) {
            $jewIncome = $this->calculateJewIncome();
            $jincome = floor(($this->balance + $sincome + $bincome) * ($jewIncome / 100));
        } else {
            $jincome = 0;
        }
        $max = $this->getIncome(true, true);
        return [
            'jew'       => $jincome,
            'slaves'    => $sincome,
            'buildings' => $bincome,
            'max'       => $max,
            'total'     => $jincome + $sincome + $bincome
        ];
    }

    public function checkRanking()
    {
        $wallets = Wallet::orderBy('balance', 'desc')
                         ->where('is_bot', false)
                         ->where('is_blacklisted', false)
                         ->where('name', '!=', 'wizard')
                         ->pluck('id')->toArray();
        $rank = array_search($this->id, $wallets);

        // array_search() returns false if the wallet ID is not found
        // Also, array_search() is zero-based, so we add 1 to the rank.
        return $rank !== false ? $rank + 1 : null;
    }

    public function getSubaccountsTotalBalance()
    {
        $total = 0;
        foreach ($this->subaccounts as $account) {
            $total += $account->balance;
        }
        return $total;
    }

    public function isBuildingActive(Building $requestedBuilding): bool
    {
        $mp = $this->getMaintenancePower();
        $buildings = $this->buildings()->wherePivot('is_active', true)->orderBy('min_chinese', 'desc')->get();
        foreach ($buildings as $building) {
            $mp -= $building->min_chinese;
            if ($building->id === $requestedBuilding->id) {
                return $mp >= 0;
            }
        }
        return false;
    }

    public function syncSlaveSetup()
    {
        if ($this->niggers_mode === SlaveryManager::NIGGER_ATTACK) {
            $setup = [
                ['mode' => SlaveryManager::NIGGER_ATTACK, 'amount' => $this->niggers]
            ];
        } else {
            if ($this->niggers_mode === SlaveryManager::NIGGERS_DEFEND) {
                $setup = [
                    ['mode' => SlaveryManager::NIGGERS_DEFEND, 'amount' => $this->niggers]
                ];
            } else {
                $setup = [
                    ['mode' => SlaveryManager::NIGGER_WORK, 'amount' => $this->niggers]
                ];
            }
        }
        $this->slave_setup = $setup;
    }

    public function calculateJewReturnPrice()
    {
        $settings = Settings::instance();
        $price = $settings->get('jew_price', 1000);
        $gassed = (int)$this->gassed_jews;
        foreach (SlaveryManager::JEW_REFUND as $amount => $percent) {
            if ($gassed > $amount) {
                continue;
            }
            return $price * $percent;
        }
    }

    public function calculateJewIncome()
    {
        $settings = Settings::instance();
        $jewIncome = $settings->get('jew_income', 2);
        $gassed = (int)$this->gassed_jews;
        foreach (SlaveryManager::GAS_JEW_BONUS_Q as $amount => $percent) {
            if ($gassed > $amount) {
                continue;
            }
            return $jewIncome + ($gassed * $percent);
        }
        return 0;
    }

    public function syncChinkSetup(): void
    {
        if (!$this->chinese_setup) {
            $setup = [
                ['mode' => SlaveryManager::CHINESE_WORK, 'amount' => $this->chinese]
            ];
            $this->chinese_setup = $setup;
        }
    }


    private function validateSlaveSetup(): void
    {
        if (!$this->slave_setup) {
            $this->syncSlaveSetup();
        }
        // Array to hold mode values
        $modes = [];

        // Variable to hold the sum of amounts
        $sum = 0;

        // Iterate over the array
        foreach ($this->slave_setup as $subArray) {
            // Check if the mode value is already in the $modes array
            if (in_array($subArray["mode"], $modes, true)) {
                throw new ApplicationException('Duplicated mode');
            }

            // If no, add the mode value to the $modes array
            $modes[] = $subArray["mode"];

            // Add the amount value to the sum
            $sum += $subArray["amount"];
        }

        // After the loop, check if the sum is greater than $max
        if ($sum > $this->niggers) {
            throw new ApplicationException('Too many niggers in slave setup, max is ' . $this->niggers);
        }
    }


    private function validateChineseSetup(): void
    {
        if (!$this->chinese_setup) {
            $this->syncChinkSetup();
        }
        // Array to hold mode values
        $modes = [];

        // Variable to hold the sum of amounts
        $sum = 0;

        // Iterate over the array
        foreach ($this->chinese_setup as $subArray) {
            // Check if the mode value is already in the $modes array
            if (in_array($subArray["mode"], $modes, true)) {
                throw new ApplicationException('Duplicated mode');
            }

            // If no, add the mode value to the $modes array
            $modes[] = $subArray["mode"];

            // Add the amount value to the sum
            $sum += $subArray["amount"];
        }

        // After the loop, check if the sum is greater than $max
        if ($sum > $this->chinese_setup) {
            throw new ApplicationException('Too many chinks in chink setup, max is ' . $this->chinese);
        }
    }

    private function getIncomeFromBuildings()
    {
        $activeBuildings = $this->buildings()->wherePivot('is_active', true)->get();
        return $activeBuildings->sum('income');
    }
}
