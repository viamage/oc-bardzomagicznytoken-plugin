<?php

use JZ\BardzoMagicznyCoin\Controllers\Api\AssaultsController;
use JZ\BardzoMagicznyCoin\Controllers\Api\BuildingController;
use JZ\BardzoMagicznyCoin\Controllers\Api\SlaveController;
use JZ\BardzoMagicznyCoin\Controllers\Api\StakeController;
use JZ\BardzoMagicznyCoin\Controllers\ApiController;
use Illuminate\Routing\Middleware\ThrottleRequests;

Route::group(
    ['prefix' => '/_bmc/api/v1', 'middleware' => ['api', \JZ\BardzoMagicznyCoin\Middleware\BMCApiAuth::class]],
    static function () {
        Route::options(
            '{level1?}/{level2?}/{level3?}',
            static function () {
                return Response::make('', 204);
            }
        );
        Route::post(
            'cata',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->makeCataclysm();
            }
        );
        Route::post(
            'subscribe',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->subscribe();
            }
        );
        Route::get(
            'total-coins',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->totalCoins();
            }
        );
        Route::post(
            'transaction',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->createTransaction();
            }
        );
        Route::get(
            'dfp',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getRandomDeepFocusEntry();
            }
        );
        Route::post(
            'dfp',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->addToDeepFocusPlaylist();
            }
        );
        Route::post(
            'pay',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->createTransaction('payment');
            }
        );
        Route::post(
            'cabal-state',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->updateCabalState();
            }
        );
        Route::post(
            'event',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->processEvent();
            }
        );
        Route::get(
            'event',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->checkEvent();
            }
        );
        Route::post(
            'bet',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->bet();
            }
        );
        Route::post(
            'alms',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->alms();
            }
        );
        Route::post(
            'challenge',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->challenge();
            }
        );
        Route::get(
            'challenges',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getChallenges();
            }
        );
        Route::post(
            'challenge-reject',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->rejectChallenge();
            }
        );
        Route::post(
            'challenge-accept',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->acceptChallenge();
            }
        );
        Route::post(
            'challenge-cancel',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->cancelChallenge();
            }
        );
        Route::get(
            'bet-balance',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->betBalance();
            }
        );
        Route::get(
            'bet-ranking',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getBetRanking();
            }
        );
        Route::get(
            'challenge-ranking',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getChallengeRanking();
            }
        );
        Route::post(
            'poczaruj',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->poczaruj();
            }
        );
        Route::post(
            'create-wallet',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->createWallet();
            }
        );
        Route::post(
            'ui-code',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->generateUiCode();
            }
        );
        Route::get(
            'transactions',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getTransactions();
            }
        );
        Route::get(
            'balance',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getBalance();
            }
        );
        Route::get(
            'ranking',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getRanking();
            }
        );
        Route::get(
            'transaction',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->getTransaction();
            }
        );
        Route::get(
            'undo-transaction',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->undoTransaction();
            }
        );
        Route::post(
            'hire-jew',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->hireJew();
            }
        );
        Route::post(
            'gas-jew',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->gasJew();
            }
        );
        Route::post(
            'gas-jews',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->multiGasJew();
            }
        );

        Route::post(
            'hide',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->hide();
            }
        );

        Route::post(
            'unhide',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->unhide();
            }
        );

        Route::post(
            'hire-chink',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->hireChink();
            }
        )->middleware(ThrottleRequests::class . ':60,1');
        Route::post(
            'buy-chink',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->buyChink();
            }
        );
        Route::post(
            'sell-chink',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->sellChink();
            }
        );
        Route::post(
            'cancel-sell-chink',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->cancelSellSlave();
            }
        );
        Route::post(
            'kill-chink',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->killChink();
            }
        );


        Route::post(
            'hire-nigger',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->hireNigger();
            }
        );
        Route::post(
            'buy-nigger',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->buyNigger();
            }
        );
        Route::post(
            'sell-nigger',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->sellNigger();
            }
        );
        Route::post(
            'cancel-sell-nigger',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->cancelSellSlave();
            }
        );
        Route::post(
            'slave-trade',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->slaveTrade();
            }
        );
        Route::post(
            'slave-trades',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->slaveTrades();
            }
        );
        Route::post(
            'kill-nigger',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->killNigger();
            }
        );
        Route::post(
            'responsible-disclosure',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->responsibleDisclosure();
            }
        );
        Route::post(
            'slaves-status',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->slaveStatus();
            }
        );
        Route::post(
            'slaves-ranking',
            static function () {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->slavesRanking();
            }
        );

        Route::post(
            'create-stake',
            static function () {
                /** @var StakeController $apiController */
                $apiController = app()->make(StakeController::class);
                return $apiController->createStake();
            }
        );

        Route::get(
            'stakes',
            static function () {
                /** @var StakeController $apiController */
                $apiController = app()->make(StakeController::class);
                return $apiController->getActiveStakes();
            }
        );

        Route::post(
            'close-stake',
            static function () {
                /** @var StakeController $apiController */
                $apiController = app()->make(StakeController::class);
                return $apiController->closeStake();
            }
        );

        Route::post(
            'finish-stake',
            static function () {
                /** @var StakeController $apiController */
                $apiController = app()->make(StakeController::class);
                return $apiController->finishStake();
            }
        );

        Route::post(
            'vote-stake',
            static function () {
                /** @var StakeController $apiController */
                $apiController = app()->make(StakeController::class);
                return $apiController->voteStake();
            }
        );

        Route::get(
            'stake',
            static function () {
                /** @var StakeController $apiController */
                $apiController = app()->make(StakeController::class);
                return $apiController->getStakeRates();
            }
        );

        Route::post('nigger-mode', static function () {
            try {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                $apiController->setNiggerMode();
                return response()->json(['message' => 'Ustawiono wszystkich murzynów w tryb ' . request()->get('mode')]);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });

        Route::post('chink-setup', static function () {
            try {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->setChinkSetup();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });


        Route::post('nigger-setup', static function () {
            try {
                /** @var SlaveController $apiController */
                $apiController = app()->make(SlaveController::class);
                return $apiController->setSlaveSetup();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });

        Route::post('create-private-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->createAssault(false);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });

        Route::post('create-public-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->createAssault(true);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('cancel-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->cancelAssault();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('join-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->joinAssault();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('invite-to-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->inviteToAssault();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('leave-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->leaveAssault();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('switch-target', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->switchTarget();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('commence-assault', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->commenceAssault();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::get('assault-status', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->assaultStatus();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::get('my-assaults', static function () {
            try {
                /** @var AssaultsController $apiController */
                $apiController = app()->make(AssaultsController::class);
                return $apiController->myAssaults();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::get('my-buildings', static function () {
            try {
                /** @var BuildingController $apiController */
                $apiController = app()->make(BuildingController::class);
                return $apiController->myBuildings();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::get('buildings', static function () {
            try {
                /** @var BuildingController $apiController */
                $apiController = app()->make(BuildingController::class);
                return $apiController->listAvailableBuildings();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('buy-building', static function () {
            try {
                /** @var BuildingController $apiController */
                $apiController = app()->make(BuildingController::class);
                return $apiController->buyBuilding();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('activate-building', static function () {
            try {
                /** @var BuildingController $apiController */
                $apiController = app()->make(BuildingController::class);
                return $apiController->activateBuilding();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });
        Route::post('deactivate-building', static function () {
            try {
                /** @var BuildingController $apiController */
                $apiController = app()->make(BuildingController::class);
                return $apiController->deactivateBuilding();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        });

        Route::get(
            'parse-help',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->parseHelp();
            }
        );
        Route::get(
            'parse-bmc-help',
            static function () {
                /** @var ApiController $apiController */
                $apiController = app()->make(ApiController::class);
                return $apiController->parseBMCHelp();
            }
        );
    }
);
