<?php

namespace JZ\BardzoMagicznyCoin\ValueObjects;

class UIBuilding
{
    public $id;
    public $name;
    public $attack;
    public $defence;
    public $income;
    public $mp;
    public $active = false;
    public $pivot_active = false;
}
