<?php

namespace JZ\BardzoMagicznyCoin\ValueObjects;

use JZ\BardzoMagicznyCoin\Models\Transaction;

/**
 *
 */
class BetResult
{
    /**
     * @var bool
     */
    public $victory = false;
    /**
     * @var Transaction
     */
    public $transaction;

    /**
     * @var int
     */
    public $balanceAfter;

    /**
     * @param Transaction $transaction
     * @param bool        $victory
     * @param int         $balanceAfter
     */
    public function __construct(Transaction $transaction, bool $victory, int $balanceAfter)
    {
        $this->victory = $victory;
        $this->transaction = $transaction;
        $this->balanceAfter = $balanceAfter;
    }
}
