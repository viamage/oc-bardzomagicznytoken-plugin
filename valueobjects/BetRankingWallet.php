<?php

namespace JZ\BardzoMagicznyCoin\ValueObjects;

class BetRankingWallet
{
    public $name;
    public $lost = 0;
    public $won = 0;
    public $total = 0;
    public $games = 0;
    public $wonGames = 0;
    public $lostGames = 0;
}
