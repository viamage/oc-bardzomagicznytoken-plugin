<?php

namespace JZ\BardzoMagicznyCoin\Controllers\Api;

use Illuminate\Http\JsonResponse;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Building;

class BuildingController
{
    private $walletRepository;
    private $transactionManager;

    public function __construct(WalletRepository $walletRepository, TransactionManager $transactionManager)
    {
        $this->walletRepository = $walletRepository;
        $this->transactionManager = $transactionManager;
    }

    public function listAvailableBuildings(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $buildings = Building::where('is_visible', true)->get();
        $message = '|ID|Budynek|AP|DP|Przychód|Koszt|Min. MP|' . PHP_EOL;
        $message .= '|---|---|---|---|---|---|---|---|' . PHP_EOL;
        foreach ($buildings as $building) {
            $message .= '|' . $building->id .
                        '|' . $building->name .
                        '|' . $building->attack .
                        '|' . $building->defence .
                        '|' . $building->income .
                        '|' . $building->cost .
                        '|' . $building->min_chinese . '|' . PHP_EOL;
        }
        $message .= PHP_EOL . 'Twoje środki: ' . $wallet->balance . ' BMC' . PHP_EOL;
        $message .= 'Twoje MP: ' . $wallet->getMaintenancePower() . PHP_EOL;
        $message .= 'Twoje Dostępne MP: ' . $wallet->getAvailableMaintenancePower() . PHP_EOL;

        return response()->json(['message' => $message]);
    }

    public function buyBuilding(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            $wizard = $this->walletRepository->getWalletForName('wizard');
            if ($wallet->is_hidden) {
                BMCGameEvents::hiddenStatusRemoved($wallet);
            }
            $buildingId = request()->get('id');
            /** @var Building $building */
            $building = Building::find($buildingId);
            if (!$building) {
                throw new \Exception('Nie ma budynku z ID ' . $buildingId . '.');
            }
            if ($building->cost > $wallet->balance) {
                throw new \Exception('Nie stać cię.');
            }
            $alreadyBought = $wallet->buildings->where('name', $building->name)->count();
            if($alreadyBought >= $building->max_amount) {
                throw new \Exception('Nie możesz mieć więcej niż ' . $building->max_amount . ' budynków tego typu');
            }
            $this->transactionManager->sendToWallet(
                $wallet,
                $wizard,
                $building->cost,
                'transfer',
                'Zakup budynku ' . $building->name
            );
            $building->wallets()->attach($wallet);
            return response()->json(['message' => 'Pomyślnie zakupiono ' . $building->name . '.']);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function destroyBuilding(): JsonResponse
    {
    }

    public function deactivateBuilding(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $building_id = request()->get('building_id'); // assuming building id is provided in request
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        $building = $wallet->buildings()->find($building_id);
        if ($building) {
            $building->pivot->is_active = false;
            $building->pivot->save();
        }

        return response()->json(['message' => 'Pomyślnie dezaktywowano budynek.']);
    }

    public function activateBuilding(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $building_id = request()->get('building_id'); // assuming building id is provided in request
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        $building = $wallet->buildings()->find($building_id);
        if ($building) {
            $building->pivot->is_active = true;
            $building->pivot->save();
        }

        return response()->json(['message' => 'Pomyślnie aktywowano budynek.']);
    }

    public function myBuildings(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $buildings = $wallet->buildings;
        $message = '|ID|Budynek|AP|DP|Przychód|Koszt|Min. MP|Aktywny|' . PHP_EOL;
        $message .= '|---|---|---|---|---|---|---|---|---|' . PHP_EOL;
        $mp = $wallet->getMaintenancePower();
        $totalMP = $mp;
        foreach ($buildings as $building) {
            if($wallet->isBuildingActive($building)) {
                $mp -= $building->min_chinese;
                $activeString = 'Tak';
            } else {
                $activeString = 'Nie';
            }
            $message .= '|' . $building->id .
                        '|' . $building->name .
                        '|' . $building->attack .
                        '|' . $building->defence .
                        '|' . $building->income .
                        '|' . $building->cost .
                        '|' . $building->min_chinese .
                        '|' . $activeString . '|' .
                        PHP_EOL;
        }
        $message .= PHP_EOL . 'Twoje środki: ' . $wallet->balance . ' BMC' . PHP_EOL;
        $message .= 'Twoje MP: ' . $totalMP . PHP_EOL;
        $message .= 'Dostępne MP: ' . $mp . PHP_EOL;

        return response()->json(['message' => $message]);
    }
}
