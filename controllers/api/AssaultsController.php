<?php

namespace JZ\BardzoMagicznyCoin\Controllers\Api;

use Hashids\Hashids;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use JZ\BardzoMagicznyCoin\Classes\AssaultManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Assault;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Pheanstalk\Exception;

/**
 *
 */
class AssaultsController
{
    /**
     *
     */
    /**
     * @var WalletRepository
     */
    private $walletRepository;
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var AssaultManager
     */
    private $assaultManager;

    /**
     * @param WalletRepository      $walletRepository
     * @param TransactionRepository $transactionRepository
     * @param TransactionManager    $transactionManager
     */
    public function __construct(
        WalletRepository      $walletRepository,
        TransactionRepository $transactionRepository,
        TransactionManager    $transactionManager,
        AssaultManager        $assaultManager
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->transactionManager = $transactionManager;
        $this->walletRepository = $walletRepository;
        $this->assaultManager = $assaultManager;
    }

    /**
     * @param bool $public
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function createAssault(bool $public = true): JsonResponse
    {
        $this->validateCreateAssaultRequest();
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $target = $this->walletRepository->getWalletForName(request()->get('target'));
        $amount = request()->get('amount');
        if ($amount === 'all') {
            $amount = $wallet->balance - 1;
        }
        $assault = $this->assaultManager->createAssault($wallet, $target, $amount, $public);
        $message = 'Napad zaplanowany! ' . $wallet->name . ' przygotowuje napad na ' . $target->name . '!' . PHP_EOL;
        if (!$public) {
            $message .= 'Aby zaprosić do napadu użyj `wizard invite-assault '
                        . $assault->code . ' "portfel,portfel2"`' . PHP_EOL;
        }
        $message .= 'Aby dołączyć, użyj `wizard join-assault ' . $assault->code . ' <kwota>`' . PHP_EOL;
        $message .= 'Aby rozpocząć napad, ' . $wallet->name . ' musi użyć `wizard commence-assault '
                    . $assault->code . '`';
        return response()->json(['message' => $message], 201);
    }

    public function myAssaults(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet || $wallet->is_bot) {
                throw new \Exception('Coś nie gra z tym portfelem');
            }
            /** @var Assault[] $assaults */
            $assaults = Assault::where('is_finished', false)
                               ->whereHas('wallets', function ($query) use ($wallet) {
                                   $query->where('wallet_id', $wallet->id);
                               })
                               ->get();
            $possibleAssaults = Assault::where(function ($q) use ($wallet) {
                $q->whereHas('invited', function ($q) use ($wallet) {
                    $q->where('id', $wallet->id);
                })->orWhere('is_public', true);
            })->whereDoesntHave('wallets', function ($q) use ($wallet) {
                $q->where('id', $wallet->id);
            })->where('is_finished', false)->whereHas('wallets')->get();

            $message = '| Kod | Boss | Cel | Stawka | Typy | Szanse |' . PHP_EOL;
            $message .= '| --- | --- | --- | --- | --- | --- |' . PHP_EOL;
            foreach ($assaults as $assault) {
                $message .= '| ' . $assault->code . ' | ' . $assault->boss->name
                            . ' | ' . $assault->target->name . ' | '
                            . $assault->wallets->sum('pivot.value') . ' | '
                            . $assault->wallets->count() . ' | ' . $this->calculateChances($assault)
                            . '%' . ' |' . PHP_EOL;
            }
            if ($possibleAssaults->count() > 0) {
                $message .= PHP_EOL;
                $message .= '---' . PHP_EOL;
                $message .= PHP_EOL;
                $message .= 'Napady do których możesz dołączyć: ' . PHP_EOL . PHP_EOL;
                $message .= '| Kod | Boss | Cel | Stawka | Typy | Szanse |' . PHP_EOL;
                $message .= '| --- | --- | --- | --- | --- | --- |' . PHP_EOL;
                foreach ($possibleAssaults as $assault) {
                    $message .= '| ' . $assault->code . ' | ' . $assault->boss->name . ' | ' . $assault->target->name . ' | ' . $assault->wallets->sum('pivot.value') . ' | ' . $assault->wallets->count() . ' | ' . $this->calculateChances($assault) . '%' . ' |' . PHP_EOL;
                }
            }
            return response()->json(['message' => $message], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function assaultStatus(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        if (!$wallet || $wallet->is_bot) {
            throw new \Exception('Coś nie gra z tym portfelem');
        }
        /** @var Assault $assault */
        $assault = Assault::where('code', request()->get('assault'))->first();
        if (!$assault) {
            throw new \Exception('Nie ma takiego napadu');
        }
        if ($assault->wallets->where('id', $wallet->id)->count() === 0) {
            throw new \Exception('Nie ma takiego napadu');
        }

        if ($assault->is_finished) {
            $message = 'Napad na ' . $assault->target->name . ' jest zakończony!' . PHP_EOL;
            if ($assault->result === 0) {
                $message .= 'Niestety nie udało się.' . PHP_EOL;
            }
            if ($assault->result === 1) {
                $message .= 'Łatwo poszło' . PHP_EOL;
            }
            if ($assault->result === 3) {
                $message .= 'Anulowano ten napad.' . PHP_EOL;
            }
        } else {
            $message = 'Napad na ' . $assault->target->name . ' jest w przygotowaniu!' . PHP_EOL;
        }

        $message .= 'Szanse: ' . $this->calculateChances($assault) . '%' . PHP_EOL;
        $message .= 'Stawka: ' . $assault->wallets->sum('pivot.value') . ' BMC' . PHP_EOL;
        $message .= 'Zgromadzono typów: ' . $assault->wallets->count() . PHP_EOL . PHP_EOL;
        if(!$assault->is_finished) {
            $message .= '| Typ | AP |' . PHP_EOL;
            $message .= '| --- | --- |' . PHP_EOL;
            foreach ($assault->getAttackDetails() as $entry) {
                $message .= '| ' . $entry['name'] . ' | ' . $entry['power'] . '% |' . PHP_EOL;
            }
        }
        return response()->json(['message' => $message]);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function cancelAssault(): JsonResponse
    {
        $this->validateCancelAssaultRequest();
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $assault = Assault::where('code', request()->get('assault'))->first();
        if ($assault->is_finished) {
            throw new \Exception('This assault is finished');
        }
        if (!$assault) {
            throw new \Exception('Nie ma takiego napadu');
        }

        $this->assaultManager->cancelAssault($wallet, $assault);

        return response()->json(['message' => 'Napad anulowany.']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function joinAssault(): JsonResponse
    {
        $this->validateJoinAssaultRequest();
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        /** @var Assault $assault */
        $assault = Assault::where('code', request()->get('assault'))->first();
        if (!$assault) {
            throw new \Exception('Nie ma takiego napadu');
        }
        if ($assault->is_finished) {
            throw new \Exception('This assault is finished');
        }
        $amount = request()->get('amount');
        if ($amount === 'all') {
            $amount = $wallet->balance - 1;
        }

        $assault = $this->assaultManager->joinAssault($assault, $wallet, $amount);

        $message = 'Dołączyłeś do napadu! Szanse urosły i wynoszą ' . $assault->getChances() . '%' . PHP_EOL;
        $message .= 'Możesz się jeszcze wycofać: `wizard leave-assault ' . $assault->code . '`' . PHP_EOL;
        return response()->json(['message' => $message], 201);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function inviteToAssault(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $targetNames = explode(',', request()->get('targets'));
        $targets = $this->walletRepository->getWalletsForNames($targetNames);
        \Log::info('targets' . print_r($targetNames, true));
        $requested = count($targetNames);
        $found = $targets->count();
        /** @var Assault $assault */
        $assault = Assault::where('code', request()->get('assault'))->first();
        if ($assault->is_finished) {
            throw new \Exception('This assault is finished');
        }
        if (!$assault) {
            throw new \Exception('Nie ma takiego napadu');
        }
        $message = '';
        if (!$wallet || $wallet->is_bot) {
            throw new \Exception('Coś nie gra z tym portfelem');
        }
        if ($targets->where('id', $wallet->id)->count() > 0) {
            --$found;
        }
        if ($targets->where('id', $assault->target_id)->count() > 0) {
            --$found;
        }
        $alreadyInvited = $assault->invited()->whereIn('name', $targetNames)->pluck('id', 'id')->toArray();
        $found -= count($alreadyInvited);
        if ($requested !== $found) {
            $message .= 'Nie znaleziono wszystkich zaproszonych, wysłano '
                        . $found . ' z ' . $requested . '.' . PHP_EOL;
        } else {
            $message .= 'Znaleziono wszystkich zaproszonych i wysłano zaproszenia (' . $found . '/' .
                        $requested . ').' . PHP_EOL;
        }
        $this->assaultManager->inviteToAssault($targets, $assault, $wallet);

        return response()->json(['message' => $message], 201);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function leaveAssault()
    {
        $this->validateLeaveAssaultRequest();
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $assault = Assault::where('code', request()->get('assault'))->first();
        if (!$assault) {
            throw new \Exception('Nie ma takiego napadu');
        }
        if ($assault->is_finished) {
            throw new \Exception('This assault is finished');
        }
        $this->assaultManager->leaveAssault($assault, $wallet);

        $message = 'You have left assault!' . PHP_EOL;
        return response()->json(['message' => $message], 201);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function switchTarget()
    {
        $target = $this->walletRepository->getWalletForName(request()->get('target'));
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        if (!$wallet || $wallet->is_bot) {
            throw new \Exception('Coś nie gra z tym portfelem');
        }
        $assault = Assault::where('code', request()->get('assault'))->first();
        if (!$assault) {
            throw new \Exception('Assault not found');
        }
        if (!$target) {
            throw new \Exception('Target not found');
        }
        if ($assault->is_finished) {
            throw new \Exception('This assault is finished');
        }
        $this->assaultManager->switchTarget($assault, $wallet, $target);

        return response()->json(['message' => 'Zmieniono cel!'], 201);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function commenceAssault()
    {
        $this->validateCommenceAssaultRequest();
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        if (!$wallet || $wallet->is_bot) {
            throw new \Exception('Coś nie gra z tym portfelem.');
        }
        /** @var Assault $assault */
        $assault = Assault::where('code', request()->get('assault'))->first();
        if (!$assault) {
            throw new \Exception('Nie ma takiego napadu');
        }
        if ($assault->boss_id !== $wallet->id) {
            throw new \Exception('Nie ty tu jesteś szefem');
        }
        if ($assault->is_finished) {
            throw new \Exception('This assault is finished');
        }
        $assault = $this->assaultManager->commenceAssault($assault, $wallet);
        $message = '';
        if ($assault->result === 0) {
            $message .= 'Napad nieudany! ' . $assault->target->name . ' jest bezpieczny.' . PHP_EOL;
        } else {
            $message .= 'Napad udany! ' . $assault->target->name . ' wyjebany na '
                        . (int)$assault->getTotalValue()
                        . ' BMC' . PHP_EOL;
        }

        return response()->json(['message' => $message], 200);
    }

    /**
     * @throws ValidationException
     */
    private function validateCreateAssaultRequest()
    {
        $data = request()->all();
        $rules = [
            'name'   => 'required',
            'target' => 'required',
            'amount' => 'required',
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new ValidationException($v);
        }
    }

    /**
     * @throws ValidationException
     */
    private function validateCancelAssaultRequest()
    {
        $data = request()->all();
        $rules = [
            'name'    => 'required',
            'assault' => 'required'
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new ValidationException($v);
        }
    }

    /**
     * @throws Exception
     */
    private function validateJoinAssaultRequest()
    {
        $data = request()->all();
        $rules = [
            'name'    => 'required',
            'assault' => 'required',
            'amount'  => 'required',
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new Exception($v->errors()->first());
        }
    }

    /**
     * @throws Exception
     */
    private function validateCommenceAssaultRequest()
    {
        $data = request()->all();
        $rules = [
            'name'    => 'required',
            'assault' => 'required',
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new Exception($v->errors()->first());
        }
    }

    private function validateLeaveAssaultRequest()
    {
        $data = request()->all();
        $rules = [
            'name'    => 'required',
            'assault' => 'required',
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new Exception($v->errors()->first());
        }
    }

    private function calculateChances(Assault $assault)
    {
        return $assault->getChances();
    }
}
