<?php

namespace JZ\BardzoMagicznyCoin\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\SlaveTrade;
use JZ\BardzoMagicznyCoin\Models\SlaveTransport;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use JZ\BardzoMagicznyCoin\ValueObjects\SlaveRankingWallet;
use October\Rain\Exception\ApplicationException;

/**
 *
 */
class SlaveController
{
    /**
     * @var SlaveryManager
     */
    private $slaveryManager;

    private $walletRepository;

    /**
     * @param SlaveryManager $slaveManager
     */
    public function __construct(SlaveryManager $slaveManager, WalletRepository $walletRepository)
    {
        $this->slaveryManager = $slaveManager;
        $this->walletRepository = $walletRepository;
    }

    /**
     *
     */
    public function setNiggerMode()
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            $mode = request()->get('mode');
            if (!$wallet) {
                throw new ApplicationException('Invalid wallet');
            }
            Log::info($mode);
            if (!in_array($mode, ['attack', 'defend', 'work'])) {
                throw new ApplicationException('Invalid mode');
            }
            $this->slaveryManager->setNiggerMode($wallet, $mode);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function setChinkSetup(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            $work = request()->get('work');
            $maintenance = request()->get('maintenance');
            if (!$wallet) {
                throw new ApplicationException('Invalid wallet');
            }
            $this->slaveryManager->setChinkSetup($wallet, $work, $maintenance);
            $useless = $wallet->chinese - $work - $maintenance;
            return response()->json(['message' => 'New setup created: '
                                                  . $work . ' workers, '
                                                  . $maintenance . ' maintenance, '
                                                  . $useless . ' useless'
                                    ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    public function setSlaveSetup(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            $attack = request()->get('attack');
            $defence = request()->get('defence');
            $work = request()->get('work');
            if (!$wallet) {
                throw new ApplicationException('Invalid wallet');
            }
            $this->slaveryManager->setNiggerSetup($wallet, $work, $attack, $defence);
            $useless = $wallet->niggers - $attack - $defence - $work;
            return response()->json(['message' => 'New setup created: '
                                                  . $attack . ' attackers, '
                                                  . $defence . ' defenders, '
                                                  . $work . ' workers, '
                                                  . $useless . ' useless'
                                    ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    /**
     * @return JsonResponse
     */
    public function hireNigger(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $amount = request()->get('amount', 1);
            if(!trim($amount)){
                $amount = 1;
            }

            if ($amount && $amount > 5) {
                return response()->json([
                                            'error' => 'Maksymalnie 5. Pamiętaj, że jak kupujesz więcej, to mogą uciec po drodze.'
                                        ]);
            }
            // todo move to SlaveryManager
            $settings = Settings::instance();
            $niggerPrice = $settings->get('nigger_price', 200) * $amount;
            if ($wallet->balance < $niggerPrice) {
                return response()->json(['error' => 'Nie stać cię. Murzyn kosztuje ' . $niggerPrice . ' :bmc:']);
            }
            $transport = $this->slaveryManager->hireNigger($wallet, $niggerPrice, $amount);
            $wallet->hired_at = $transport->created_at;
            $wallet->save();
            $cd = SlaveryManager::HIRE_CD . ' minut(y).';
            $message = 'Kupiono ' . $amount . ' murzynów za ' . $niggerPrice . ' :bmc: ! Dostawa za ' . $cd .' :kotel_euiv:';
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function hireChink(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }

            $amount = request()->get('amount', 1);

            if ($amount && $amount > 5) {
                return response()->json([
                                            'error' => 'Maksymalnie 5. Pamiętaj, że jak kupujesz więcej, to mogą uciec po drodze.'
                                        ]);
            }

            // todo move to SlaveryManager
            $settings = Settings::instance();
            $chinkPrice = $settings->get('chink_price', 200) * $amount;
            if ($wallet->balance < $chinkPrice) {
                return response()->json(['error' => 'Nie stać cię. Żółtek kosztuje ' . $chinkPrice . ' :bmc:']);
            }
            $transport = $this->slaveryManager->hireChink($wallet, $chinkPrice, $amount);
            $wallet->hired_at =  $transport->created_at;
            $wallet->save();
            $cd = SlaveryManager::HIRE_CD . ' minut(y).';
            $message = 'Kupiono ' . $amount . ' żółtków za '
                       . $chinkPrice . ' :bmc: Dostawa za ' . $cd . ' :chink:' . PHP_EOL;
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function killChink(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }

            $chinkCount = $this->slaveryManager->killChink($wallet);
            $message = 'Zwolniono :kappa: żółtka! Posiadasz aktualnie ' . $chinkCount . ' żółtków :kotel_euiv: ';

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function killNigger(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            // todo move to SlaveryManager
            $settings = Settings::instance();
            $niggersIncome = $settings->get('niggers_income', 5);
            $niggersCount = $this->slaveryManager->killNigger($wallet);
            $message = 'Zwolniono :kappa: murzyna! Posiadasz aktualnie ' . $niggersCount . ' murzynów, dostarczających '
                       . $wallet->getWorkPower() * $niggersIncome . ' :bmc: dziennie :kotel_euiv: ';

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function sellNigger(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $amount = request()->get('amount');
            $price = request()->get('price');
            $buyerName = request()->get('buyer');
            $buyer = Wallet::where('name', $buyerName)->first();
            if ($buyerName && !$buyer) {
                return response()->json(['error' => 'Portfel ' . $buyerName . ' nie istnieje']);
            }
            $offer = $this->slaveryManager->sellNigger($wallet, $amount, $price, $buyer);
            $message = 'Stworzono ofertę! :kotel_euiv: ' . $wallet->name . ' chce sprzedać '
                       . $amount . ' murzynów za ' . $price
                       . ' :bmc: . ID oferty: ' . $offer->id
                       . '. Ofertę można zaakceptować komendą `wizard buy-nigger ' . $offer->id . '` ';
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function sellChink(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $amount = request()->get('amount');
            $price = request()->get('price');
            $buyerName = request()->get('buyer');
            $buyer = Wallet::where('name', $buyerName)->first();
            if ($buyerName && !$buyer) {
                return response()->json(['error' => 'Portfel ' . $buyerName . ' nie istnieje']);
            }
            $offer = $this->slaveryManager->sellChink($wallet, $amount, $price, $buyer);
            $message = 'Stworzono ofertę! :kotel_euiv: ' . $wallet->name . ' chce sprzedać '
                       . $amount . ' chińczyków za ' . $price
                       . ' :bmc: . ID oferty: ' . $offer->id
                       . '. Ofertę można zaakceptować komendą `wizard buy-chink ' . $offer->id . '` ';
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function cancelSellSlave(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $type = post('type', 'nigger');
            $offerId = request()->get('offer');
            $this->slaveryManager->cancelSellSlave($wallet, $offerId, $type);
            $message = 'Anulowano ofertę #' . $offerId;
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    /**
     * @return JsonResponse
     */
    public function buyNigger(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $offerId = request()->get('offer');
            $offer = $this->slaveryManager->buySlave($wallet, $offerId, 'nigger');
            $message = 'Zaakceptowałes ofertę ' . $offer->seller->name
                       . ' :kotel_euiv: . Zakupiono murzynów: ' . $offer->amount
                       . ' za ' . $offer->price;
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    /**
     * @return JsonResponse
     */
    public function buyChink(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $offerId = request()->get('offer');
            $offer = $this->slaveryManager->buySlave($wallet, $offerId, 'chinese');
            $message = 'Zaakceptowałes ofertę ' . $offer->seller->name
                       . ' :kotel_euiv: . Zakupiono chińczyków: ' . $offer->amount
                       . ' za ' . $offer->price;
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function multiGasJew(): JsonResponse
    {
        try {
            if (BMCGameEvents::areJewsDead()) {
                throw new TransactionException('Wszyscy żydzi nie żyją! :fireworks:');
            }
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            // todo move to SlaveryManager
            $settings = Settings::instance();
            $jewPrice = $settings->get('jew_price', 1000);
            $amount = request()->get('amount', 1);
            if (!is_numeric($amount) || $amount < 1) {
                return response()->json(['error' => 'Zła ilość']);
            }
            $jewsPrice = $jewPrice * $amount;
            if ($wallet->balance < $jewsPrice) {
                return response()->json(['error' => 'Nie stać cię. '
                                                    . $amount . ' żydów kosztuje ' . $jewsPrice . ' :bmc:']);
            }
            $wizardWallet = Wallet::where('name', 'wizard')->first();
            $this->walletRepository->deductFromWalletBalance($wallet, $jewsPrice);
            $this->walletRepository->addToWalletBalance($wizardWallet, $jewsPrice);

            $this->transactionRepository->createTransaction(
                $wallet,
                $wizardWallet,
                $jewsPrice,
                'transfer',
                'Holocaust Investment'
            );
            sleep(1);
            $this->slaveryManager->gasJew($wallet, $jewPrice, $amount, false);

            $message = $amount . ' żydów poszło z dymem. Tym razem też niestety wirtualnie.';

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }


    /**
     *
     */
    public function hireJew(): JsonResponse
    {
        try {
            if (BMCGameEvents::areJewsDead()) {
                throw new TransactionException('Wszyscy żydzi nie żyją! :fireworks:');
            }
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            // todo move to SlaveryManager
            $settings = Settings::instance();
            $jewPrice = $settings->get('jew_price', 1000);
            $jewIncome = $wallet->calculateJewIncome();
            if ($wallet->balance < $jewPrice) {
                return response()->json(['error' => 'Nie stać cię. Żyd kosztuje ' . $jewPrice . ' :bmc:']);
            }
            $this->slaveryManager->hireJew($wallet, $jewPrice);
            $message = 'Zatrudniono żyda! Żyd zwiększa twój majątek o '
                       . $jewIncome
                       . '% dziennie, ale sprawia że częściej przegrywasz w kasynie i challengach,
                        because with jews you loose';

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     *
     */
    public function gasJew(): JsonResponse
    {
        try {
            if (BMCGameEvents::areJewsDead()) {
                throw new TransactionException('Wszyscy żydzi nie żyją! :fireworks:');
            }
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $amount = 1;
            if (request()->get('amount')) {
                $amount = (int)request()->get('amount');
            }
            // todo move to SlaveryManager
            $settings = Settings::instance();
            $jewPrice = $settings->get('jew_price', 1000);
            $transaction = $this->slaveryManager->gasJew($wallet, $jewPrice, $amount);
            $message = 'Zwolniono :kappa: żyda! Za dobry uczynek otrzymujesz '
                       . $transaction->value
                       . ' :bmc: .' . ' Lecimy do 7 milionów!';
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function slaveStatus(): JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            if (!$wallet) {
                return response()->json(['error' => 'Nie istnieje taki portfel.']);
            }
            $message = $this->slaveryManager->getSlaveStatus($wallet);

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function slaveTrade(): JsonResponse
    {
        try {
            $offerId = request()->get('offer');
            /** @var SlaveTrade $offer */
            $offer = SlaveTrade::where('id', $offerId)->first();
            if (!$offer) {
                return response()->json(['error' => 'Offer not found']);
            }
            $message = ':kotel_euiv: Oferta od ' . $offer->seller->name . PHP_EOL
                       . 'Typ: ' . $offer->type . PHP_EOL
                       . 'Ilość: ' . $offer->amount . PHP_EOL
                       . 'Cena: ' . $offer->price . PHP_EOL;
            if ($offer->is_closed && $offer->buyer) {
                $message .= 'Oferta zamknięta . Nabywca: ' . $offer->buyer->name;
            }
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function slaveTrades(): JsonResponse
    {
        try {
            $payload = [
                '| ID | Seller | Slaves | Price | Reserved |',
                '|---|---|---|---|---|'
            ];
            /** @var SlaveTrade $offer */
            $trades = SlaveTrade::where('is_closed', false)->get();
            if ($trades->count() === 0) {
                return response()->json(['message' => 'No active offers found.']);
            }
            foreach ($trades as $offer) {
                $reserved = 'No';
                if ($offer->buyer_id) {
                    $reserved = 'Yes';
                }
                $offerDetails = $offer->amount . ' ???';
                if ($offer->type === 'nigger') {
                    $offerDetails = $offer->amount . ' :kotel_euiv: ';
                }
                if ($offer->type === 'chinese') {
                    $offerDetails = $offer->amount . ' :chink: ';
                }

                $payload[] = '| '
                             . $offer->id . ' | '
                             . $offer->seller->name . ' | '
                             . $offerDetails . ' | '
                             . $offer->price . ' :bmc: | '
                             . $reserved . ' |';
            }
            $list = implode(PHP_EOL, $payload);

            return response()->json(['message' => $list]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function slavesRanking(): JsonResponse
    {
        try {
            $settings = Settings::instance();
            $maxNiggers = $settings->get('max_niggers', 50);
            $maxChinks = $settings->get('max_chinese', 70);
            $ranking = $this->walletRepository->getSlaveRanking();
            $payload = [
                '| Pos | Name | Niggers | Chink | Gassed Jews | Active Jew | Slaves Value |',
                '|---|---|---|---|---|---|---|'
            ];
            $position = 1;
            $totalNiggers = 0;
            $totalJews = 0;
            $totalGassedJews = 0;
            $totalValue = 0;
            $totalChinks = 0;
            /** @var SlaveRankingWallet $obj */
            foreach ($ranking as $obj) {
                $totalNiggers += $obj->niggers;
                $totalChinks += $obj->chinese;
                $totalGassedJews += $obj->gassedJews;
                $totalValue += $obj->slavesValue;
                if ($obj->hasJew === 'Yes :kotelwsieni:') {
                    ++$totalJews;
                }
                $payload[] = '|' . $position . ' | `' . $obj->name . '` | ' . $obj->niggers
                             . ' :kotel_euiv: |' . $obj->chinese . ' :chink: | ' . $obj->gassedJews . '|'
                             . $obj->hasJew . '| **' . $obj->slavesValue . '** :bmc: |';
                ++$position;
            }

            $payload[] = '| | Total: | **' . $totalNiggers . ' / ' . $maxNiggers . '** :kotel_euiv: | **'
                         . $totalChinks . ' / ' . $maxChinks . '** :chink: |'
                         . $totalGassedJews
                         . ' :zyklon: | ' . $totalJews . ' :kotelwsieni:  | ' . $totalValue . '|';
            $list = implode(PHP_EOL, $payload);

            return response()->json(['message' => $list]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    // TODO move all slave stuff here from ApiController
}
