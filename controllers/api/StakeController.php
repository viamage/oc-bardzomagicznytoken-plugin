<?php

namespace JZ\BardzoMagicznyCoin\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Stake;

/**
 *
 */
class StakeController
{
    /**
     * @var StakesManager
     */
    private $stakeManager;

    /**
     * @var WalletRepository
     */
    private $walletRepository;

    /**
     * @param StakesManager $stakesManager
     */
    public function __construct(StakesManager $stakesManager, WalletRepository $walletRepository)
    {
        $this->stakeManager = $stakesManager;
        $this->walletRepository = $walletRepository;
    }

    /**
     * @return JsonResponse
     */
    public function getActiveStakes(): JsonResponse
    {
        try {
            $stakes = Stake::where('is_finished', false)->get();
            $message = 'Active Stakes:' . PHP_EOL;
            foreach ($stakes as $stake) {
                $message .= '- (' . $stake->id . ') **'
                            . $stake->label
                            . '** ' . $this->getOptionsString($stake->options);
                if ($stake->is_open) {
                    $message .= ' :white_check_mark:';
                } else {
                    $message .= ' :x:';
                }
                $message .= PHP_EOL;
            }
            if ($stakes->count() === 0) {
                $message = 'No active stakes';
            } else {
                $message .= 'To vote use: `wizard vote-stake <id> <option> <amount>`';
            }
            return response()->json(['message' => $message]);
        } catch (TransactionException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function createStake(): JsonResponse
    {
        try {
            $data = request()->all();
            $this->validateCreateStakePayload($data);
            $wallet = $this->walletRepository->getWalletForName(request()->get('wallet'));
            $options = $this->parseOptions($data['options']);
            $stake = $this->stakeManager->createStake($wallet, $data['label'], $options);
            $message = 'Stake Created! :tada:' . PHP_EOL .
                       $stake->label . PHP_EOL;
            foreach ($stake->options as $option) {
                $message .= $option['id'] . ' - ' . $option['label'] . PHP_EOL;
            }
            $message .= 'To vote for an option write: `wizard vote-stake ' . $stake->id . ' <option> <amount>`';
            return response()->json(['message' =>
                                         $message
                                    ]);
        } catch (TransactionException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function closeStake(): JsonResponse
    {
        try {
            $id = request()->get('id');

            if (!$id) {
                throw new TransactionException('Stake id is required');
            }
            $stake = Stake::where('id', $id)->first();
            if (!$stake) {
                throw new TransactionException('Stake not found');
            }
            $wallet = $this->walletRepository->getWalletForName(request()->get('wallet'));
            if ($wallet->id !== $stake->wallet_id) {
                throw new TransactionException('You are not the creator of this stake');
            }
            $this->stakeManager->closeStake($stake);

            return response()->json(['message' => 'Stake closed. Voting is no longer possible.']);
        } catch (TransactionException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function finishStake(): JsonResponse
    {
        try {
            $id = request()->get('id');

            if (!$id) {
                throw new TransactionException('Stake id is required');
            }
            $stake = Stake::where('id', $id)->where('is_finished', false)->first();
            if (!$stake) {
                throw new TransactionException('Stake not found');
            }
            $wallet = $this->walletRepository->getWalletForName(request()->get('wallet'));
            if ($wallet->id !== $stake->wallet_id) {
                throw new TransactionException('You are not the creator of this stake');
            }
            $wonCount = $this->stakeManager->finishStake($stake, request()->get('win_option'));
            $message = 'Stake finished. Rewards sent to players. '
                       . $wonCount;
            if ($wonCount === 1) {
                $message .= ' bid won!';
            } else {
                $message .= ' bids won!';
            }
            return response()->json([
                                        'message' => $message
                                    ]);
        } catch (TransactionException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function voteStake(): JsonResponse
    {
        try {
            $id = request()->get('id');
            if (!$id) {
                throw new TransactionException('Stake id is required');
            }
            $stake = Stake::where('id', $id)->first();
            if (!$stake) {
                throw new TransactionException('Stake not found');
            }
            $wallet = $this->walletRepository->getWalletForName(request()->get('wallet'));
            $this->stakeManager->voteForStake($wallet, $stake, request()->get('option'), request()->get('amount'));


            return response()->json([
                                        'message' => 'Voted! :tada:'
                                    ]);
        } catch (TransactionException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function getStakeRates(): JsonResponse
    {
        try {
            $id = request()->get('id');
            if (!$id) {
                throw new TransactionException('Stake id is required');
            }
            /** @var Stake $stake */
            $stake = Stake::where('id', $id)->first();
            if (!$stake) {
                throw new TransactionException('Stake not found');
            }
            $rates = $this->stakeManager->calculateStakeRates($stake, true);
            $message = '**' . $stake->label . '** (' . $stake->id . ')' . PHP_EOL . PHP_EOL;
            $message .= 'Current Stakes:' . PHP_EOL;
            foreach ($rates as $option => $rate) {
                $message .= 'Option ' . $option . ': ' . $rate . ' :bmc:' . PHP_EOL;
            }
            if ($stake->is_finished) {
                $results = $this->stakeManager->countWinners($stake);
                $message .= PHP_EOL . 'Stake ended, ' . $results->winners . ' bids won total pool of ' . $results->pool;
            }
            if (!$stake->is_finished && !$stake->is_open) {
                $message .= PHP_EOL
                            . 'Voting is closed for this stake. Creator can finish it with `wizard finish-stake '
                            . $stake->id . ' <option>`';
            }
            if (!$stake->is_finished && $stake->is_open) {
                $message .= PHP_EOL . 'To vote use: `wizard vote-stake ' . $stake->id . ' <option> <amount>`';
            }
            return response()->json([
                                        'message' => $message
                                    ]);
        } catch (TransactionException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @param array $data
     */
    private function validateCreateStakePayload(array $data)
    {
        $rules = [
            'label'   => 'required',
            'options' => 'required',
            'wallet'  => 'required'
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new TransactionException($v->errors()->first());
        }
    }

    /**
     * @param $options
     */
    private function parseOptions($options)
    {
        $options = explode(',', $options);
        $parsedOptions = [];
        $count = 1;
        foreach ($options as $option) {
            $parsedOptions[] = [
                'id'    => $count,
                'label' => trim($option)
            ];
            ++$count;
        }
        return $parsedOptions;
    }

    private function getOptionsString($options)
    {
        $last = count($options);
        $count = 1;
        $string = '(';
        foreach ($options as $option) {
            $string .= $option['id'] . ': ' . $option['label'];
            if ($count !== $last) {
                $string .= ', ';
            }
            ++$count;
        }
        $string .= ')';
        return $string;
    }
}
