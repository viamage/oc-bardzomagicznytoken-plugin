<?php

namespace JZ\BardzoMagicznyCoin\Controllers;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use JZ\BardzoMagicznyCoin\Classes\BMCBetManager;
use JZ\BardzoMagicznyCoin\Classes\BMCChallengeManager;
use JZ\BardzoMagicznyCoin\Classes\BMCEventManager;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\CoinLabelHelper;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Classes\Spellbook;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Exceptions\BMCEventException;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Event;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\SlaveTrade;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use JZ\BardzoMagicznyCoin\ValueObjects\BetRankingWallet;
use JZ\BardzoMagicznyCoin\ValueObjects\SlaveRankingWallet;
use JZ\DeepFocusPlaylist\Classes\YouTube;
use JZ\DeepFocusPlaylist\Models\Playlist;
use JZ\DeepFocusPlaylist\Models\Entry;
use October\Rain\Support\Facades\Markdown;
use PHPUnit\Util\Exception;

/**
 * TODO some generic validation middleware
 */
class ApiController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;
    /**
     * @var WalletRepository
     */
    private $walletRepository;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var BMCChallengeManager
     */
    private $challengeManager;

    /**
     * @var SlaveryManager
     */
    private $slaveryManager;

    /**
     * @var BMCBetManager
     */
    private $betManager;

    /**
     * @var Spellbook
     */
    private $spellBook;

    /**
     * @var BMCEventManager
     */
    private $eventManager;

    /**
     * @param WalletRepository      $walletRepository
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(
        WalletRepository      $walletRepository,
        TransactionRepository $transactionRepository,
        TransactionManager    $transactionManager,
        BMCChallengeManager   $challengeManager,
        BMCEventManager       $eventManager,
        SlaveryManager        $slaveryManager,
        BMCBetManager         $betManager,
        Spellbook             $spellBook
    ) {
        $this->walletRepository = $walletRepository;
        $this->transactionRepository = $transactionRepository;
        $this->transactionManager = $transactionManager;
        $this->challengeManager = $challengeManager;
        $this->eventManager = $eventManager;
        $this->slaveryManager = $slaveryManager;
        $this->betManager = $betManager;
        $this->spellBook = $spellBook;
    }

    public function makeCataclysm(): JsonResponse
    {
        if (!request()->get('user')) {
            return response()->json(['error' => 'Invalid request']);
        }

        $wallet = $this->walletRepository->getWalletForName(request()->get('user'));

        $result = $this->eventManager->triggerCataclysm($wallet);
        return response()->json(['message' => $result]);
    }

    /**
     * @return JsonResponse
     */
    public function createWallet(): JsonResponse
    {
        if (!request()->get('name')) {
            return response()->json(['error' => 'Invalid request']);
        }
        if ($this->walletRepository->getWalletForName(request()->get('name'))) {
            return response()->json(['error' => 'Masz już portfel!']);
        }

        $wallet = $this->walletRepository->createWallet(request()->get('name'), 0);

        return response()->json([
                                    'message' => 'Stworzyłem portfel! Ustawiłem domyślnie jako profil bota // @jakub',
                                    'wallet'  => $wallet,
                                ]);
    }

    /**
     * @return JsonResponse
     */
    public function totalCoins(): JsonResponse
    {
        $wallets = $this->walletRepository->getWhitelistedWallets();
        $sum = 0;
        /** @var Wallet $wallet */
        foreach ($wallets as $wallet) {
            if ($wallet->name !== 'wizard') {
                $sum += $wallet->balance;
            }
        }
        return response()->json(['message' => 'Łącznie na rynku znajduje się ' . $sum . ' :bmc:']);
    }

    /**
     * @return JsonResponse
     */
    public function subscribe(): JsonResponse
    {
        if (!request()->get('user')) {
            return response()->json(['error' => 'Invalid request']);
        }

        $wallet = $this->walletRepository->getWalletForName(request()->get('user'));

        if (!$wallet) {
            return response()->json(['error' => 'Nie masz portfela! Spróbuj `wizard create wallet`']);
        }
        if ($wallet->is_subscribed) {
            return response()->json(['error' => 'Już zasubskrybowałeś notyfikacje na temat BMC']);
        }

        $wallet->is_subscribed = true;
        $wallet->save();
        return response()->json(['message' => 'Zasubskrybowałeś notyfikacje na temat BMC. Użyj `wizard unsubscribe bmc` żeby zrezygnować.']);
    }

    /**
     * @return JsonResponse
     */
    public function unsubscribe(): JsonResponse
    {
        if (!request()->get('user')) {
            return response()->json(['error' => 'Invalid request']);
        }
        $wallet = $this->walletRepository->getWalletForName(request()->get('user'));
        if (!$wallet) {
            return response()->json(['error' => 'Nie masz portfela! Spróbuj `wizard create wallet`']);
        }
        if ($wallet->is_subscribed) {
            return response()->json(['error' => 'Nie zasubskrybowałeś notyfikacje na temat BMC']);
        }
        $wallet->is_subscribed = false;
        $wallet->save();
        return response()->json(['message' => 'Odsubskrybowałeś notyfikacje na temat BMC. Użyj `wizard subscribe bmc` żeby ponownie zasubskrybować.']);
    }

    /**
     * @param string $mode
     *
     * @return JsonResponse
     */
    public function createTransaction(string $mode = 'transfer'): JsonResponse
    {
        try {
            if (request()->get('target') === 'all') {
                $mode = 'multitransfer';
            }
            if ($mode === 'transfer' || $mode === 'payment') {
                return $this->transactionManager->sendToWalletForRequest($mode);
            }
            if ($mode === 'multitransfer') {
                return $this->transactionManager->sendToAllWalletsForRequest();
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            return response()->json([
                                        'error' => $e->getMessage(),
                                    ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function bet(): JsonResponse
    {
        try {
            $results = $this->betManager->bet();
            if ($results->victory) {
                $message = 'Wygrałeś! Twój stan konta to ' . $results->balanceAfter
                           . ' ' . CoinLabelHelper::generate($results->balanceAfter);
            } else {
                $message = 'Przegrałeś! Twój stan konta to ' . $results->balanceAfter
                           . ' ' . CoinLabelHelper::generate($results->balanceAfter);
            }
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            return response()->json([
                                        'error' => $e->getMessage(),
                                    ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function betBalance(): JsonResponse
    {
        $username = request()->get('name');
        if (!$username) {
            return response()->json(['error' => 'Invalid Payload']);
        }
        $wallet = $this->walletRepository->getWalletForName($username);
        $wonBets = $this->transactionRepository->getWonBetsForWallet($wallet);
        $lostBets = $this->transactionRepository->getLostBetsForWallet($wallet);
        $wonAmount = 0;
        $lostAmount = 0;
        $totalAmount = 0;
        $wonBets->each(function (Transaction $transaction) use (&$wonAmount, &$totalAmount) {
            $wonAmount += $transaction->value;
            $totalAmount += $transaction->value;
        });
        $lostBets->each(function (Transaction $transaction) use (&$lostAmount, &$totalAmount) {
            $lostAmount += $transaction->value;
            $totalAmount -= $transaction->value;
        });
        if ($totalAmount < 0) {
            $opinion = 'Jeszcze się odegrasz ' . $username . ' :kekw:';
        } else {
            $opinion = 'Gratulacje!';
        }

        return response()->json([
                                    'message' => $username . ' wygrał do tej pory '
                                                 . $wonAmount . ' BMC i przejebał '
                                                 . $lostAmount . ' BMC. Saldo: '
                                                 . $totalAmount . ' BMC :bmc: ' . $opinion,
                                ]);
    }

    /**
     * @return JsonResponse
     */
    public function getBetRanking(): JsonResponse
    {
        $ranking = $this->walletRepository->getBetRanking();
        $payload = [
            '| Pos | Name | Won :bmc: | Lost :bmc: | Total :bmc: | Games | W/L |',
            '|---|---|---|---|---|---|---|',
        ];
        $position = 1;
        /** @var BetRankingWallet $obj */
        foreach ($ranking as $obj) {
            $payload[] = '|' . $position . ' | `' . $obj->name . '` | ' . $obj->won . '|' . $obj->lost . '|**'
                         . $obj->total . '**|' . $obj->games . '|' . $obj->wonGames . '/' . $obj->lostGames . '|';
            ++$position;
        }
        $list = implode(PHP_EOL, $payload);

        return response()->json(['message' => $list]);
    }


    /**
     * @return JsonResponse
     */
    public function getChallengeRanking(): JsonResponse
    {
        $ranking = $this->challengeManager->getChallengeRanking();
        $payload = [
            '| Pos | Name | Won :bmc: | Lost :bmc: | Total :bmc: | Games | W/L |',
            '|---|---|---|---|---|---|---|',
        ];
        $position = 1;
        /** @var BetRankingWallet $obj */
        foreach ($ranking as $obj) {
            $payload[] = '|' . $position . ' | `' . $obj->name . '` | ' . $obj->won . '|' . $obj->lost . '|**'
                         . $obj->total . '**|' . $obj->games . '|' . $obj->wonGames . '/' . $obj->lostGames . '|';
            ++$position;
        }
        $list = implode(PHP_EOL, $payload);

        return response()->json(['message' => $list]);
    }

    /**
     * @return JsonResponse
     */
    public function poczaruj(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));

        try {
            $this->spellBook->checkIfMagician($wallet);
            $this->spellBook->checkSpellCD($wallet);
            $message = $this->spellBook->castSpell($wallet);

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function generateUiCode(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $code = $this->generateRandomString(128);
        $wallet->ui_code = $code;
        $wallet->save();
        $message = 'Twój kod do UI to: ' . PHP_EOL . '```'. PHP_EOL . $code . PHP_EOL . '```' . PHP_EOL;
        $message .= 'Panel dostepny jest pod adresem ' . env('APP_URL') .'/bmc';
        try {
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function getRandomDeepFocusEntry(): JsonResponse
    {
        $entry = Entry::inRandomOrder()->first();
        $message = 'Losowa piosenka z Deep Focus Playlist: ' . PHP_EOL . $entry->url;
        try {
            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function addToDeepFocusPlaylist(): JsonResponse
    {
        try {
            $source = $this->walletRepository->getWalletForName(request()->get('source'));
            $wizard = $this->walletRepository->getWalletForName('wizard');
            if (!$source) {
                throw new TransactionException('Nie znaleziono portfela!');
            }
            if ($source->balance < 5) {
                return response()->json(['error' => 'Nie stać cię. Dodanie piosenki do DFP kosztuje 5 :bmc:']);
            }
            $playlist = Playlist::where('slug', 'deep-focus-playlist')->first();
            if (Entry::where('url', request()->get('url'))->where('playlist_id', $playlist->id)->first()){
                return response()->json(['error' => 'Ta piosenka już jest w Deep Focus Playlist!']);
            }
            $this->transactionManager->sendToWallet($source, $wizard, 5, 'Deep Focus Playlist fee');
            $entry = new Entry();
            $entry->url = request()->get('url');
            $entry->playlist_id = $playlist->id;
            $entry->save();
            $message = 'Piosenka została dodana do Deep Focus Playlist! Pobrano 5 BMC';

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function alms(): JsonResponse
    {
        try {
            $source = $this->walletRepository->getWalletForName(request()->get('source'));
            if (!$source) {
                throw new TransactionException('Nie znaleziono portfela!');
            }
            $title = 'Jałmużna od ' . $source->name;
            $amount = request()->get('amount');
            $count = request()->get('count', 5);
            $transactions = $this->transactionManager->giveAlms($source, $amount, $count, 'multitransfer', $title);
            $finalAmount = 0;
            $targets = [];
            /** @var Transaction $transaction */
            foreach ($transactions as $transaction) {
                $finalAmount += $transaction->value;
                $targets[] = $transaction->target->name;
            }
            $response = 'Fundacja Pomoc Ciemnosci rozdysponowała ' . $finalAmount
                        . ' :bmc: z podanej kwoty wśród ' . $count . ' najbiedniejszych: '
                        . implode(', ', $targets) . '. Bóg zapłać! ';

            return response()->json([
                                        'message' => $response,
                                    ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            return response()->json([
                                        'error' => $e->getMessage(),
                                    ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function challenge(): JsonResponse
    {
        try {
            $challenge = $this->challengeManager->raiseChallenge();
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        return response()->json([
                                    'message' => 'Wyzwanie stworzone! @'
                                                 . $challenge->challenged->name . '! '
                                                 . $challenge->challenger->name
                                                 . ' chce się bić o ' . $challenge->amount
                                                 . ' BMC. Użyj `wizard accept` by zaakceptować lub `wizard reject` by odrzucić.',
                                    'hash'    => $challenge->hash,
                                ]);
    }

    /**
     *
     * @return JsonResponse
     */
    public function rejectChallenge(): JsonResponse
    {
        try {
            $message = $this->challengeManager->rejectChallenge();
            return response()->json([
                                        'message' => $message,
                                    ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     *
     * @return JsonResponse
     */
    public function cancelChallenge(): JsonResponse
    {
        try {
            $challenged = $this->challengeManager->cancelChallenge();
            return response()->json([
                                        'message' => 'Pomyślnie anulowano wyzwanie przeciwko ' . $challenged,
                                    ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     *
     * @return JsonResponse
     */
    public function acceptChallenge(): JsonResponse
    {
        try {
            $result = $this->challengeManager->acceptChallenge();
            return response()->json([
                                        'message' => $result,
                                    ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     *
     * @return JsonResponse
     */
    public function getChallenges(): JsonResponse
    {
        try {
            $result = $this->challengeManager->getChallenges();
            return response()->json([
                                        'message' => $result,
                                    ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function checkEvent(): JsonResponse
    {
        try {
            $this->validateEventRequest();
            $userName = request()->get('name');
            $wallet = $this->walletRepository->getWalletForName($userName);
            if (!$wallet) {
                throw new \Exception('Nie masz portfela! Użyj `wizard create wallet ' . request()->get('name') . '`');
            }
            $this->eventManager->checkEvent($wallet);
            return response()->json(['message' => 'ok']);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function processEvent(): JsonResponse
    {
        try {
            $this->validateEventRequest();
            $userName = request()->get('name');
            $wallet = $this->walletRepository->getWalletForName($userName);
            if (!$wallet) {
                throw new \Exception('Nie masz portfela! Użyj `wizard create wallet ' . request()->get('name') . '`');
            }
            return response()->json(['message' => $this->eventManager->processEvent($wallet)]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getTransaction()
    {
        try {
            $userName = request()->get('name');
            $wallet = $this->walletRepository->getWalletForName($userName);
            if (!$wallet) {
                throw new \Exception('Nie masz portfela! Użyj `wizard create wallet ' . request()->get('name') . '`');
            }
            $id = request()->get('id');
            /** @var Transaction $transaction */
            $transaction = Transaction::where('hash', $id)->first();
            //if ($transaction->source->name !== $userName || $transaction->target->name !== $userName) {
            //    throw new TransactionException('Nie masz dostępu do tej transakcji');
            //}
            $title = $transaction->title;
            if (!$title) {
                $title = '-';
            }
            $message = 'Transakcja #' . $transaction->hash . ' ' .
                       $transaction->value . ' BMC. Od: ' .
                       $transaction->source->name . ' Do: ' . $transaction->target->name . ' Typ: ' .
                       $transaction->type . ' Data: ' . $transaction->created_at . ' Tytuł: ' . $title;
            \Log::info($message);
            return response()->json([
                                        'message' => $message,
                                    ]);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json([
                                        'error' => $e->getMessage(),
                                    ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getTransactions(): JsonResponse
    {
        try {
            $userName = request()->get('name');
            $wallet = $this->walletRepository->getWalletForName($userName);
            $url = env('APP_URL') . '/bmc-wallet/' . $wallet->hash;
            return response()->json(['url' => $url]);
            /*
                       $filterLabel = 'Lista dzisiejszych transakcji';
                       $from = Carbon::now()->startOfDay();
                       $to = Carbon::now()->endOfDay();

                       $userName = request()->get('name');
                       $wallet = $this->walletRepository->getWalletForName($userName);
                       if (!$wallet) {
                           throw new \Exception('Nie masz portfela! Użyj `wizard create wallet ' . request()->get('name') . '`');
                       }
                       if (request()->get('from')) {
                           $filterLabel = 'Lista transakcji od ' . request()->get('from');
                           $from = new Carbon(request()->get('from'));
                       }
                       if (request()->get('to')) {
                           $filterLabel = 'Lista transakcji od ' . request()->get('from') . ' do ' . request()->get('to');
                           $to = new Carbon(request()->get('to'));
                       }
                       if ($from->diffInDays($to, true) > 120) {
                           throw new Exception('Maksymalna różnica dat to 120 dni');
                       }

                       $result = $filterLabel . PHP_EOL . PHP_EOL;
                       $transactions = $this->transactionRepository->getTransactionsForWallet($wallet, $from, $to);
                       $tableHeader = '| Data | Nadawca | Odbiorca | Kwota | Typ | Tytuł | Saldo | Hash |' . PHP_EOL;
                       $tableHeader .= '|:---|:---|:---|:---|:---|:---|:---|:---|' . PHP_EOL;
                       $result .= $tableHeader;
                       $count = 1;
                       $segments = [];
                       foreach ($transactions as $t) {
                           if ($count % 40 === 0) {
                               $segments[] = $result;
                               $result = $tableHeader;
                           }
                           if ($t->source_id === $wallet->id) {
                               $transferType = ':arrow_heading_up:';
                               $balance = $t->source_balance;
                           } else {
                               $transferType = ':arrow_heading_down:';
                               $balance = $t->target_balance;
                           }
                           $result .= '|' . $t->created_at . '|`' . $t->source->name . '`|`' . $t->target->name . '`|' . $t->value
                                      . '|' . $transferType . ' (' . $t->type . ')' . '|' . $t->title . '|' . $balance . '|' . $t->hash . '|' . PHP_EOL;
                           if ($count === $transactions->count()) {
                               $segments[] = $result;
                           }
                           ++$count;
                       }
                       if ($transactions->count() === 0) {
                           $segments = [$filterLabel . ' jest pusta'];
                       }

                       return response()->json([
                                                   'message' => $segments
                                               ]);
            */
        } catch (InvalidFormatException $e) {
            return response()->json(['error' => 'Coś jest nie tak z tymi datami.']);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json([
                                        'error' => $e->getMessage(),
                                    ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getBalance(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));

        if (!$wallet) {
            return response()->json(['error' => 'Nie istnieje taki portfel.']);
        }
        if ($wallet->is_blacklisted) {
            return response()->json(['error' => 'Ten portfel jest na czarnej liście :niger: ']);
        }
        return response()->json([
                                    'message' => 'Stan konta dla ' . $wallet->name . ' wynosi ' . $wallet->balance . ' '
                                                 . CoinLabelHelper::generate($wallet->balance),
                                    'balance' => $wallet->balance,
                                ]);
    }

    /**
     * @return JsonResponse
     */
    public function getRanking(): JsonResponse
    {
        $ranking = $this->walletRepository->getRanking();
        $payload = [
            '| Pos | Name | :bmc: | AP | Def | WP |',
            '|---|---|---|---|---|---|',
        ];
        $position = 1;
        /** @var Wallet $wallet */
        foreach ($ranking as $wallet) {
            $payload[] = '|' . $position . ' | `' . $wallet->name . '` | '
                         . $wallet->balance . ' | '
                         . $wallet->getAttackPower() . ' | '
                         . $wallet->getDefence() . ' | '
                         . $wallet->getWorkPower() . ' |';
            ++$position;
        }
        $list = implode(PHP_EOL, $payload);

        return response()->json(['message' => $list]);
    }


    /**
     * TODO move logic to TransactionManager
     *
     * @return JsonResponse
     */
    public function undoTransaction(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        if ($wallet->name !== 'jakub') {
            return response()->json([
                                        'error' => 'Brak uprawnień.',
                                    ]);
        }
        $id = request()->get('id');
        try {
            /** @var Transaction $transaction */
            $transaction = Transaction::where('hash', $id)->firstOrFail();
            $transaction->source->balance += $transaction->value;
            $transaction->target->balance -= $transaction->value;
            $transaction->source->save();
            $transaction->target->save();
            $transaction->delete();
            return response()->json([
                                        'message' => 'Pomyślnie cofnięto transakcję #' . $id,
                                    ]);
        } catch (\Exception $e) {
            return response()->json([
                                        'error' => $e->getMessage(),
                                    ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function responsibleDisclosure(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $amount = 50;
        if (!$wallet) {
            return response()->json(['error' => 'Nie istnieje taki portfel.']);
        }
        try {
            $wizard = $this->walletRepository->getWalletForName('wizard');
            $this->transactionManager->sendToWallet($wizard, $wallet, $amount);

            return response()->json(['message' => $wallet->name
                                                  . 'otrzymuje nagrodę za odpowiedzialne zgłoszenie błędu.']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function keep(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $amount = request()->get('amount');
        if (!$wallet) {
            return response()->json(['error' => 'Nie istnieje taki portfel.']);
        }
        if (!$amount) {
            return response()->json(['error' => 'Nieprawidłowa kwota']);
        }

        try {
            $message = $this->transactionManager->keep($wallet, $amount);

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function retrieve(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $amount = request()->get('amount');
        if (!$wallet) {
            return response()->json(['error' => 'Nie istnieje taki portfel.']);
        }
        if (!$amount) {
            return response()->json(['error' => 'Nieprawidłowa kwota']);
        }

        try {
            $message = $this->transactionManager->retrieve($wallet, $amount);

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function safeStatus(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        if (!$wallet) {
            return response()->json(['error' => 'Nie istnieje taki portfel.']);
        }
        try {
            $message = $this->transactionManager->safeStatus($wallet);

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function hide(): ?JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            BMCGameEvents::hiddenStatusSet($wallet);
            return null;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function unhide(): ?JsonResponse
    {
        try {
            $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
            BMCGameEvents::hiddenStatusRemoved($wallet);
            return null;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function updateCabalState(): JsonResponse
    {
        try {
            $state = request()->get('state');
            if (!str_contains($state, 'current global metrics')) {
                throw new BMCEventException('Invalid Cabal Response, leaving old data intact');
            }
            $settings = Settings::instance();
            $settings->cabal_state = $state;
            $settings->processRawCabalState();
            $settings->save();
            return response()->json(['message' => 'Synced']);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @throws \Exception
     */
    private function validateEventRequest()
    {
        if (!request()->get('name')) {
            throw new \Exception('Invalid wallet.');
        }
    }

    /**
     * TODO: move out of BMC plugin
     */
    public function parseHelp()
    {
        $commands = request()->get('commands');
        \Log::info(print_r($commands, true));

        $generic = [];
        $bmc = [];
        $result = [];

        foreach ($commands as $command) {
            if (str_contains($command, '#BMC')) {
                $bmc[] = str_replace('#BMC', '', $command);
            } else {
                $generic[] = $command;
            }
        }
        foreach ($generic as $cmd) {
            $result[] = $cmd;
        }
        //foreach ($bmc as $cmd) {
        //    $result[] = $cmd;
        //}

        return response()->json(['message' => $result]);
    }

    /**
     * TODO: move out of BMC plugin
     */
    public function parseBMCHelp()
    {
        $commands = request()->get('commands');
        \Log::info(print_r($commands, true));

        $generic = [];
        $bmc = [];
        $result = [];

        foreach ($commands as $command) {
            if (str_contains($command, '#BMC')) {
                $bmc[] = str_replace('#BMC', '', $command);
            } else {
                $generic[] = $command;
            }
        }
        //foreach ($generic as $cmd) {
        //    $result[] = $cmd;
        //}
        $result[] = PHP_EOL . '---';
        $result[] = '### BMC :bmc:';
        foreach ($bmc as $cmd) {
            $result[] = $cmd;
        }

        $this->saveBmcCommands($bmc);

        return response()->json(['message' => $result]);
    }

    /**
     * @return JsonResponse
     * @deprecated
     */
    public function safeRules(): JsonResponse
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        if (!$wallet) {
            return response()->json(['error' => 'Nie istnieje taki portfel.']);
        }
        $settings = Settings::instance();
        $percent = $settings->safe_percent;

        try {
            $message = 'Sejf mnoży środki co 30 dni o ' . $percent . '%, pod kilkoma warunkami: ' . PHP_EOL;
            $message .= '- Wypłaty % są co 30 dni od pierwszej wpłaty.' . PHP_EOL;
            $message .= '- Każde wyciągnięcie pieniędzy z sejfu resetuje timer - % zostanie naliczony 30 dni po ostatniej wypłacie.' . PHP_EOL;
            $message .= '- Każda wpłata w trakcie cyklu rozliczeniowego trafia do puli oczekującej na kolejne rozliczenie i % od niej nie zostanie doliczony do najbliższego rozliczenia.' . PHP_EOL;
            $message .= 'Niech was Allah ma w opiece.';

            return response()->json(['message' => $message]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @throws \Exception
     */
    private function generateRandomString(int $length = 64): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    private function saveBmcCommands(array $bmc)
    {
        $settings = Settings::instance();
        $result = [];
        foreach($bmc as $command) {
            $cmd = explode(' - ', $command);
            $result[] = [
                'command' => Markdown::parse(str_replace(['<', '>'], ['[', ']'], $cmd[0])),
                'description' => Markdown::parse($cmd[1]),
            ];
        }
        $settings->help_content = json_encode($result);
        $settings->save();
    }
}
