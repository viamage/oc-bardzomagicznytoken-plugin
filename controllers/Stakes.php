<?php namespace JZ\BardzoMagicznyCoin\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Stakes Back-end Controller
 */
class Stakes extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relations.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('JZ.BardzoMagicznyCoin', 'bardzomagicznycoin', 'stakes');
    }
}
