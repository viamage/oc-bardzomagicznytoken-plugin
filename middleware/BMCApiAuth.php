<?php

namespace JZ\BardzoMagicznyCoin\Middleware;


use Closure;

class BMCApiAuth
{
    public function handle($request, Closure $next)
    {
        try {
            $this->authorize();
        } catch (\Exception $e) {
            return response()->json(['error' => 'INVALID TOKEN']);
        }

        return $next($request);
    }

    public function authorize()
    {
        $token = request()->header('x-mm-token');
        if ($token !== env('WIZARD_BMC_TOKEN')) {
            throw new \Exception('INVALID TOKEN');
        }
    }
}
