<?php namespace JZ\BardzoMagicznyCoin\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use JZ\BardzoMagicznyCoin\Classes\BMCChallengeManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Wallet;

/**
 *
 */
class BMCRankings extends ComponentBase
{
    /**
     * @var WalletRepository
     */
    private $walletRepo;
    /**
     * @var BMCChallengeManager
     */
    private $challengeManager;

    /**
     * @param CodeBase|null $cmsObject
     * @param array         $properties
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->walletRepo = app()->make(WalletRepository::class);
        $this->challengeManager = app()->make(BMCChallengeManager::class);
    }

    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name'        => 'BMCRankings Component',
            'description' => 'No description provided yet...'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function onRun()
    {
        $this->page['include_bots'] = $includeBots = (bool)get('include_bots', false);
        $this->page['main_ranking'] = $this->walletRepo->getRanking($includeBots);
    }

    public function onLoadMainRanking()
    {
        $includeBots = post('include_bots', false);
        $this->page['main_ranking'] = $this->walletRepo->getRanking($includeBots);
        $this->page['include_bots'] = $includeBots;
    }

    public function onLoadChallengesRanking()
    {
        $this->page['challenge_ranking'] = $this->challengeManager->getChallengeRanking();;
    }

    public function onLoadBetRanking()
    {
        $this->page['bet_ranking'] = $d = $this->walletRepo->getBetRanking();
    }

    public function onLoadSlaveRanking()
    {
        $this->page['slavery_ranking'] = $c = $this->walletRepo->getSlaveRanking();
    }
}
