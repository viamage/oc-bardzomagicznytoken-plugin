<?php namespace JZ\BardzoMagicznyCoin\Components;

use Cms\Classes\ComponentBase;
use JZ\BardzoMagicznyCoin\Models\Settings;

class BMCRulebook extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BMCRulebook Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $settings = Settings::instance();
        $helpContent = json_decode($settings->help_content, true);
        $this->page['slave_commands'] = $this->getSlaveCommands($helpContent);
        $this->page['stake_commands'] = $this->getStakeCommands($helpContent);
        $this->page['assault_commands'] = $this->getAssaultCommands($helpContent);
        $this->page['challenge_commands'] = $this->getChallengeCommands($helpContent);
        $this->page['commands'] = $helpContent;
    }

    private function getSlaveCommands($helpContent)
    {
        $result = [];
        foreach ($helpContent as $command) {
            if (str_contains($command['command'], ['nigger', 'slave', 'jew', 'holocaust'])) {
                $result[] = $command;
            }
        }
        return $result;
    }

    private function getStakeCommands($helpContent)
    {
        $result = [];
        foreach ($helpContent as $command) {
            if (str_contains($command['command'], ['stake'])) {
                $result[] = $command;
            }
        }
        return $result;
    }

    private function getAssaultCommands($helpContent)
    {
        $result = [];
        foreach ($helpContent as $command) {
            if (str_contains($command['command'], ['assault', 'switch-target'])) {
                $result[] = $command;
            }
        }
        return $result;
    }

    private function getChallengeCommands($helpContent)
    {
        $result = [];
        foreach ($helpContent as $command) {
            if (str_contains($command['command'], ['challenge', '<strong>accept</strong>', '<strong>reject</strong>'])) {
                $result[] = $command;
            }
        }
        return $result;
    }
}
