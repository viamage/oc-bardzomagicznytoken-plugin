<?php namespace JZ\BardzoMagicznyCoin\Components;

use Cms\Classes\ComponentBase;
use JZ\BardzoMagicznyCoin\Classes\BMCComponentBase;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use October\Rain\Exception\ApplicationException;

class BMCManager extends BMCComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BMCManager Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function onSendBMC()
    {
        $target = post('bmc_address');
        $amount = post('bmc_amount');
        $targetWallet = Wallet::where('name', $target)->first();
        $wallet = $this->getConnectedWallet();
        if ($amount > $wallet->balance) {
            throw new ApplicationException('Not enough funds');
        }
        if (!$targetWallet) {
            throw new ApplicationException('No such wallet');
        }
        /** @var TransactionManager $transactionManager */
        $transactionManager = app()->make(TransactionManager::class);
        $transactionManager->sendToWallet($wallet, $targetWallet, $amount, 'transfer', get('title', ''));
        $this->page['target'] = $target;
        $this->page['amount'] = $amount;
        $this->page['wallet'] = $wallet;
    }
}
