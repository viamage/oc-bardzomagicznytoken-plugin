<?php namespace JZ\BardzoMagicznyCoin\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use JZ\BardzoMagicznyCoin\Classes\BMCComponentBase;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Building;
use JZ\BardzoMagicznyCoin\ValueObjects\UIBuilding;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;

/**
 *
 */
class BMCBuildings extends BMCComponentBase
{
    /**
     * @var WalletRepository
     */
    private $walletRepository;
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @param CodeBase|null $cmsObject
     * @param array         $properties
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->walletRepository = app()->make(WalletRepository::class);
        $this->transactionManager = app()->make(TransactionManager::class);
    }

    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name'        => 'BMCBuildings Component',
            'description' => 'No description provided yet...'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onRun()
    {
        $wallet = $this->getConnectedWallet();
        $activeBuildingCount = 0;
        $buildingsIncome = 0;
        $buildings = [];
        $att = 0;
        $def = 0;
        $totalMp = 0;
        $usedMp = 0;
        $userMp = $wallet->getMaintenancePower();
        $availableMp = $wallet->getAvailableMaintenancePower();
        /** @var Building $building */
        foreach ($wallet->buildings as $building) {
            $b = new UIBuilding();
            $b->id = $building->id;
            $b->name = $building->name;
            $b->attack = $building->attack;
            $b->defence = $building->defence;
            $b->income = $building->income;
            $b->mp = $building->min_chinese;
            if($building->pivot->is_active){
                $b->pivot_active = true;
            }
            if ($wallet->isBuildingActive($building)) {
                $b->active = true;
                $activeBuildingCount++;
            }
            $totalMp += $building->min_chinese;
            if ($b->active) {
                $buildingsIncome += $building->income;
                $usedMp += $building->min_chinese;
                $att += $building->attack;
                $def += $building->defence;
            }
            $buildings[$building->id] = $b;
        }
        $this->page['available_buildings'] = $this->getAvailableBuildings($buildings);
        $buildingsMaintenance = 'Using ' . $usedMp . ' of ' . $userMp . ' available';
        $buildingStats = 'AP +' . $att . ', DEF +' . $def;
        $this->page['my_buildings'] = $buildings;
        $this->page['active_building_count'] = $activeBuildingCount;
        $this->page['building_stats'] = $buildingStats;
        $this->page['buildings_income'] = $buildingsIncome;
        $this->page['buildings_maintenance'] = $buildingsMaintenance;
        $this->page['available_mp'] = $availableMp;

    }

    /**
     *
     */
    public function onDeactivateBuilding()
    {
        $wallet = $this->getConnectedWallet();
        $building_id = request()->get('building_id'); // assuming building id is provided in request

        $building = $wallet->buildings()->find($building_id);
        if ($building) {
            $building->pivot->is_active = false;
            $building->pivot->save();
        }

        Flash::success('Deaktywowano budynek ' . $building->name . '.');
        return redirect()->refresh();
    }

    /**
     *
     */
    public function onActivateBuilding()
    {
        $wallet = $this->getConnectedWallet();
        $building_id = request()->get('building_id'); // assuming building id is provided in request

        $building = $wallet->buildings()->find($building_id);
        if ($building) {
            $building->pivot->is_active = true;
            $building->pivot->save();
        }

        Flash::success('Aktywowano budynek ' . $building->name . '.');
        return redirect()->refresh();
    }


    /**
     * @throws \Exception
     */
    public function onBuyBuilding()
    {
        $wallet = $this->getConnectedWallet();
        $wizard = $this->walletRepository->getWalletForName('wizard');
        $buildingId = request()->get('building_id');
        /** @var Building $building */
        $building = Building::find($buildingId);
        if (!$building) {
            throw new ApplicationException('Nie ma budynku z ID ' . $buildingId . '.');
        }
        if ($building->cost > $wallet->balance) {
            throw new ApplicationException('Nie stać cię.');
        }
        $alreadyBought = $wallet->buildings->where('name', $building->name)->count();
        if ($alreadyBought >= $building->max_amount) {
            throw new ApplicationException(
                'Nie możesz mieć więcej niż ' . $building->max_amount . ' budynków tego typu'
            );
        }
        $this->transactionManager->sendToWallet(
            $wallet,
            $wizard,
            $building->cost,
            'transfer',
            'Zakup budynku ' . $building->name
        );
        $building->wallets()->attach($wallet);
        Flash::success('Kupiłeś budynek ' . $building->name . '.');
        return redirect()->refresh();
    }

    private function getAvailableBuildings(array $buildings)
    {
        $alreadyBought = array_keys($buildings);
        return Building::where('is_visible', true)->whereNotIn('id', $alreadyBought)->get();
    }
}
