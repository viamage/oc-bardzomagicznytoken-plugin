<?php namespace JZ\BardzoMagicznyCoin\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use JZ\BardzoMagicznyCoin\Classes\BMCComponentBase;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use October\Rain\Support\Facades\Flash;

/**
 *
 */
class BMCSlaveSetup extends BMCCOmponentBase
{
    /**
     * @var SlaveryManager
     */
    private $sm;

    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name'        => 'BMCSlaveSetup Component',
            'description' => 'No description provided yet...'
        ];
    }

    /**
     * @param CodeBase|null $cmsObject
     * @param array         $properties
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->sm = app()->make(SlaveryManager::class);
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onRun()
    {
        $wallet = $this->getConnectedWallet();
        if (!$wallet->slave_setup) {
            $wallet->syncSlaveSetup();
        }
        if (!$wallet->chinese_setup) {
            $wallet->syncChinkSetup();
        }
        $niggerSetup = [
            'work'   => 0,
            'attack' => 0,
            'defend' => 0
        ];
        $chinkSetup = [
            'work'        => 0,
            'maintenance' => 0
        ];
        foreach ($wallet->slave_setup as $entry) {
            if ($entry['mode'] === SlaveryManager::NIGGER_WORK) {
                $niggerSetup['work'] = $entry['amount'];
            }
            if ($entry['mode'] === SlaveryManager::NIGGER_ATTACK) {
                $niggerSetup['attack'] = $entry['amount'];
            }
            if ($entry['mode'] === SlaveryManager::NIGGERS_DEFEND) {
                $niggerSetup['defend'] = $entry['amount'];
            }
        }
        foreach ($wallet->chinese_setup as $entry) {
            if ($entry['mode'] === SlaveryManager::CHINESE_WORK) {
                $chinkSetup['work'] = $entry['amount'];
            }
            if ($entry['mode'] === SlaveryManager::CHINESE_MAINTAIN) {
                $chinkSetup['mp'] = $entry['amount'];
            }
        }

        $this->page['nigger_setup'] = $niggerSetup;
        $this->page['chink_setup'] = $chinkSetup;
    }

    /**
     *
     */
    public function onChangeNiggerSetup()
    {
        $attack = post('attack');
        $def = post('defend');
        $work = post('work');
        $wallet = $this->getConnectedWallet();
        $this->sm->setNiggerSetup($wallet, $work, $attack, $def);
        Flash::success('Nigger setup changed.');
        return redirect()->refresh();
    }

    public function onChangeChinkSetup()
    {
        $mp = post('mp');
        $work = post('work');
        $wallet = $this->getConnectedWallet();
        $this->sm->setChinkSetup($wallet, $work, $mp);
        Flash::success('Chink setup changed.');
        return redirect()->refresh();
    }
}
