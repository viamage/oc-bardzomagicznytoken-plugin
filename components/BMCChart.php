<?php namespace JZ\BardzoMagicznyCoin\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

class BMCChart extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BMCChart Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        //$this->onLoadWeeklyChart();
    }

    public function onLoadYearlyChart()
    {
        $walletId = Session::get('wallet_id');
        $wallet = null;
        if ($walletId) {
            $wallet = Wallet::where('id', $walletId)->first();
        }

        if (!$wallet) {
            return redirect()->to('/bmc-signup');
        }
        $oneYearAgo = Carbon::now()->subYear()->toDateString();

        $transactions = Transaction::where(function ($q) use ($wallet) {
            $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
        })
                                   ->whereDate('created_at', '>=', $oneYearAgo)
                                   ->orderBy('created_at')
                                   ->get()
                                   ->groupBy(function ($transaction) {
                                       // Group by date of transaction
                                       return $transaction->created_at->toDateString();
                                   })
                                   ->map(function ($transactions) {
                                       // Return the last transaction of each day
                                       return $transactions->last();
                                   });
        /** @var Collection $dataset */
        $dataset = $transactions->map(function ($transaction) use ($wallet) {
            // If the transaction's source is the wallet, use source_balance, otherwise use target_balance
            $balance = $transaction->source_id == $wallet->id ? $transaction->source_balance : $transaction->target_balance;

            // Return the transaction date and balance as an associative array
            return [
                'x' => $transaction->created_at->toDateString(),
                'y' => $balance
            ];
        });

// Convert to JSON for use in Chart.js
        $this->page['yearly_chart_data'] = $dataset->values()->toJson();
    }

    public function onLoadTrimonthlyChart()
    {
        $walletId = Session::get('wallet_id');
        $wallet = null;
        if ($walletId) {
            $wallet = Wallet::where('id', $walletId)->first();
        }

        if (!$wallet) {
            return redirect()->to('/bmc-signup');
        }
        $oneYearAgo = Carbon::now()->subMonths(3)->toDateString();

        $transactions = Transaction::where(function ($q) use ($wallet) {
            $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
        })
                                   ->whereDate('created_at', '>=', $oneYearAgo)
                                   ->orderBy('created_at')
                                   ->get()
                                   ->groupBy(function ($transaction) {
                                       // Group by date of transaction
                                       return $transaction->created_at->format('Y-m-d H');
                                   })
                                   ->map(function ($transactions) {
                                       // Return the last transaction of each day
                                       return $transactions->last();
                                   });
        /** @var Collection $dataset */
        $dataset = $transactions->map(function ($transaction) use ($wallet) {
            // If the transaction's source is the wallet, use source_balance, otherwise use target_balance
            $balance = $transaction->source_id == $wallet->id ? $transaction->source_balance : $transaction->target_balance;

            // Return the transaction date and balance as an associative array
            return [
                'x' => $transaction->created_at->toDateTimeString(),
                'y' => $balance
            ];
        });

// Convert to JSON for use in Chart.js
        $this->page['trimonthly_chart_data'] = $dataset->values()->toJson();
    }

    public function onLoadMonthlyChart()
    {
        $walletId = Session::get('wallet_id');
        $wallet = null;
        if ($walletId) {
            $wallet = Wallet::where('id', $walletId)->first();
        }

        if (!$wallet) {
            return redirect()->to('/bmc-signup');
        }
        $oneYearAgo = Carbon::now()->subMonths(1)->toDateString();

        $transactions = Transaction::where(function ($q) use ($wallet) {
            $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
        })
                                   ->whereDate('created_at', '>=', $oneYearAgo)
                                   ->orderBy('created_at')
                                   ->get()
                                   ->groupBy(function ($transaction) {
                                       // Group by date of transaction
                                       return $transaction->created_at->format('Y-m-d H');
                                   })
                                   ->map(function ($transactions) {
                                       // Return the last transaction of each day
                                       return $transactions->last();
                                   });
        /** @var Collection $dataset */
        $dataset = $transactions->map(function ($transaction) use ($wallet) {
            // If the transaction's source is the wallet, use source_balance, otherwise use target_balance
            $balance = $transaction->source_id == $wallet->id ? $transaction->source_balance : $transaction->target_balance;

            // Return the transaction date and balance as an associative array
            return [
                'x' => $transaction->created_at->toDateTimeString(),
                'y' => $balance
            ];
        });

// Convert to JSON for use in Chart.js
        $this->page['monthly_data'] = $dataset->values()->toJson();
    }

    public function onLoadWeeklyChart()
    {
        $walletId = Session::get('wallet_id');
        $wallet = null;
        if ($walletId) {
            $wallet = Wallet::where('id', $walletId)->first();
        }

        if (!$wallet) {
            return redirect()->to('/bmc-signup');
        }
        $oneYearAgo = Carbon::now()->subWeek()->toDateString();

        $transactions = Transaction::where(function ($q) use ($wallet) {
            $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
        })
                                   ->whereDate('created_at', '>=', $oneYearAgo)
                                   ->orderBy('created_at')
                                   ->get()
                                   ->groupBy(function ($transaction) {
                                       // Group by date of transaction
                                       return $transaction->created_at->format('Y-m-d H');
                                   })
                                   ->map(function ($transactions) {
                                       // Return the last transaction of each day
                                       return $transactions->last();
                                   });
        /** @var Collection $dataset */
        $dataset = $transactions->map(function ($transaction) use ($wallet) {
            // If the transaction's source is the wallet, use source_balance, otherwise use target_balance
            $balance = $transaction->source_id == $wallet->id ? $transaction->source_balance : $transaction->target_balance;

            // Return the transaction date and balance as an associative array
            return [
                'x' => $transaction->created_at->toDateTimeString(),
                'y' => $balance
            ];
        });

// Convert to JSON for use in Chart.js
        $this->page['weekly_data'] = $dataset->values()->toJson();
    }
}
