<?php namespace JZ\BardzoMagicznyCoin\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;

class MattermostOnlyPage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'MattermostOnlyPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $walletId = Session::get('wallet_id');
        if (!$walletId) {
            return redirect()->to('/bmc-signup');
        }
    }
}
