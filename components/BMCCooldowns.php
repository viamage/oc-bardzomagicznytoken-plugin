<?php namespace JZ\BardzoMagicznyCoin\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use JZ\BardzoMagicznyCoin\Classes\AssaultManager;
use JZ\BardzoMagicznyCoin\Classes\BMCComponentBase;
use JZ\BardzoMagicznyCoin\Classes\Spellbook;
use JZ\BardzoMagicznyCoin\Exceptions\BMCUIException;
use JZ\BardzoMagicznyCoin\Models\Assault;
use JZ\BardzoMagicznyCoin\Models\MagicAction;

class BMCCooldowns extends BMCComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BMCCooldowns Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        try {
            $wallet = $this->getConnectedWallet();
        } catch (BMCUIException $e) {
            $wallet = null;
        }
        if (!$wallet) {
            return;
        }
        $this->page['transport'] = $transport = $wallet->slaveTransports->first();
        $transportCD = 0;
        if ($transport) {
            $transportCD = (180 * 60) - Carbon::now()->diffInSeconds($transport->created_at);
        }
        try {
            $spellCd = $this->getSpellCD();
        } catch (\Exception $e) {
            $spellCd = 0;
        }
        $hideCD = 0;
        if ($wallet->hidden_at) {
            $hideCD = (30 * 60) - Carbon::now()->diffInSeconds($wallet->hidden_at);
        }
        $lastAssault = Assault::where('boss_id', $wallet->id)->orderBy('created_at', 'desc')->first();
        $assaultCD = 0;
        if ($lastAssault) {
            $assaultCD = (AssaultManager::ASSAULT_CD * 60) - Carbon::now()->diffInSeconds($lastAssault->created_at);
        }

        $this->page['spell_cd'] = $spellCd;
        $this->page['transport_cd'] = $transportCD;
        $this->page['hide_cd'] = $hideCD;
        $this->page['assault_cd'] = $assaultCD;
    }


    public function getSpellCD()
    {
        $limitDate = (Carbon::now())->subHours(Spellbook::SPELLS_TIMEOUT);
        $wallet = $this->getConnectedWallet();
        if ($magicAction = MagicAction::where('wallet_id', $wallet->id)
                                      ->where('created_at', '>', $limitDate)->first()) {
            $secondsRemaining = (Spellbook::SPELLS_TIMEOUT * 60 * 60) - (Carbon::now())->diffInSeconds($magicAction->created_at);
            return $secondsRemaining;
        }
        return 0;
    }
}
