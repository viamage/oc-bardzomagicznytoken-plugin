<?php namespace JZ\BardzoMagicznyCoin\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use October\Rain\Exception\ApplicationException;

class BMCWallet extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Transactions Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'wallet' => [
                'title'       => 'Wallet',
                'description' => 'Wallet',
                'default'     => '{{ :wallet }}',
                'type'        => 'string',
            ],
        ];
    }

    public function onRun()
    {
        $wallet = Wallet::where('hash', $this->property('wallet'))->first();
        if (!$wallet) {
            $walletId = Session::get('wallet_id');
            if ($walletId) {
                $wallet = Wallet::where('id', $walletId)->first();
            }
        }
        if (!$wallet) {
            return redirect()->to('/bmc-signup');
        }
        $this->page['wallet'] = $wallet;
        $this->page['from'] = $from = get('from', Carbon::now()->startOfDay()->toDateString());
        $this->page['to'] = $to = get('to', Carbon::now()->endOfDay()->toDateString());
        $this->page['type'] = $type = get('type', 'all');
        $this->page['hash'] = $hash = get('hash', '');
        $from = (new Carbon($from))->startOfDay();
        $to = (new Carbon($to))->endOfDay();
        if (!in_array($type, ['all', 'incoming', 'outgoing'])) {
            throw new ApplicationException('Invalid type');
        }

        if ($type === 'all') {
            $transactions = Transaction::where(function ($q) use ($wallet) {
                $q->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
            });
        }
        if ($type === 'incoming') {
            $transactions = Transaction::where(function ($q) use ($wallet) {
                $q->where('target_id', $wallet->id);
            });
        }
        if ($type === 'outgoing') {
            $transactions = Transaction::where(function ($q) use ($wallet) {
                $q->where('source_id', $wallet->id);
            });
        }
        if ($hash) {
            $transactions = $transactions->where('hash', $hash);
        }
        $jewIncome = $wallet->calculateJewIncome();

        $this->page['jew_income'] = $jewIncome;
        $this->page['filters'] = '?from=' . $from->toDateString() . '&to=' . $to->toDateString() . '&type=' . $type .'&hash='.$hash;
        $this->page['transactions'] = $transactions->where('created_at', '>=', $from)
                                                   ->where('created_at', '<=', $to)
                                                   ->orderBy('created_at', 'desc')->paginate();

        return;
    }
}
