<?php namespace JZ\BardzoMagicznyCoin\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use JZ\BardzoMagicznyCoin\Classes\BMCComponentBase;
use JZ\BardzoMagicznyCoin\Classes\Spellbook;
use JZ\BardzoMagicznyCoin\Classes\TransactionManager;
use JZ\BardzoMagicznyCoin\Exceptions\SpellException;
use JZ\BardzoMagicznyCoin\Models\MagicAction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;

class MattermostConnector extends BMCComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'MattermostConnector Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {

    }

    public function onConnect()
    {
        $code = post('code');
        if (!$code) {
            throw new ApplicationException('Invalid Code');
        }
        $wallet = Wallet::where('ui_code', $code)->first();
        if (!$wallet) {
            throw new ApplicationException('Invalid Code');
        }
        Session::put('wallet_id', $wallet->id);
        return redirect()->to('/bmc');
    }

    public function onDisconnectMattermost()
    {
        Session::forget('wallet_id');
        return redirect()->to('/bmc-signup');
    }

    public function onSendToSubaccount()
    {
        $targets = post('send');
        foreach ($targets as $target => $amount) {
            $targetWallet = Wallet::where('name', $target)->first();
            $wallet = $this->getConnectedWallet();
            if ($amount > $wallet->balance) {
                throw new ApplicationException('Not enough funds');
            }
            if (!$targetWallet) {
                throw new ApplicationException('No such wallet');
            }
            /** @var TransactionManager $transactionManager */
            $transactionManager = app()->make(TransactionManager::class);
            $transactionManager->sendToWallet($wallet, $targetWallet, $amount, 'transfer', get('title', 'Quick Send'));
        }
        Flash::success('Sent successfully!');
        return redirect()->refresh();
    }

    public function onGetFromSubaccount()
    {
        $wallet = $this->getConnectedWallet();
        $sources = post('get');
        foreach ($sources as $source => $amount) {
            $sourceWallet = Wallet::where('name', $source)->first();
            if ($sourceWallet->parent_id !== $wallet->id) {
                throw new ApplicationException('Not your wallet');
            }
            if ($amount > $sourceWallet->balance) {
                throw new ApplicationException('Not enough funds');
            }
            if (!$sourceWallet) {
                throw new ApplicationException('No such wallet');
            }
            /** @var TransactionManager $transactionManager */
            $transactionManager = app()->make(TransactionManager::class);
            $transactionManager->sendToWallet($sourceWallet, $wallet, $amount, 'transfer', get('title', 'Quick Get'));
        }
        Flash::success('Sent successfully!');
        return redirect()->refresh();
    }

    public function onSwitchToSubaccount()
    {
        $targetWallet = post('wallet');
        $parentWallet = $this->getConnectedWallet();
        $wallet = Wallet::where('name', $targetWallet)->first();
        if (!$wallet) {
            throw new ApplicationException('No such wallet');
        }
        if ($wallet->parent_id !== $parentWallet->id) {
            throw new ApplicationException('Not your wallet');
        }
        Session::put('wallet_id', $wallet->id);
        return redirect()->refresh();
    }

    public function onGoToParentWallet(){
        $wallet = $this->getConnectedWallet();
        $parentWallet = $wallet->parent;
        if(!$parentWallet){
            throw new ApplicationException('No parent wallet');
        }
        Session::put('wallet_id', $parentWallet->id);
        return redirect()->refresh();
    }

}
