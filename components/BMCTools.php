<?php namespace JZ\BardzoMagicznyCoin\Components;

use Carbon\Carbon;
use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Validator;
use JZ\BardzoMagicznyCoin\Classes\AssaultManager;
use JZ\BardzoMagicznyCoin\Classes\BMCComponentBase;
use JZ\BardzoMagicznyCoin\Classes\BMCGameEvents;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Classes\StakesManager;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Jobs\GasChamberJob;
use JZ\BardzoMagicznyCoin\Models\Assault;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\SlaveTrade;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\Apparatus\Classes\JobManager;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;

/**
 *
 */
class BMCTools extends BMCComponentBase
{
    /**
     * @var SlaveryManager
     */
    private $slaveryManager;

    /**
     * @var StakesManager
     */
    private $stakeManager;

    /**
     * @var WalletRepository
     */
    private $walletRepository;
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var AssaultManager
     */
    private $assaultManager;

    /**
     * @param CodeBase|null $cmsObject
     * @param array         $properties
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->slaveryManager = app()->make(SlaveryManager::class);
        $this->stakeManager = app()->make(StakesManager::class);
        $this->walletRepository = app()->make(WalletRepository::class);
        $this->transactionRepository = app()->make(TransactionRepository::class);
        $this->assaultManager = app()->make(AssaultManager::class);
    }

    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name'        => 'BMCTools Component',
            'description' => 'No description provided yet...'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     *
     */
    public function onRun()
    {
        $wallet = $this->getConnectedWallet();
        $this->page['slaves'] = SlaveTrade::where('is_closed', false)->get();
        $this->page['total_gassed'] = Wallet::select('gassed_jews')->sum('gassed_jews');
        $this->page['all_jews_dead'] = BMCGameEvents::areJewsDead();
        $this->page['stakes'] = $stakes = Stake::orderBy('created_at', 'desc')->where(function ($q) {
            $q->where('is_finished', false)->orWhere('created_at', '>=', Carbon::now()->subMonth());
        })->get();
        $stakeRates = [];
        foreach ($stakes as $stake) {
            $stakeRates[$stake->id] = $this->stakeManager->calculateStakeRates($stake, false);
        }
        $this->page['stakes_rates'] = $stakeRates;
        $this->page['assaults'] = Assault::where(function ($q) use ($wallet) {
            $q->whereHas('wallets', function ($q) use ($wallet) {
                $q->where('id', $wallet->id);
            });
        })->where('is_finished', false)->whereHas('wallets')->get();
        $this->page['invited_assaults'] = Assault::where(function ($q) use ($wallet) {
            $q->whereHas('invited', function ($q) use ($wallet) {
                $q->where('id', $wallet->id);
            })->orWhere('is_public', true);
        })->whereDoesntHave('wallets', function ($q) use ($wallet) {
            $q->where('id', $wallet->id);
        })->where('is_finished', false)->whereHas('wallets')->get();
        $this->page['wallets'] = $this->walletRepository->getWhitelistedWallets();
    }

    public function onLeaveAssault()
    {
        $wallet = $this->getConnectedWallet();
        $assault = Assault::find(post('assault_id'));
        $this->assaultManager->leaveAssault($assault, $wallet);
        Flash::success('Successfully left assault');
        return redirect()->refresh();
    }

    public function onJoinAssault()
    {
        $wallet = $this->getConnectedWallet();
        $assault = Assault::find(post('assault_id'));
        $amount = post('amount');
        $this->assaultManager->joinAssault($assault, $wallet, $amount);
        Flash::success('Successfully joined assault');
        return redirect()->refresh();
    }

    public function onCreateAssault()
    {
        $wallet = $this->getConnectedWallet();
        $target = $this->walletRepository->getWalletForName(post('target'));
        if (!$target) {
            throw new ApplicationException('Invalid target');
        }
        $targetIds = post('invited');
        $targets = Wallet::whereIn('id', $targetIds)->where('is_bot', false)->where('is_blacklisted', false)->get();
        $public = (bool)post('is_public');
        $assault = $this->assaultManager->createAssault($wallet, $target, post('amount'), $public);
        if($targets->count()) {
            $this->assaultManager->inviteToAssault($targets, $assault, $wallet);
        }
        Flash::success('Successfully created assault');
        return redirect()->refresh();
    }

    public function onInviteToAssault()
    {
        $wallet = $this->getConnectedWallet();
        $targetIds = post('target_ids');
        $targets = Wallet::whereIn('id', $targetIds)->where('is_bot', false)->where('is_blacklisted', false)->get();
        $assault = Assault::find(post('assault_id'));
        $this->assaultManager->inviteToAssault($targets, $assault, $wallet);
        Flash::success('Successfully invited people');
        return redirect()->refresh();
    }

    public function onCancelAssault()
    {
        $wallet = $this->getConnectedWallet();
        $assault = Assault::find(post('assault_id'));
        if (!$assault) {
            throw new ApplicationException('Invalid target');
        }
        $assault = $this->assaultManager->cancelAssault($wallet, $assault);
        Flash::success('Successfully cancelled assault');
        return redirect()->refresh();
    }

    public function onCommenceAssault()
    {
        $wallet = $this->getConnectedWallet();
        $assault = Assault::find(post('assault_id'));
        if (!$assault) {
            throw new ApplicationException('Invalid target');
        }
        $assault = $this->assaultManager->commenceAssault($assault, $wallet);
        if($assault->result === 1){
            Flash::success('Assault succeeded!');
        } else {
            Flash::error('Assault failed :(');
        }

        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws ApplicationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onVote()
    {
        $wallet = $this->getConnectedWallet();
        $stakeId = post('stake_id');
        $option = post('option_id');
        $amount = post('amount');
        $stake = Stake::find($stakeId);
        if (!$stake) {
            throw new ApplicationException('Invalid stake');
        }
        if ($amount > $wallet->balance) {
            throw new ApplicationException('Not enough balance');
        }
        $this->stakeManager->voteForStake($wallet, $stake, $option, $amount);

        Flash::success('Successfully voted');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws TransactionException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onCreateStake()
    {
        $data = request()->all();
        $this->validateCreateStakePayload($data);
        $wallet = $this->getConnectedWallet();
        $options = $this->parseOptions($data['options']);
        $this->stakeManager->createStake($wallet, $data['label'], $options);
        Flash::success('Successfully created stake');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws TransactionException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onFinishStake()
    {
        $winOption = post('win_option');
        $id = post('stake_id');
        $wallet = $this->getConnectedWallet();
        $stake = Stake::where('id', $id)->where('is_finished', false)->first();
        if (!$stake) {
            throw new TransactionException('Stake not found');
        }
        if ($wallet->id !== $stake->wallet_id) {
            throw new TransactionException('You are not the creator of this stake');
        }
        $wonCount = $this->stakeManager->finishStake($stake, $winOption);
        Flash::success('Successfully finished stake. ' . $wonCount . ' bids won!');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws TransactionException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onCloseStake()
    {
        $id = post('stake_id');
        $wallet = $this->getConnectedWallet();
        $stake = Stake::where('id', $id)->where('is_finished', false)->first();
        if (!$stake) {
            throw new TransactionException('Stake not found');
        }
        if ($wallet->id !== $stake->wallet_id) {
            throw new TransactionException('You are not the creator of this stake');
        }
        $this->stakeManager->closeStake($stake);
        Flash::success('Successfully closed stake.');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws TransactionException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onOpenStake()
    {
        $id = post('stake_id');
        $wallet = $this->getConnectedWallet();
        $stake = Stake::where('id', $id)->where('is_finished', false)->first();
        if (!$stake) {
            throw new TransactionException('Stake not found');
        }
        if ($wallet->id !== $stake->wallet_id) {
            throw new TransactionException('You are not the creator of this stake');
        }
        $this->stakeManager->openStake($stake);
        Flash::success('Successfully opened stake');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws ApplicationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onCancelTrade()
    {
        $id = post('trade_id');
        $offer = SlaveTrade::find($id);
        $wallet = $this->getConnectedWallet();

        if (!$offer) {
            throw new ApplicationException('Invalid trade');
        }
        $this->slaveryManager->cancelSellSlave($wallet, $offer->id);
        Flash::success('Successfully cancelled trade');
        return redirect()->refresh();
    }

    /**
     *
     */
    public function onBuyTrade()
    {
        $id = post('trade_id');
        $offer = SlaveTrade::find($id);
        $wallet = $this->getConnectedWallet();

        if (!$offer) {
            throw new ApplicationException('Invalid trade');
        }
        $type = request()->get('type');
        $this->slaveryManager->buySlave($wallet, $offer->id, $type);
        Flash::success('Successfully bought slave!');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws ApplicationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     */
    public function onSellSlave()
    {
        $wallet = $this->getConnectedWallet();
        $amount = post('niggers_amount');
        $price = post('niggers_price');
        $buyerName = post('niggers_buyer');
        $type = post('type');
        $buyer = null;
        if ($buyerName) {
            $buyer = Wallet::where('name', $buyerName)->first();
            if (!$buyer) {
                throw new ApplicationException('Buyer wallet does not exists');
            }
        }
        if($type === 'nigger') {
            $offer = $this->slaveryManager->sellNigger($wallet, $amount, $price, $buyer);
        } elseif($type === 'chinese'){
            $offer = $this->slaveryManager->sellChink($wallet, $amount, $price, $buyer);
        } else {
            throw new ApplicationException('Unknown type');
        }
        Flash::success('Successfully created trade');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function onGasJew()
    {
        $settings = Settings::instance();
        $jewPrice = $settings->get('jew_price', 1000);
        $wallet = $this->getConnectedWallet();
        $this->slaveryManager->gasJew($wallet, $jewPrice);
        Flash::success('Successfully gassed jew!');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws ApplicationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function onHireJew()
    {
        $settings = Settings::instance();
        $jewPrice = $settings->get('jew_price', 1000);
        $wallet = $this->getConnectedWallet();
        if ($wallet->balance < $jewPrice) {
            throw new ApplicationException('Nie stać cię. Żyd kosztuje ' . $jewPrice . ' :bmc:');
        }
        $this->slaveryManager->hireJew($wallet, $jewPrice);
        Flash::warning('Successfully hired jew!');
        return redirect()->refresh();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|mixed|object
     * @throws ApplicationException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\BMCUIException
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function onHolocaust()
    {
        $wallet = $this->getConnectedWallet();
        $settings = Settings::instance();
        $jewPrice = $settings->get('jew_price', 1000);
        $amount = post('jews_amount');
        if (!is_numeric($amount) || $amount < 1) {
            throw new ApplicationException('Bad amount');
        }
        $jewsPrice = $jewPrice * $amount;
        if ($wallet->balance < $jewsPrice) {
            throw new ApplicationException('Too expensive. You need to invest ' . $jewsPrice . ' :bmc:');
        }
        $wizardWallet = Wallet::where('name', 'wizard')->first();
        $this->walletRepository->deductFromWalletBalance($wallet, $jewsPrice);
        $this->walletRepository->addToWalletBalance($wizardWallet, $jewsPrice);

        $this->transactionRepository->createTransaction(
            $wallet,
            $wizardWallet,
            $jewsPrice,
            'transfer',
            'Holocaust Investment'
        );
        sleep(1);
        $this->slaveryManager->gasJew($wallet, $jewPrice, $amount, false);

        Flash::success('Thank you for joining holocaust.');
        return redirect()->refresh();
    }

    /**
     * @param array $data
     */
    private function validateCreateStakePayload(array $data)
    {
        $rules = [
            'label'   => 'required',
            'options' => 'required',
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            throw new TransactionException($v->errors()->first());
        }
    }

    /**
     * @param $options
     */
    private function parseOptions($options)
    {
        $parsedOptions = [];
        $count = 1;
        foreach ($options as $option) {
            if (trim($option)) {
                $parsedOptions[] = [
                    'id'    => $count,
                    'label' => trim($option)
                ];
                ++$count;
            }
        }
        return $parsedOptions;
    }
}
