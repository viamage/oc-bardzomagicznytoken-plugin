<?php

namespace JZ\BardzoMagicznyCoin\Interfaces;

use JZ\BardzoMagicznyCoin\Models\Wallet;
use October\Rain\Database\Collection;

interface WalletRepository
{
    public function getWalletForName(string $name): ?Wallet;

    public function getWhitelistedWallets(): Collection;

    public function getPoorestWhitelistedWallets(int $amount): Collection;

    public function addToWalletBalance(Wallet $wallet, int $amount): Wallet;

    public function deductFromWalletBalance(Wallet $wallet, int $amount): Wallet;

    public function createWallet(string $name, int $balance): Wallet;

    public function getRanking(bool $includeBots = false): Collection;

    public function getAllWallets(): Collection;

    public function getBetRanking();

    public function getSlaveRanking();

    public function getWalletsForNames(array $names);
}
