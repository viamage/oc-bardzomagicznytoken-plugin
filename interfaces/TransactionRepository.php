<?php

namespace JZ\BardzoMagicznyCoin\Interfaces;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

interface TransactionRepository
{
    public function getTransactionsForWallet(Wallet $wallet, Carbon $from = null, Carbon $to = null): Collection;

    public function createTransaction(Wallet $source, Wallet $target, int $amount, string $type = 'transfer', string $title = null): Transaction;

    public function getWonBetsForWallet(?Wallet $wallet): Collection;

    public function getLostBetsForWallet(?Wallet $wallet): Collection;

    public function getWonChallengesForWallet(Wallet $wallet): Collection;

    public function getLostChallengesForWallet(Wallet $wallet): Collection;
}
