<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class BigIntValues2 extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_transactions',
            function (Blueprint $table) {
                $table->bigInteger('value')->change();
            }
        );

    }

    public function down()
    {

    }
}
