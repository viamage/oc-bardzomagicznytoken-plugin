<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddDatePivot extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_assaults_teams',
            function (Blueprint $table) {
                $table->dateTime('date')->after('strength');
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_assaults_teams',
            function (Blueprint $table) {
                $table->dropColumn('date');
            }
        );
    }
}
