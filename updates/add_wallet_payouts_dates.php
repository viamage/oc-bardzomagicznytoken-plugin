<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Illuminate\Support\Carbon;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddWalletPayoutsDates extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->boolean('is_hidden')->after('is_blacklisted')->default(false);
                $table->integer('payout_hour')->after('niggers')->nullable();
                $table->dateTime('last_payout_at')->after('niggers')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->dropColumn('is_hidden');
                $table->dropColumn('payout_hour');
                $table->dropColumn('last_payout_at');
            }
        );
    }
}
