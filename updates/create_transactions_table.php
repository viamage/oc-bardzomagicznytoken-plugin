<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateTransactionsTable Migration
 */
class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash', 255);
            $table->integer('source_id');
            $table->integer('target_id');
            $table->integer('value');
            $table->string('type')->default('transfer');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_transactions');
    }
}
