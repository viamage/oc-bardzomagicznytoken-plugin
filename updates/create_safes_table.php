<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateSafesTable Migration
 */
class CreateSafesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_safes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wallet_id');
            $table->integer('value');
            $table->integer('pending_value')->default(0);
            $table->integer('percent')->default(5);
            $table->dateTime('accounting_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_safes');
    }
}
