<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddTransactionTitle extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_transactions',
            function (Blueprint $table) {
                $table->string('title', 255)->after('type')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_transactions',
            function (Blueprint $table) {
                $table->dropColumn('title');
            }
        );
    }
}
