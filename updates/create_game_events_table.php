<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGameEventsTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_game_events', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('type')->index();
            $table->integer('triggered_by_id')->index();
            $table->integer('length')->default(24);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_game_events');
    }
}
