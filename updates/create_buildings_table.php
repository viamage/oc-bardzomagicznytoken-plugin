<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBuildingsTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_buildings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('attack')->default(0);
            $table->integer('defence')->default(0);
            $table->integer('income')->default(0);
            $table->integer('cost');
            $table->integer('min_chinese')->default(0);
            $table->integer('min_niggers')->default(0);
            $table->boolean('is_visible')->default(false);
            $table->boolean('requires_slaves')->default(true);
            $table->timestamps();
        });
        Schema::create('jz_bardzomagicznycoin_buildings_wallets', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('wallet_id');
            $table->integer('building_id');
            $table->integer('amount')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_buildings');
        Schema::dropIfExists('jz_bardzomagicznycoin_buildings_wallets');
    }
}
