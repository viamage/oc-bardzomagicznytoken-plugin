<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSlaveTradesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_slave_trades', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('seller_id');
            $table->integer('buyer_id')->nullable();
            $table->integer('amount');
            $table->integer('price');
            $table->string('type')->default('nigger');
            $table->boolean('is_closed')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_slave_trades');
    }
}
