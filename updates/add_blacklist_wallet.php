<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddBlacklistWallet extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->boolean('is_blacklisted')->after('balance')->default(false);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->dropColumn('is_blacklisted');
            }
        );
    }
}
