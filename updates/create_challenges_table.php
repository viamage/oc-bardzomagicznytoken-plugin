<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateChallengesTable Migration
 */
class CreateChallengesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash', 255);
            $table->integer('challenger_id');
            $table->integer('challenged_id');
            $table->integer('amount');
            $table->boolean('accepted')->default(false);
            $table->integer('winner_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_challenges');
    }
}
