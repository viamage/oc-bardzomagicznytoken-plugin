<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddAliasesWallet extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->string('alias')->after('is_blacklisted')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->dropColumn('alias');
            }
        );
    }
}
