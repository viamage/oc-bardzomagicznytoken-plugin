<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class WalletBalanceBigint extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_transactions',
            function (Blueprint $table) {
                $table->bigInteger('source_balance')->change();
                $table->bigInteger('target_balance')->change();
            }
        );
    }

    public function down()
    {
    }
}
