<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateMagicActionsTable Migration
 */
class CreateMagicActionsTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_magic_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wallet_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_magic_actions');
    }
}
