<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddAliasesWallet extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_buildings_wallets',
            function (Blueprint $table) {
                $table->boolean('is_active')->after('amount')->default(true)->nullable();
            }
        );
        Schema::table(
            'jz_bardzomagicznycoin_buildings',
            function (Blueprint $table) {
                $table->integer('max_amount')->after('cost')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_buildings_wallets',
            function (Blueprint $table) {
                $table->dropColumn('is_active');
            }
        );
        Schema::table(
            'jz_bardzomagicznycoin_buildings',
            function (Blueprint $table) {
                $table->dropColumn('max_amount');
            }
        );
    }
}
