<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePlayerStakesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_player_stakes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('wallet_id');
            $table->integer('stake_id');
            $table->integer('option');
            $table->integer('amount');
            $table->integer('win_amount')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_player_stakes');
    }
}
