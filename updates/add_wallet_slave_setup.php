<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Illuminate\Support\Carbon;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddWalletSlaveSetup extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->longText('slave_setup')->after('niggers')->nullable();
            }
        );
        foreach(Wallet::all() as $wallet){
            $wallet->last_payout_at = (Carbon::now());
            $wallet->save();
        }
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->dropColumn('slave_setup');
            }
        );
    }
}
