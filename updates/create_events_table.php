<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateEventsTable Migration
 */
class CreateEventsTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->date('date');
            $table->integer('prize')->default(100);
            $table->text('notification_message')->nullable();
            $table->boolean('is_enabled')->default(false);
            $table->timestamps();
        });

        Schema::create('jz_bardzomagicznycoin_events_wallets', function (Blueprint $table) {
            $table->integer('wallet_id');
            $table->integer('event_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_events');
        Schema::dropIfExists('jz_bardzomagicznycoin_events_wallets');
    }
}
