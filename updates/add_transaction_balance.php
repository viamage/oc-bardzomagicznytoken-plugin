<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddTransactionBalance extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_transactions',
            function (Blueprint $table) {
                $table->integer('source_balance')->after('title')->nullable();
                $table->integer('target_balance')->after('source_balance')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_transactions',
            function (Blueprint $table) {
                $table->dropColumn('source_balance');
                $table->dropColumn('target_balance');
            }
        );
    }
}
