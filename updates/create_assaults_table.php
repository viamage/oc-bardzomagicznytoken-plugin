<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAssaultsTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_assaults', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code')->nullable();
            $table->integer('target_id')->index();
            $table->boolean('is_finished')->default(false);
            $table->integer('boss_id');
            $table->integer('result')->nullable();
            $table->boolean('is_public')->default(false);
            $table->timestamps();
        });

        Schema::create('jz_bardzomagicznycoin_assaults_teams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('assault_id')->index();
            $table->integer('wallet_id')->index();
            $table->integer('value');
            $table->integer('strength');
        });

        Schema::create('jz_bardzomagicznycoin_assaults_invited', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('assault_id')->index();
            $table->integer('wallet_id')->index();
        });

        Schema::table('jz_bardzomagicznycoin_wallets', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('niggers_mode')->after('gassed_jews')->default(false);
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_assaults');
        Schema::dropIfExists('jz_bardzomagicznycoin_assaults_teams');
        Schema::dropIfExists('jz_bardzomagicznycoin_assaults_invited');
        Schema::table('jz_bardzomagicznycoin_wallets', function (Blueprint $table) {
            $table->dropColumn('niggers_mode');
        });
    }
}
