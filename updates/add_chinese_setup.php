<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddChineseSetup extends Migration
{
    public function up()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->text('chinese_setup')->after('slave_setup')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'jz_bardzomagicznycoin_wallets',
            function (Blueprint $table) {
                $table->dropColumn('chinese_setup');
            }
        );
    }
}
