<?php namespace JZ\BardzoMagicznyCoin\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStakesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_bardzomagicznycoin_stakes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('label', 255);
            $table->text('options');
            $table->integer('wallet_id');
            $table->boolean('is_open')->default(true);
            $table->integer('win_option')->nullable();
            $table->boolean('is_finished')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_bardzomagicznycoin_stakes');
    }
}
