<?php namespace JZ\BardzoMagicznyCoin;

use Backend;
use JZ\BardzoMagicznyCoin\Components\BMCBuildings;
use JZ\BardzoMagicznyCoin\Components\BMCChart;
use JZ\BardzoMagicznyCoin\Components\BMCCooldowns;
use JZ\BardzoMagicznyCoin\Components\BMCManager;
use JZ\BardzoMagicznyCoin\Components\BMCRankings;
use JZ\BardzoMagicznyCoin\Components\BMCRulebook;
use JZ\BardzoMagicznyCoin\Components\BMCSlaveSetup;
use JZ\BardzoMagicznyCoin\Components\BMCTools;
use JZ\BardzoMagicznyCoin\Components\BMCTransactions;
use JZ\BardzoMagicznyCoin\Components\BMCWallet;
use JZ\BardzoMagicznyCoin\Components\MattermostConnector;
use JZ\BardzoMagicznyCoin\Components\MattermostOnlyPage;
use JZ\BardzoMagicznyCoin\Console\BalanceFill;
use JZ\BardzoMagicznyCoin\Console\ChargeHidden;
use JZ\BardzoMagicznyCoin\Console\ChinkShipArrived;
use JZ\BardzoMagicznyCoin\Console\CoinsAt;
use JZ\BardzoMagicznyCoin\Console\Debug;
use JZ\BardzoMagicznyCoin\Console\PrepareSlavery;
use JZ\BardzoMagicznyCoin\Console\Refund;
use JZ\BardzoMagicznyCoin\Console\ShipArrived;
use JZ\BardzoMagicznyCoin\Console\SlaveAway;
use JZ\BardzoMagicznyCoin\Console\Slavery;
use JZ\BardzoMagicznyCoin\Console\SlaveTransports;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Repositories\TransactionEloquentRepository;
use JZ\BardzoMagicznyCoin\Repositories\WalletEloquentRepository;
use System\Classes\PluginBase;

/**
 * BardzoMagicznyCoin Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'BardzoMagicznyCoin',
            'description' => 'BMC Backend',
            'author'      => 'JZ',
            'icon'        => 'icon-star'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(WalletRepository::class, WalletEloquentRepository::class);
        $this->app->bind(TransactionRepository::class, TransactionEloquentRepository::class);
        $this->commands([
                            PrepareSlavery::class,
                            Slavery::class,
                            Debug::class,
                            BalanceFill::class,
                            ShipArrived::class,
                            ChinkShipArrived::class,
                            ChargeHidden::class,
                            Refund::class,
                            SlaveTransports::class,
                            SlaveAway::class
                        ]);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot(): void
    {

    }

    /**
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    public function registerSchedule($schedule)
    {
        $offset = $this->getRandomOffset();
        $schedule->command('bmc:prepare-slavery')->dailyAt('00:00');
        $schedule->command('bmc:slavery')->hourlyAt(5);
        $schedule->command('bmc:chink-ship-arrived')->dailyAt('00:01');
        $schedule->command('bmc:ship-arrived')->dailyAt('00:01');
        $schedule->command('bmc:slave-away')->dailyAt('00:15');
        $schedule->command('bmc:charge-hidden')->hourlyAt($offset);
        $schedule->command('bmc:slave-transports')->everyMinute();
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            BMCWallet::class           => 'bmc_wallet',
            BMCTransactions::class     => 'bmc_transactions',
            BMCRankings::class         => 'bmc_rankings',
            BMCChart::class            => 'bmc_chart',
            BMCManager::class          => 'bmc_manager',
            BMCTools::class            => 'bmc_tools',
            BMCRulebook::class         => 'bmc_rulebook',
            BMCBuildings::class        => 'bmc_buildings',
            BMCSlaveSetup::class       => 'bmc_slave_setup',
            BMCCooldowns::class        => 'bmc_cooldowns',
            MattermostConnector::class => 'bmc_mm_connector',
            MattermostOnlyPage::class  => 'bmc_mm_only',
        ]; // Remove this line to activate
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'jz.bardzomagicznycoin.wallets'      => [
                'tab'   => 'BardzoMagicznyCoin',
                'label' => 'Wallets Access'
            ],
            'jz.bardzomagicznycoin.transactions' => [
                'tab'   => 'BardzoMagicznyCoin',
                'label' => 'Transactions Access'
            ],
            'jz.bardzomagicznycoin.challenges'   => [
                'tab'   => 'BardzoMagicznyCoin',
                'label' => 'Challenges Access'
            ],
            'jz.bardzomagicznycoin.events'       => [
                'tab'   => 'BardzoMagicznyCoin',
                'label' => 'Events Access'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation(): array
    {
        return [
            'bardzomagicznycoin' => [
                'label'       => 'BMC',
                'url'         => Backend::url('jz/bardzomagicznycoin/wallets'),
                'icon'        => 'icon-money',
                'permissions' => ['jz.bardzomagicznycoin.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'wallets'      => [
                        'label'       => 'Wallets',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('jz/bardzomagicznycoin/wallets'),
                        'permissions' => ['js.bardzomagicznycoin.star'],
                    ],
                    'transactions' => [
                        'label'       => 'Transactions',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('jz/bardzomagicznycoin/transactions'),
                        'permissions' => ['js.bardzomagicznycoin.transactions'],
                    ],
                    'challenges'   => [
                        'label'       => 'Challenges',
                        'icon'        => 'icon-space-shuttle',
                        'url'         => Backend::url('jz/bardzomagicznycoin/challenges'),
                        'permissions' => ['js.bardzomagicznycoin.challenges'],
                    ],
                    'stakes'       => [
                        'label'       => 'Stakes',
                        'icon'        => 'icon-gift',
                        'url'         => Backend::url('jz/bardzomagicznycoin/stakes'),
                        'permissions' => ['js.bardzomagicznycoin.stakes'],
                    ],
                    'events'       => [
                        'label'       => 'Events',
                        'icon'        => 'icon-gift',
                        'url'         => Backend::url('jz/bardzomagicznycoin/events'),
                        'permissions' => ['js.bardzomagicznycoin.events'],
                    ],
                    'gameevents'   => [
                        'label'       => 'Game Events',
                        'icon'        => 'icon-gift',
                        'url'         => Backend::url('jz/bardzomagicznycoin/gameevents'),
                        'permissions' => ['js.bardzomagicznycoin.gameevents'],
                    ],
                    'assaults'     => [
                        'label'       => 'Assaults',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('jz/bardzomagicznycoin/assaults'),
                        'permissions' => ['js.bardzomagicznycoin.assaults'],
                    ],
                    'buildings'    => [
                        'label'       => 'Buildings',
                        'icon'        => 'icon-building',
                        'url'         => Backend::url('jz/bardzomagicznycoin/buildings'),
                        'permissions' => ['js.bardzomagicznycoin.buildings'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'BMC Settings',
                'description' => 'BMC Settings',
                'category'    => 'BMC',
                'icon'        => 'icon-money',
                'class'       => Settings::class,
                'order'       => 100,
                'permissions' => ['jz.bardzomagicznycoin.settings']
            ],
        ];
    }

    private function getRandomOffset()
    {
        $configPath = storage_path('schedule.json');
        $scheduleConfig = file_exists($configPath) ? json_decode(file_get_contents($configPath), true) : [];

        $currentHour = date('G'); // 24-hour format without leading zeros (0 to 23)

        if (!isset($scheduleConfig['bmc:charge-hidden'])
            || $scheduleConfig['bmc:charge-hidden']['hour'] != $currentHour) {
            // Only set the offset if it's not already set or if the hour has changed
            $scheduleConfig['bmc:charge-hidden'] = [
                'hour'   => $currentHour,
                'offset' => random_int(1, 59),
            ];
            file_put_contents($configPath, json_encode($scheduleConfig));
        }

        return $scheduleConfig['bmc:charge-hidden']['offset'];
    }
}
