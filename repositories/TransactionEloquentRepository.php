<?php

namespace JZ\BardzoMagicznyCoin\Repositories;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Database\Eloquent\Collection;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

class TransactionEloquentRepository implements TransactionRepository
{

    public function getTransactionsForWallet(Wallet $wallet, Carbon $from = null, Carbon $to = null): Collection
    {
        $q = Transaction::where(function($qe) use($wallet){
            $qe->where('source_id', $wallet->id)->orWhere('target_id', $wallet->id);
        });
        if ($from) {
            $q = $q->where('created_at', '>=', $from);
        }
        if ($to) {
            $q = $q->where('created_at', '<=', $to);
        }
        return $q->with('source')->with('target')->orderBy('created_at', 'desc')->get();
    }

    public function createTransaction(Wallet $source, Wallet $target, int $amount, string $type = 'transfer', string $title = null): Transaction
    {
        $transaction = new Transaction();
        $transaction->source_id = $source->id;
        $transaction->target_id = $target->id;
        $transaction->value = $amount;
        $transaction->type = $type;
        $transaction->title = $title;
        $transaction->hash = '';
        $transaction->source_balance = $source->balance;
        $transaction->target_balance = $target->balance;
        $transaction->save();
        $hashids = new Hashids(env('APP_KEY'), 32); // pad to length 10
        $transaction->hash = $hashids->encode($transaction->id);
        $transaction->save();
        return $transaction;
    }

    public function getWonBetsForWallet(?Wallet $wallet): Collection
    {
        return $wallet->incomingTransactions()->where('source_id', 1)->where('type', 'bet')->get();
    }

    public function getLostBetsForWallet(?Wallet $wallet): Collection
    {
        return $wallet->outgoingTransactions()->where('target_id', 1)->where('type', 'bet')->get();
    }

    public function getWonChallengesForWallet(Wallet $wallet): Collection
    {
        return $wallet->incomingTransactions()->where('type', 'challenge')->get();
    }

    public function getLostChallengesForWallet(Wallet $wallet): Collection
    {
        return $wallet->outgoingTransactions()->where('type', 'challenge')->get();
    }
}
