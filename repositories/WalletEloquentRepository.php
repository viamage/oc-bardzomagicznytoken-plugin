<?php

namespace JZ\BardzoMagicznyCoin\Repositories;

use Hashids\Hashids;
use JZ\BardzoMagicznyCoin\Classes\SlaveryManager;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use JZ\BardzoMagicznyCoin\ValueObjects\BetRankingWallet;
use JZ\BardzoMagicznyCoin\ValueObjects\SlaveRankingWallet;
use October\Rain\Database\Collection;

class WalletEloquentRepository implements WalletRepository
{
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function getWalletForName(string $name): ?Wallet
    {
        return Wallet::where('name', $name)->with('buildings')->first();
    }

    public function getWhitelistedWallets(): Collection
    {
        return Wallet::where('is_blacklisted', false)->with('buildings')->where('is_active', true)->get();
    }


    public function getPoorestWhitelistedWallets(int $amount = 5): Collection
    {
        return Wallet::where('is_blacklisted', false)
                     ->where('is_active', true)
                     ->where('is_bot', false)
                     ->orderBy('balance', 'asc')->limit($amount)->get();
    }

    public function addToWalletBalance(Wallet $wallet, int $amount): Wallet
    {
        $wallet->balance += $amount;
        $wallet->save();
        return $wallet;
    }

    public function deductFromWalletBalance(Wallet $wallet, int $amount): Wallet
    {
        $wallet->balance -= $amount;
        $wallet->save();
        return $wallet;
    }


    public function createWallet(string $name, int $balance): Wallet
    {
        $wallet = new Wallet();
        $wallet->name = $name;
        $wallet->balance = $balance;
        $wallet->hash = '';
        $wallet->save();
        $hashids = new Hashids(env('APP_KEY'), 24); // pad to length 10
        $wallet->hash = $hashids->encode($wallet->id);
        return $wallet;
    }

    public function getRanking(bool $includeBots = false): Collection
    {
        $query = Wallet::where('is_blacklisted', false)->with('buildings')->where('name', '!=', 'wizard');

        if (!$includeBots) {
            $query->where('is_bot', false);
        }

        return $query->orderBy('balance', 'desc')->limit(20)->get();
    }

    public function getAllWallets(): Collection
    {
        return Wallet::all();
    }

    public function getBetRanking()
    {
        $wallets = $this->getWhitelistedWallets();
        $ranking = [];
        foreach ($wallets as $wallet) {
            $obj = new BetRankingWallet();
            $obj->name = $wallet->name;
            $wonBets = $this->transactionRepository->getWonBetsForWallet($wallet);
            $lostBets = $this->transactionRepository->getLostBetsForWallet($wallet);
            $obj->wonGames = $wonBets->count();
            $obj->lostGames = $lostBets->count();
            $wonBets->each(function (Transaction $transaction) use (&$obj) {
                ++$obj->games;
                $obj->won += $transaction->value;
                $obj->total += $transaction->value;
            });
            $lostBets->each(function (Transaction $transaction) use (&$obj) {
                ++$obj->games;
                $obj->lost += $transaction->value;
                $obj->total -= $transaction->value;
            });
            if ($obj->games > 0) {
                $ranking[] = $obj;
            }
        }
        $collection = collect($ranking);
        return $collection->sortByDesc('total');
    }

    public function getSlaveRanking()
    {
        $settings = Settings::instance();
        $jewPrice = $settings->get('jew_price', 1000);
        $niggerPrice = $settings->get('nigger_price', 200);
        $wallets = $this->getWhitelistedWallets();
        $ranking = [];
        /** @var Wallet $wallet */
        foreach ($wallets as $wallet) {
            $obj = new SlaveRankingWallet();
            $obj->name = $wallet->name;
            $obj->niggers = $wallet->niggers;
            $obj->chinese = $wallet->chinese ?? 0;
            if ($wallet->has_jew) {
                $obj->hasJew = 'Yes :(';
            } else {
                $obj->hasJew = 'No :)';
            }

            $obj->gassedJews = $wallet->gassed_jews;
            $obj->slavesValue = ($wallet->niggers * $niggerPrice);
            if ($wallet->has_jew) {
                $obj->slavesValue += $jewPrice;
            }
            if ($obj->slavesValue > 0 || $obj->gassedJews > 0) {
                $ranking[] = $obj;
            }
        }
        $collection = collect($ranking);
        return $collection->sortByDesc('slavesValue');
    }

    public function getWalletsForNames(array $names)
    {
        return Wallet::where('is_blacklisted', false)->where('is_active', true)->where('is_bot', false)->whereIn('name', $names)->get();
    }
}
