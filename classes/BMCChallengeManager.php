<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Http\JsonResponse;
use JZ\BardzoMagicznyCoin\Exceptions\BMCChallengeException;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Challenge;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use JZ\BardzoMagicznyCoin\ValueObjects\BetRankingWallet;

class BMCChallengeManager
{
    private $transactionManager;
    private $walletRepository;

    public function __construct(WalletRepository $walletRepository, TransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
        $this->walletRepository = $walletRepository;
    }

    /**
     * TODO create challenge repository you lazy fuck
     *
     * @return Challenge
     * @throws TransactionException
     * @throws BMCChallengeException
     */
    public function raiseChallenge(): Challenge
    {
        $challengerName = request()->get('challenger');
        $challengedName = request()->get('challenged');
        if ($challengerName === $challengedName) {
            throw new BMCChallengeException('Nie możesz wyzwać samego siebie!');
        }
        $challenger = $this->walletRepository->getWalletForName($challengerName);
        $challenged = $this->walletRepository->getWalletForName($challengedName);
        if (!$challenger) {
            throw new BMCChallengeException(
                'Nie masz portfela! Spróbuj `wizard create-wallet ' . $challengedName . '`'
            );
        }
        if ($challenger->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($challenger);
        }
        if (!$challenged) {
            throw new BMCChallengeException('Wyzwany ' . $challengedName . ' nie ma portfela!');
        }
        if ($challenger->is_bot || $challenged->is_bot || $challenged->name === 'wizard') {
            throw new BMCChallengeException('Boty nie umieją walczyć');
        }
        $fiveMinutesAgo = Carbon::now()->subMinutes(5);
        /** @var Challenge $activeChallenge */
        $activeChallenge = Challenge::where(function ($q) use ($challenger, $challenged) {
            $q->where('challenger_id', $challenger->id)->orWhere('challenged_id', $challenged->id);
        })
                                    ->where('created_at', '>', $fiveMinutesAgo)
                                    ->where('winner_id', null)->first();
        if ($activeChallenge) {
            $minutesLeft = 5 - (Carbon::now())->diffInMinutes($activeChallenge->created_at, true);
            if ($activeChallenge->challenger_id === $challenger->id) {
                throw new BMCChallengeException(
                    'Już masz aktywne wyzwanie przeciwko ' . $activeChallenge->challenged->name
                    . ' o ' . $activeChallenge->amount . ' BMC. Poczekaj '
                    . $minutesLeft . ' minut albo użyj `wizard cancel`'
                );
            } else {
                throw new BMCChallengeException(
                    $challengedName . ' ma już aktywne wyzwanie od ' . $activeChallenge->challenger->name
                    . ' o ' . $activeChallenge->amount . ' BMC.'
                );
            }
        }

        $amount = request()->get('amount');
        if (!$challenger || !$challenged) {
            throw new \Exception('Invalid Request');
        }
        $this->transactionManager->validateAmount($amount);
        $this->validateChallenge($challenger, $challenged, $amount);
        $hashids = new Hashids(env('APP_KEY'), 24);
        $challenge = new Challenge();
        $challenge->hash = $hashids->encode(Challenge::count() + 1);
        $challenge->challenged_id = $challenged->id;
        $challenge->challenger_id = $challenger->id;
        $challenge->amount = $amount;
        $challenge->accepted = false;
        $challenge->save();

        return $challenge;
    }

    public function getChallenges()
    {
        $walletName = request()->get('name');
        $wallet = $this->walletRepository->getWalletForName($walletName);
        $hourAgo = Carbon::now()->subMinutes(60);

        $challenges = Challenge::where('created_at', '>', $hourAgo)->where(function ($q) use ($wallet) {
            $q->where('challenged_id', $wallet->id)->orWhere('challenger_id', $wallet->id);
        })->get();

        $result = '|Wyzywający|Wyzwany|Kwota|Zaakceptowany|Wygrany|' . PHP_EOL;
        $result .= '|:---|:---|:---|:---|:---|' . PHP_EOL;
        /** @var Challenge $ch */
        foreach ($challenges as $ch) {
            $accepted = ':thumbsdown_proper:';
            if ($ch->accepted) {
                $accepted = ':thumbsup_proper:';
            }
            $winner = '';
            if ($ch->winner) {
                $winner = $ch->winner->name;
            }
            $result .= '|' . $ch->challenger->name . '|' . $ch->challenged->name . '|' . $ch->amount . '|' . $accepted . '|' . $winner . '|' . PHP_EOL;
        }
        return $result;
    }

    /**
     * TODO return challenge move response generation to controller
     *
     * @param string $hash
     *
     * @return JsonResponse
     * @throws BMCChallengeException
     */
    public function acceptChallenge(): string
    {
        $challengedName = request()->get('name');
        $challenged = $this->walletRepository->getWalletForName($challengedName);
        if (!$challenged) {
            throw new BMCChallengeException('Nie masz portfela! Spróbuj `wizard create-wallet '
                                            . $challengedName . '`');
        }
        if ($challenged->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($challenged);
        }
        $fiveMinutesAgo = Carbon::now()->subMinutes(5);
        /** @var Challenge $challenge */
        $challenge = Challenge::where('challenged_id', $challenged->id)
                              ->where('created_at', '>', $fiveMinutesAgo)->where('accepted', false)->first();
        if (!$challenge) {
            throw new BMCChallengeException('Nie masz żadnego wyzwania z ostatnich 5 minut');
        }

        $this->validateChallenge($challenge->challenger, $challenge->challenged, $challenge->amount);
        $challenge->accepted = true;
        $betSeed = random_int(0, 100);
        if ($challenge->challenger->has_jew) {
            $betSeed -= 10;
        }
        if ($challenge->challenged->has_jew) {
            $betSeed += 10;
        }
        if ($betSeed < 50) {
            $betResult = 1;
        } else {
            $betResult = 2;
        }

        // challenger lost
        if ($betResult === 1) {
            $challenge->winner_id = $challenge->challenged->id;
            $this->transactionManager->sendToWallet(
                $challenge->challenger,
                $challenge->challenged,
                $challenge->amount,
                'challenge'
            );
            $message = $challenge->challenger->name
                       . ' przegrał! Przelewam ' . $challenge->amount . ' BMC na konto ' . $challenge->challenged->name;
        } else {
            // challenger win
            $challenge->winner_id = $challenge->challenger->id;
            $this->transactionManager->sendToWallet(
                $challenge->challenged,
                $challenge->challenger,
                $challenge->amount,
                'challenge'
            );
            $message = $challenge->challenger->name . ' wygrał! Przelewam '
                       . $challenge->amount . ' BMC na konto ' . $challenge->challenger->name;
        }
        $challenge->save();
        return $message;
    }

    public function rejectChallenge(): string
    {
        $challengedName = request()->get('name');
        $challenged = $this->walletRepository->getWalletForName($challengedName);
        if (!$challenged) {
            throw new BMCChallengeException('Nie masz portfela! Spróbuj `wizard create-wallet ' . $challengedName . '`');
        }
        if ($challenged->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($challenged);
        }
        $fiveMinutesAgo = Carbon::now()->subMinutes(5);
        $challenge = Challenge::where('challenged_id', $challenged->id)->where('created_at', '>', $fiveMinutesAgo)->where('accepted', false)->first();
        if (!$challenge) {
            throw new BMCChallengeException('Nie masz żadnego wyzwania z ostatnich 5 minut');
        }
        $challenge->delete();

        return 'Wyzwanie odrzucone';
    }

    public function cancelChallenge(): string
    {
        $challengerName = request()->get('name');
        $challenger = $this->walletRepository->getWalletForName($challengerName);
        if (!$challenger) {
            throw new BMCChallengeException('Nie masz portfela! Spróbuj `wizard create-wallet '
                                            . $challengerName . '`');
        }
        $fiveMinutesAgo = Carbon::now()->subMinutes(5);
        $challenge = Challenge::where('challenger_id', $challenger->id)
                              ->where('created_at', '>', $fiveMinutesAgo)->where('accepted', false)->first();
        if (!$challenge) {
            throw new BMCChallengeException('Nie masz żadnego wyzwania z ostatnich 5 minut');
        }
        $challenged = $challenge->challenged->name;
        $challenge->delete();

        return $challenged;
    }

    /**
     * @param Wallet $challenger
     * @param Wallet $challenged
     * @param mixed  $amount
     *
     * @throws TransactionException
     */
    private function validateChallenge(Wallet $challenger, Wallet $challenged, $amount): void
    {
        if (!$amount || $amount < 0 || !is_numeric($amount) || str_contains($amount, '.') || str_contains($amount, ',')) {
            \Log::error('INVALID AMOUNT ' . $amount);
            throw new TransactionException('Nieprawidłowa kwota!');
        }

        if ($challenger->balance < $amount) {
            throw new TransactionException('Niewystarczające środki na koncie :( Spróbuj `wizard weź poczaruj`');
        }
        if ($challenged->balance < $amount) {
            $maxAmount = $challenged->balance;
            if ($challenger->balance < $challenged->balance) {
                $maxAmount = $challenger->balance;
            }
            throw new TransactionException($challenged->name . ' ma za mało środków na koncie. Maksymalna kwota zakładu to ' . $maxAmount);
        }

        if ($challenger->balance === 1) {
            throw new TransactionException('Duele zablokowane - zbyt niski stan konta. Spróbuj `wizard weź poczaruj`');
        }

        if ($challenged->balance === 1) {
            throw new TransactionException('Duele zablokowane - zbyt niski stan konta. Spróbuj `wizard weź poczaruj`');
        }
    }

    public function getChallengeRanking()
    {
        $wallets = $this->walletRepository->getWhitelistedWallets();
        $ranking = [];
        foreach ($wallets as $wallet) {
            $obj = new BetRankingWallet();
            $obj->name = $wallet->name;
            $wonChallenges = $this->transactionManager->transactionRepository->getWonChallengesForWallet($wallet);
            $lostChallenges = $this->transactionManager->transactionRepository->getLostChallengesForWallet($wallet);
            $obj->wonGames = $wonChallenges->count();
            $obj->lostGames = $lostChallenges->count();
            $wonChallenges->each(function (Transaction $transaction) use (&$obj) {
                ++$obj->games;
                $obj->won += $transaction->value;
                $obj->total += $transaction->value;
            });
            $lostChallenges->each(function (Transaction $transaction) use (&$obj) {
                ++$obj->games;
                $obj->lost += $transaction->value;
                $obj->total -= $transaction->value;
            });
            if ($obj->games > 0) {
                $ranking[] = $obj;
            }
        }
        $collection = collect($ranking);
        return $collection->sortByDesc('total');
    }
}
