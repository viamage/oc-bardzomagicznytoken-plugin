<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use JZ\BardzoMagicznyCoin\ValueObjects\BetResult;

class BMCBetManager
{

    /**
     * Chance for victory in wizard bet
     */
    public const BET_CHANCE = 495;

    public const MINIMUM_BET = 20;


    private $walletRepository;
    private $transactionManager;

    public function __construct(WalletRepository $walletRepository, TransactionManager $transactionManager)
    {
        $this->walletRepository = $walletRepository;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @return BetResult
     * @throws TransactionException
     * @throws \Exception
     */
    public function bet(): BetResult
    {
        $wallet = $this->walletRepository->getWalletForName(request()->get('name'));
        $wizard = $this->walletRepository->getWalletForName('wizard');
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if (!$wallet) {
            throw new TransactionException(
                'Nie masz portfela! Użyj `wizard create wallet ' . request()->get('name') . '`'
            );
        }
        $amount = request()->get('amount');
        if ($amount === 'all') {
            $amount = $wallet->balance;
        }

        $this->validateBet($wallet, $amount);
        $betResult = random_int(1, 1000);
        $betChance = self::BET_CHANCE;
        if ($wallet->has_jew) {
            $betChance -= 100;
        }
        if ($betResult <= $betChance) {
            $transaction = $this->transactionManager->sendToWallet($wizard, $wallet, $amount, 'bet');
            $victory = true;
        } else {
            $transaction = $this->transactionManager->sendToWallet($wallet, $wizard, $amount, 'bet');
            $victory = false;
        }
        return new BetResult($transaction, $victory, $wallet->balance);
    }

    /**
     * @param Wallet $wallet
     * @param mixed  $amount
     *
     * @throws TransactionException
     */
    private function validateBet(Wallet $wallet, $amount): void
    {
        if (!$amount || $amount < 0 || !is_numeric($amount) || str_contains($amount, '.') || str_contains($amount, ',')) {
            \Log::error('INVALID AMOUNT ' . $amount);
            throw new TransactionException('Nieprawidłowa kwota!');
        }

        if ($amount < self::MINIMUM_BET) {
            throw new TransactionException(
                'Kasyno nie przyjmuje stawek mniejszych niż ' . self::MINIMUM_BET . ' :bmc:'
            );
        }

        if ($wallet->balance < $amount) {
            throw new TransactionException('Niewystarczające środki na koncie :( Spróbuj `wizard weź poczaruj`');
        }
    }

}
