<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use JZ\BardzoMagicznyCoin\Exceptions\BMCUIException;
use JZ\BardzoMagicznyCoin\Models\Wallet;

/**
 *
 */
class BMCComponentBase extends ComponentBase
{

    /**
     *
     */
    public function componentDetails()
    {
        // TODO: Implement componentDetails() method.
    }

    /**
     * @return Wallet
     * @throws BMCUIException
     */
    public function getConnectedWallet()
    {
        $wallet = null;
        $walletId = Session::get('wallet_id');
        if ($walletId) {
            $wallet = Wallet::where('id', $walletId)->with('buildings')->first();
        }
        if (!$wallet) {
            throw new BMCUIException('No wallet connected');
        }
        return $wallet;
    }
}
