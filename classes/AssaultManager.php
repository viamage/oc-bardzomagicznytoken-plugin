<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Support\Facades\Log;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Assault;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use October\Rain\Exception\ApplicationException;

/**
 *
 */
class AssaultManager
{
    /**
     *
     */
    public const WALLET_STRENGTH = 10;
    public const ASSAULT_CD = 60;

    /**
     * @var WalletRepository
     */
    private $walletRepository;
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var SlaveryManager
     */
    private $slaveryManager;

    /**
     * @param WalletRepository   $walletRepository
     * @param TransactionManager $transactionManager
     */
    public function __construct(WalletRepository   $walletRepository,
                                TransactionManager $transactionManager,
                                SlaveryManager     $slaveryManager)
    {
        $this->walletRepository = $walletRepository;
        $this->transactionManager = $transactionManager;
        $this->slaveryManager = $slaveryManager;
    }

    /**
     * @param Wallet $boss
     * @param Wallet $target
     * @param int    $amount
     * @param bool   $public
     *
     * @return Assault
     *
     */
    public function createAssault(Wallet $boss, Wallet $target, int $amount, bool $public = true): Assault
    {
        $this->validateCreateAssault($boss, $target, $amount);
        $lastAssault = Assault::where('boss_id', $boss->id)->orderBy('created_at', 'desc')->first();
        if ($lastAssault) {
            $assaultCD = (self::ASSAULT_CD * 60) - Carbon::now()->diffInSeconds($lastAssault->created_at);
            if($assaultCD > 0){
                throw new ApplicationException('Musisz odczekać jeszcze ' . (int)($assaultCD /60) . ' minut.');
            }
        }
        $assault = new Assault();
        $assault->boss_id = $boss->id;
        $assault->target_id = $target->id;
        $assault->is_public = $public;
        $assault->save();

        $hi = new Hashids(env('APP_KEY'), 6);
        $assault->code = $hi->encode($assault->id);
        $assault->save();
        $strength = $this->calculateStrength($boss);
        $pivotData = [
            'strength' => $strength,
            'value'    => $amount,
            'date'     => Carbon::now()
        ];
        $this->transactionManager->sendToWallet($boss, $this->getWizard(), request()->get('amount'), 'Assault funds');
        $assault->wallets()->attach($boss->id, $pivotData);

        return $assault;
    }


    /**
     * @param Assault $assault
     *
     * @return Assault
     *
     */
    public function cancelAssault(Wallet $wallet, Assault $assault): Assault
    {
        if (!$wallet || $wallet->is_bot) {
            throw new ApplicationException('Coś nie gra z tym portfelem');
        }
        if ($wallet->id !== $assault->boss_id) {
            throw new ApplicationException('Nie jesteś szefem tego ataku');
        }
        foreach ($assault->wallets()->withPivot(['value', 'date'])->get() as $awallet) {
            $return = (int)$this->calculateReturn($awallet->pivot->value, $awallet->pivot->date);
            if ($return > 0) {
                $this->transactionManager->sendToWallet(
                    $this->getWizard(),
                    $awallet,
                    $return,
                    'Assault refund'
                );
            }
        }
        $assault->is_finished = true;
        $assault->result = 3;
        $assault->save();
        return $assault;
    }

    /**
     * @param Assault $assault
     * @param Wallet  $wallet
     * @param int     $amount
     *
     * @return Assault
     *
     */
    public function joinAssault(Assault $assault, Wallet $wallet, int $amount): Assault
    {
        $this->validateJoinAssault($assault, $wallet, $amount);
        $strength = $this->calculateStrength($wallet);
        $value = $this->calculateValue($assault);
        if ($amount + $value > $assault->target->balance) {
            $max = $assault->target->balance - $value;
            if ($max < 0) {
                $max = 0;
            }
            throw new ApplicationException('Typ nie ma tyle hajsu. Max to ' . $max);
        }
        if ($assault->wallets->where('id', $wallet->id)->count() > 0) {
            throw new ApplicationException('Już uczestniczysz w tym napadzie');
        }

        if (!$assault->is_public) {
            if ($assault->invited->where('id', $wallet->id)->count() === 0) {
                throw new ApplicationException('Nie ma takiego napadu!');
            }
        }
        $pivotData = [
            'strength' => $strength,
            'value'    => $amount,
            'date'     => Carbon::now()
        ];
        $this->transactionManager->sendToWallet($wallet, $this->getWizard(), request()->get('amount'), 'Assault funds');
        $assault->wallets()->attach($wallet->id, $pivotData);
        BMCGameEvents::notifyWallet($wallet->name . ' dołączył do napadu ' . $assault->code, $assault->boss->name);

        return $assault;
    }

    /**
     * @return Wallet|null
     */
    private function getWizard()
    {
        return $this->walletRepository->getWalletForName('wizard');
    }

    /**
     * @param         $targets
     * @param Assault $assault
     * @param Wallet  $wallet
     */
    public function inviteToAssault($targets, Assault $assault, Wallet $wallet)
    {
        $alreadyInvited = $assault->invited()
                                  ->whereIn('name', $targets->pluck('name', 'name')
                                                            ->toArray())->pluck('id', 'id')
                                  ->toArray();
        foreach ($targets as $target) {
            if ($target->id === $assault->target_id
                || $target->id === $wallet->id
                || in_array($target->id, $alreadyInvited, true)) {
                continue;
            }
            $assault->invited()->attach($target->id);
            $invite = 'Zostałeś zaproszony do udziału w napadzie na ' . $assault->target->name . '. Użyj `wizard join-assault '
                      . $assault->code . ' <kwota>` aby dołączyć.';
            BMCGameEvents::notifyWallet($invite, $target->name);
        }
    }

    /**
     * @param        $assault
     * @param Wallet $wallet
     *
     * @return Transaction
     *
     */
    public function leaveAssault($assault, Wallet $wallet): Transaction
    {
        $this->validateLeaveAssault($assault, $wallet);
        $relatedWallet = $assault->wallets()->withPivot(['value'])->where('id', $wallet->id)->first();
        $assault->wallets()->detach($wallet);
        return $this->transactionManager->sendToWallet(
            $this->getWizard(),
            $wallet,
            $this->calculateReturn($relatedWallet->pivot->value, $relatedWallet->pivot->date),
            'Assault refund'
        );
    }

    /**
     * @param Assault $assault
     * @param Wallet  $wallet
     * @param Wallet  $target
     *
     * @throws ApplicationException
     */
    public function switchTarget(Assault $assault, Wallet $wallet, Wallet $target)
    {
        if ($assault->boss_id !== $wallet->id) {
            throw new ApplicationException('Nie jesteś szefem typie');
        }
        if ($assault->wallets->where('id', $target->id)->count() > 0) {
            throw new ApplicationException('Swojego chcesz bić? Typ bierze udział w tym napadzie.');
        }
        if ($target->balance < $assault->getTotalValue()) {
            throw new ApplicationException('Napad jest za gruby na tego typa, on nie ma tyle hajsu');
        }
        $assault->target_id = $target->id;
        $assault->save();
    }

    /**
     * @param        $assault
     * @param Wallet $wallet
     *
     * @return mixed
     *
     */
    public function commenceAssault($assault, Wallet $wallet, bool $forceSuccess = false)
    {
        $this->validateCommenceAssault($assault, $wallet);
        $chances = $this->calculateChances($assault);
        $value = $this->calculateValue($assault);
        $result = random_int(1, 100);
        $wizard = $this->getWizard();
        if ($forceSuccess) {
            $result = 0;
        }
        if ($result <= $chances) {
            $assault->result = 1;
            $this->transactionManager->sendToWallet($assault->target, $wizard, $value, 'Assault!');
            $this->assaultNiggersDied($assault->target, 'defence');
            BMCGameEvents::notifyWallet('Ojebano cie na hajs', $assault->target->name);
            foreach ($assault->wallets()->withPivot('value')->get() as $assaultWallet) {
                BMCGameEvents::hiddenStatusRemoved($assaultWallet);
                $this->notifyWalletAboutVictory($assaultWallet, $assault);
                $reward = $assaultWallet->pivot->value * 2;
                $this->transactionManager->sendToWallet(
                    $wizard,
                    $assaultWallet,
                    $reward,
                    'Assault ' . $assault->code . ' Won!'
                );
            }
        } else {
            $assault->result = 0;
            /** @var Wallet $assaultWallet */
            foreach ($assault->wallets()->withPivot('value')->get() as $assaultWallet) {
                $this->assaultNiggersDied($assaultWallet, 'attack');
                if($assaultWallet->is_hidden) {
                    BMCGameEvents::hiddenStatusRemoved($assaultWallet);
                }
                $this->notifyWalletAboutLostAssault($assaultWallet, $assault);
            }
        }

        $assault->is_finished = true;
        $assault->save();
        return $assault;
    }

    /**
     * @param \JZ\BardzoMagicznyCoin\Models\Wallet $wallet
     *
     * @return int
     */
    private function calculateStrength(\JZ\BardzoMagicznyCoin\Models\Wallet $wallet): int
    {
        $default = self::WALLET_STRENGTH;
        // todo
        return $default;
    }


    /**
     * @param Assault $assault
     *
     * @return float|int
     */
    private function calculateChances(Assault $assault)
    {
        return $assault->getChances();
    }

    /**
     * @param Assault $assault
     *
     * @return int
     */
    private function calculateValue(Assault $assault): int
    {
        return $assault->getTotalValue();
    }

    /**
     * @param Wallet $wallet
     * @param Wallet $target
     * @param int    $amount
     *
     * @throws ApplicationException
     */
    private function validateCreateAssault(Wallet $wallet, Wallet $target, int $amount)
    {
        if ($target->name === 'wizard') {
            throw new ApplicationException('Nie możesz napadać na czarodzieja');
        }
        if (!is_numeric($amount) || $amount < 0) {
            throw new ApplicationException('Coś nie gra z tą kwotą');
        }
        if (!$wallet || $wallet->is_bot) {
            throw new ApplicationException('Coś nie gra z tym portfelem');
        }
        if (!$target) {
            throw new ApplicationException('Nieprawidłowy cel');
        }
        if ($amount > $wallet->balance) {
            throw new ApplicationException('Nie stać cię');
        }
        if ($amount < 1) {
            throw new ApplicationException('Nie możesz napadać za darmo');
        }
        if ($amount > $target->balance) {
            throw new ApplicationException('Typ nie ma takiego hajsu');
        }
    }

    /**
     * @param Assault $assault
     * @param Wallet  $wallet
     * @param int     $amount
     *
     * @throws ApplicationException
     */
    private function validateJoinAssault(Assault $assault, Wallet $wallet, int $amount)
    {
        if (!is_numeric($amount) || $amount < 0) {
            throw new ApplicationException('Coś nie gra z tą kwotą');
        }

        if ($wallet->is_bot) {
            throw new ApplicationException('Coś nie gra z tym portfelem');
        }
        if ($amount > $wallet->balance) {
            throw new ApplicationException('Nie stać cię');
        }

        if ($assault->target_id === $wallet->id) {
            throw new ApplicationException('xD');
        }
    }

    /**
     * @param Assault $assault
     * @param Wallet  $wallet
     *
     * @throws ApplicationException
     */
    private function validateLeaveAssault(Assault $assault, Wallet $wallet)
    {
        if ($assault->boss_id === $wallet->id) {
            throw new ApplicationException('Szefie no co szef');
        }
        if ($wallet->is_bot) {
            throw new ApplicationException('Coś nie gra z tym portfelem');
        }
    }

    /**
     * @param        $assault
     * @param Wallet $wallet
     *
     * @throws ApplicationException
     */
    private function validateCommenceAssault($assault, Wallet $wallet)
    {
        if ($wallet->id !== $assault->boss_id) {
            throw new ApplicationException('Nie jesteś szefem typie');
        }
    }

    /**
     * @param Wallet  $assaultWallet
     * @param Assault $assault
     */
    private function notifyWalletAboutVictory($assaultWallet, $assault)
    {
        $message = 'Napad ' . $assault->code . ' zakończony sukcesem!' . PHP_EOL;
        $message .= $assault->target->name . ' został ojebany na ' . $assault->getTotalValue() . ' :bmc: ' . PHP_EOL;
        $message .= 'Twoja działka to '
                    . $assault->getMyReward($assaultWallet)
                    . ' :bmc: (oraz twój wkład wraca do Ciebie)' . PHP_EOL;
        BMCGameEvents::notifyWallet($message, $assaultWallet->name);
    }

    /**
     * @param $value
     * @param $date
     *
     * @return float|int|mixed
     */
    private function calculateReturn($value, $date)
    {
        if (!$date) {
            return $value;
        }
        $diff = (new Carbon($date))->diffInHours(Carbon::now());
        if ($diff > 10) {
            return 0;
        }

        $return = $value * ((10 - $diff) / 10);
        return $return;
    }

    /**
     * @param $assaultWallet
     * @param $assault
     */
    private function notifyWalletAboutLostAssault($assaultWallet, $assault)
    {
        $message = 'Napad ' . $assault->code . ' zakończony niepowodzeniem :(' . PHP_EOL;
        $message .= 'Twój wkład w wysokości '
                    . $assault->getMyReward($assaultWallet)
                    . ' :bmc: przepadł.' . PHP_EOL;
        BMCGameEvents::notifyWallet($message, $assaultWallet->name);
    }

    /**
     * @param $target
     *
     * @throws ApplicationException
     */
    private function assaultNiggersDied(Wallet $wallet, string $mode = 'attack')
    {
        if ($mode === 'attack') {
            $modeCode = SlaveryManager::NIGGER_ATTACK;
        } else {
            $modeCode = SlaveryManager::NIGGERS_DEFEND;
        }
        $setup = $wallet->slave_setup;
        $amount = 0;
        foreach ($setup as $entry) {
            if ($entry["mode"] == $modeCode) {
                $amount = $entry['amount'];
            }
        }
        if ($amount === 0) {
            return;
        }
        $coinFlip = random_int(0, 1);
        if ($coinFlip === 1) {
            if($mode === 'attack') {
                BMCGameEvents::notifyWallet('Jeden murzyn zginął w trakcie ataku', $wallet->name);
            } else {
                BMCGameEvents::notifyWallet('Jeden murzyn zginął w trakcie obrony', $wallet->name);
            }
            $this->slaveryManager->killNigger($wallet, false);
        }
    }
}
