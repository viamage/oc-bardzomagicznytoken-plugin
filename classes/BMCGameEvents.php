<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Carbon\Carbon;
use JZ\BardzoMagicznyCoin\Models\GameEvent;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Keios\Apparatus\Classes\RequestSender;
use Keios\SlackNotifications\Classes\SlackMessageSender;

class BMCGameEvents
{
    public const ICON_URL = 'https://uploads.viamage.com/wizard128.png';

    public static function hiddenStatusRemoved(Wallet $wallet)
    {
        $wallet->is_hidden = false;
        $wallet->save();
        self::notifyWallet('Wyszedłeś z ukrycia!', $wallet->name);
    }


    public static function hiddenStatusSet(Wallet $wallet)
    {
        if($wallet->hidden_at){
            $diff = $wallet->hidden_at->diffInMinutes(Carbon::now()) ;
            if (30 - $diff > 0) {
                self::notifyWallet('Nie możesz się ukryć tak szybko po ostatnim ukryciu! Jeszcze '
                                   . (30 - $diff) . ' minut', $wallet->name);
                return;
            }
        }

        $wallet->is_hidden = true;
        $wallet->hidden_at = Carbon::now();
        $wallet->save();
        self::notifyWallet('Ukryłeś się. Watch your steps.', $wallet->name);
    }

    public static function notifyBmcArena(string $text): void
    {
        $channel = 'bmc';
        if (env('APP_DEBUG')) {
            $channel = 'testing2';
        }
        self::sendToMattermost($text, $channel);
    }

    public static function notifyWallet(string $text, string $wallet)
    {
        $channel = '@' . $wallet;
        if (env('APP_DEBUG')) {
            $channel = 'testing2';
        }
        self::sendToMattermost($text, $channel);
    }

    public static function createEvent(string $type, int $triggeredById, int $length = 24): GameEvent
    {
        $event = new GameEvent();
        $event->type = $type;
        $event->triggered_by_id = $triggeredById;
        $event->length = $length;
        $event->save();
        return $event;
    }

    public static function areJewsDead()
    {
        return GameEvent::where('type', 'jews_dead')->count() > 0;
    }

    public static function sendToMattermost($text, $channel)
    {
        $payload = [
            'text'     => $text,
            'channel'  => $channel,
            'icon_url' => self::ICON_URL,
        ];

        $requestSender = new RequestSender();
        $r = $requestSender->sendPostRequest($payload, env('BMC_WEBHOOK'), true);
    }
}
