<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

namespace JZ\BardzoMagicznyCoin\Classes;

use Illuminate\Http\JsonResponse;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

/**
 *
 */
class TransactionManager
{
    /**
     * @var WalletRepository
     */
    public $walletRepository;
    /**
     * @var TransactionRepository
     */
    public $transactionRepository;

    /**
     * @param WalletRepository      $walletRepository
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(WalletRepository $walletRepository, TransactionRepository $transactionRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * TODO return transaction, move response generation to controller
     *
     * @param string $mode
     *
     * @return JsonResponse
     * @throws TransactionException
     */
    public function sendToWalletForRequest(string $mode = 'transfer'): JsonResponse
    {
        $source = $this->walletRepository->getWalletForName(request()->get('source'));
        $target = $this->walletRepository->getWalletForName(request()->get('target'));
        $title = request()->get('title', null);
        $amount = request()->get('amount');
        if ($target && $target->is_blacklisted) {
            return response()->json([
                                        'error' => 'Ten portfel jest na czarnej liście :niger: '
                                    ]);
        }
        $this->validateWallets($source, $target, $amount, $mode);
        $transaction = $this->sendToWallet($source, $target, $amount, $mode, $title);
        return response()->json([
                                    'message'       => 'Pomyślnie przelano ' . $amount . ' BMC do ' .
                                                       $target->name . '. ID transakcji: ' . $transaction->hash,
                                    'transactionId' => $transaction->hash,
                                    'source'        => $source,
                                    'target'        => $target,
                                    'title'         => $title
                                ]);
    }

    /**
     * TODO return transaction, move response generation to controller
     *
     * @return JsonResponse
     */
    public function sendToAllWalletsForRequest(): JsonResponse
    {
        $source = $this->walletRepository->getWalletForName(request()->get('source'));
        $amount = request()->get('amount');
        try {
            $totalAmount = $this->sendToAllWallets($source, $amount);

            return response()->json([
                                        'message' => 'Pomyślnie przelano ' . $totalAmount
                                                     . ' BMC do wszystkich portfeli.'
                                    ]);
        } catch (\Exception $e) {
            return response()->json([
                                        'error' => $e->getMessage()
                                    ]);
        }
    }

    /**
     * @param Wallet $source
     * @param Wallet $target
     * @param int    $amount
     * @param string $type
     *
     * @return Transaction
     * @throws TransactionException
     */
    public function sendToWallet(
        Wallet $source,
        Wallet $target,
        int    $amount,
        string $type = 'transfer',
        string $title = null
    ): Transaction {
        $this->validateAmount($amount);
        $this->walletRepository->deductFromWalletBalance($source, $amount);
        $this->walletRepository->addToWalletBalance($target, $amount);
        return $this->transactionRepository->createTransaction($source, $target, $amount, $type, $title);
    }

    /**
     * @param Wallet $source
     * @param int    $amount
     * @param string $type
     *
     * @return int
     */
    public function sendToAllWallets(
        Wallet $source,
        int    $amount,
        string $type = 'multitransfer',
        string $title = null,
        bool   $includeBots = true
    ): int {
        $wallets = $this->walletRepository->getWhitelistedWallets();
        $totalAmount = $amount * $wallets->count();
        $this->validateTransfer($source, $totalAmount);
        $this->walletRepository->deductFromWalletBalance($source, $totalAmount);
        foreach ($wallets as $wallet) {
            if ($wallet->name !== 'wizard') {
                if (!$includeBots && $wallet->is_bot) {
                    continue;
                }
                $this->walletRepository->addToWalletBalance($wallet, $amount);
                $this->transactionRepository->createTransaction($source, $wallet, $amount, $type, $title);
            }
        }

        return $totalAmount;
    }

    public function giveAlms(
        Wallet $source,
        int    $amount,
        int    $walletsCount = 5,
        string $type = 'multitransfer',
        string $title = 'Jałmużna'
    ): array {
        if ($source->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($source);
        }
        $this->validateTransfer($source, $amount);
        $finalAmount = $amount - ($amount % $walletsCount);
        $source = $this->walletRepository->deductFromWalletBalance($source, $finalAmount);
        $wallets = $this->walletRepository->getPoorestWhitelistedWallets($walletsCount);
        $perWalletAmount = $finalAmount / $walletsCount;
        if ($perWalletAmount === 0) {
            throw new TransactionException('Nie umiem sprawiedliwie rozdać takiej kwoty');
        }
        $transactions = [];
        foreach ($wallets as $wallet) {
            $this->walletRepository->addToWalletBalance($wallet, $perWalletAmount);
            $transactions[] = $this->transactionRepository->createTransaction(
                $source,
                $wallet,
                $perWalletAmount,
                $type,
                $title
            );
        }

        return $transactions;
    }

    /**
     * @param        $source
     * @param        $target
     * @param        $amount
     * @param string $mode
     *
     * @throws TransactionException
     */
    private function validateWallets($source, $target, $amount, $mode = 'transfer'): void
    {
        if (!$source) {
            throw new TransactionException('Nieprawidłowy nadawca!');
        }
        if (!$target) {
            throw new TransactionException('Nieprawidłowy odbiorca!');
        }
        if ($mode === 'transfer' && $source->id === $target->id) {
            throw new TransactionException('To tak nie działa :)');
        }
        if ($source->balance < $amount) {
            throw new TransactionException('Niewystarczające środki na koncie :( Spróbuj `wizard weź poczaruj`');
        }

        if ($source->balance === 1) {
            throw new TransactionException(
                'Transfery wychodzące zablokowane - zbyt niski stan konta. Spróbuj `wizard weź poczaruj`'
            );
        }
    }

    /**
     * @param $amount
     *
     * @throws TransactionException
     */
    public function validateAmount($amount): void
    {
        if (!$amount || $amount < 0 || !is_numeric($amount)
            || str_contains($amount, '.') || str_contains($amount, ',')) {
            \Log::error('INVALID AMOUNT ' . $amount);
            throw new TransactionException('Nieprawidłowa kwota! ' . $amount);
        }
    }

    /**
     * @param Wallet $wallet
     * @param mixed  $amount
     *
     * @throws TransactionException
     */
    private function validateTransfer(Wallet $wallet, $amount): void
    {
        if (!$amount || $amount < 0 || !is_numeric($amount)
            || str_contains($amount, '.') || str_contains($amount, ',')) {
            \Log::error('INVALID AMOUNT ' . $amount);
            throw new TransactionException('Nieprawidłowa kwota!');
        }
        if ($wallet->balance < $amount) {
            throw new TransactionException('Niewystarczające środki na koncie :( Spróbuj `wizard weź poczaruj`');
        }
    }
}
