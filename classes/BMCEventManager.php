<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Carbon\Carbon;
use JZ\BardzoMagicznyCoin\Exceptions\BMCEventException;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Event;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\Wallet;

class BMCEventManager
{
    private $transactionManager;
    private $walletRepository;

    public function __construct(WalletRepository $walletRepository, TransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
        $this->walletRepository = $walletRepository;
    }

    public function triggerCataclysm(Wallet $wallet)
    {
        if ($wallet->name !== 'jakub') {
            throw new BMCEventException('Nie jesteś władca tego świata!');
        }
        $settings = Settings::instance();
        if(!$settings->cataclysm_available){
            return 'Kataklizm jest niedostępny.';
        }
        $wizard = Wallet::where('name', 'wizard')->first();
        $wallets = Wallet::all();
        $result = '# NASTĄPIŁ KATAKLIZM.' . PHP_EOL . PHP_EOL;
        $result .= 'Oto Efekty:' . PHP_EOL;
        foreach ($wallets as $wallet) {
            if($wallet->name === 'wizard'){
                continue;
            }
            $targetBalance = mt_rand(100, 150);
            $currentBalance = $wallet->balance;
            $difference = $targetBalance - $currentBalance;
            if ($wallet->is_blacklisted) {
                $result .= $wallet->name . ' - wciąż na banicji.' . PHP_EOL;
                continue;
            }
            if ($difference > 0) {
                $amount = $difference;
                $this->transactionManager->sendToWallet($wizard, $wallet, $amount, 'cataclysm');
                $result .= $wallet->name . ' - zyskał ' . $difference . ' BMC';
            } elseif($difference < 0) {
                $amount = $difference * -1;
                $this->transactionManager->sendToWallet($wallet, $wizard, $amount, 'cataclysm');
                $result .= $wallet->name . ' - stracił ' . $amount . ' BMC';
            } else {
                $result .= $wallet->name . ' - nie stracił BMC';
            }
            $niggers = $wallet->niggers;
            $chinese = $wallet->chinese;
            if ($niggers > 0) {
                $result .= ', padło mu ' . $niggers . ' murzynów';
            }
            if ($chinese) {
                $result .= ' i ' . $chinese . ' żółtków.';
            }
            $wallet->niggers = 0;
            $wallet->chinese = 0;
            $wallet->slave_setup = [];
            $wallet->chinese_setup = [];
            $wallet->save();
            $result .= PHP_EOL;
        }

        return $result;
    }

    public function checkEvent(Wallet $wallet)
    {
        $today = Carbon::today();
        $event = Event::where('date', $today->toDateString())->first();
        if (!$event) {
            throw new BMCEventException('Dziś nie ma żadnego Bardzo Magicznego Wydarzenia.');
        }

        if ($event->wallets->where('id', $wallet->id)->count() > 0) {
            throw new BMCEventException('Twój portfel już brał udział w dzisiejszym wydarzeniu!');
        }
    }

    public function processEvent(Wallet $wallet)
    {
        $today = Carbon::today();
        $event = Event::where('date', $today->toDateString())->first();
        if (!$event) {
            throw new BMCEventException('Dziś nie ma żadnego Bardzo Magicznego Wydarzenia.');
        }

        if ($event->wallets->where('id', $wallet->id)->count() > 0) {
            throw new BMCEventException('Twój portfel już brał udział w dzisiejszym wydarzeniu!');
        }

        $cabalResponse = $this->getCurrentCabalResponse();
        $ranking = $this->generateRanking($cabalResponse);
        arsort($ranking);
        $totalChans = $this->getTotalChannels($cabalResponse);
        $bestChans = $this->findBestChannelsFor($wallet, $cabalResponse);
        $wizard = $this->walletRepository->getWalletForName('wizard');
        $reward = count($bestChans) * $event->prize;
        $position = $this->getPosition($wallet, $ranking);
        if ($reward > 0) {
            $this->transactionManager->sendToWallet($wizard, $wallet, $reward, 'reward-' . $event->code);
            $event->wallets()->attach($wallet);
            $event->save();
        }
        return $this->generateRewardMessage($bestChans, $totalChans, $position, $reward);
    }

    private function getCurrentCabalResponse(): array
    {
        $settings = Settings::instance();
        return json_decode($settings->cabal_state_json, true);
    }

    private function generateRanking(array $cabalResponse)
    {
        $result = [];
        foreach ($cabalResponse as $channel) {
            $mostActiveShitposter = $channel['Most active shitposter'];
            if (array_key_exists($mostActiveShitposter, $result)) {
                ++$result[$mostActiveShitposter];
            } else {
                $result[$mostActiveShitposter] = 1;
            }
        }
        return $result;
    }

    private function findBestChannelsFor(Wallet $wallet, array $cabalResponse): array
    {
        $result = [];
        foreach ($cabalResponse as $channel) {
            $mostActiveShitposter = $channel['Most active shitposter'];
            if (str_replace('`', '', $mostActiveShitposter) === $wallet->name) {
                $result[] = $channel['Channel'];
            }
        }
        return $result;
    }

    private function getPosition(Wallet $wallet, array $ranking): int
    {
        $keys = array_keys($ranking);
        $position = array_search('jakub', $keys);
        if ($position === false) {
            $position = -1;
        }
        return $position + 1;
    }

    private function getTotalChannels(array $cabalResponse): int
    {
        return count($cabalResponse);
    }

    private function generateRewardMessage(array $bestChans, int $totalChans, int $position, int $reward): string
    {
        $message = '';
        $chansString = $this->generateChansString(count($bestChans));
        if ($position === 1) {
            $message = 'Przejąłeś ' . count($bestChans) . ' ' . $chansString . '? Epicko. Zasłużona nagroda leci na Twoje konto!';
        } else {
            if (count($bestChans) > 1) {
                $message = 'Przejęte ' . count($bestChans) . ' ' . $chansString . ' - to jest solidny wynik. Zasłużona nagroda leci na Twoje konto.';
            }
            if (count($bestChans) === 1) {
                $message = 'No super aktywny na kanale ' . $bestChans[0] . ' haha gratulacje. Poszły jakieś drobne.';
            }
        }
        if (count($bestChans) === 0) {
            $message = 'Nie widzę cię na liście aktywnych shitposterów. Nie ma prezentów dla leniwych kurew :(';
        }

        return $message;
    }

    private function generateChansString(int $amount)
    {
        if ($amount === 1) {
            return 'kanał';
        }
        $lastDigit = substr($amount, -1);
        if (in_array($lastDigit, [2, 3, 4], false) && !in_array($amount, [12, 13, 14])) {
            return 'kanały';
        }

        return 'kanałów';
    }
}
