<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Carbon\Carbon;
use Illuminate\Mail\Transport\Transport;
use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Interfaces\TransactionRepository;
use JZ\BardzoMagicznyCoin\Interfaces\WalletRepository;
use JZ\BardzoMagicznyCoin\Models\Settings;
use JZ\BardzoMagicznyCoin\Models\SlaveTrade;
use JZ\BardzoMagicznyCoin\Models\SlaveTransport;
use JZ\BardzoMagicznyCoin\Models\Transaction;
use JZ\BardzoMagicznyCoin\Models\Wallet;

/**
 *
 */
class SlaveryManager
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var WalletRepository
     */
    private $walletRepository;

    /**
     * @var TransactionManager
     */
    private $transactionManager;
    public const HIRE_CD = 180;
    public const MAX_CHINESE = 50;
    public const MAX_NIGGERS = 50;
    public const GAS_JEW_BONUS_Q = [
        10       => 0.1,
        50       => 0.05,
        100      => 0.025,
        500      => 0.01,
        1000     => 0.005,
        5000     => 0.0025,
        10000    => 0.001,
        50000    => 0.0005,
        100000   => 0.00025,
        500000   => 0.0001,
        1000000  => 0.00005,
        5000000  => 0.000025,
        10000000 => 0.00001,
        15000000 => 0.000005,
        20000000 => 0.0000025
    ];
    public const JEW_REFUND = [
        10       => 1.0,
        50       => 0.9,
        100      => 0.8,
        500      => 0.7,
        1000     => 0.6,
        5000     => 0.5,
        10000    => 0.4,
        50000    => 0.3,
        100000   => 0.2,
        500000   => 0.1,
        1000000  => 0.05,
        5000000  => 0.025,
        10000000 => 0.01,
        15000000 => 0.005,
        20000000 => 0.0025
    ];

    public const NIGGER_WORK = 0;
    public const NIGGERS_DEFEND = 1;
    public const NIGGER_ATTACK = 2;
    public const NIGGERS_MODE_CHANGE_COST = 5;
    public const CHINESE_MODE_CHANGE_COST = 3;
    public const CHINESE_WORK = 0;
    public const CHINESE_MAINTAIN = 1;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        WalletRepository      $walletRepository,
        TransactionManager    $transactionManager
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->walletRepository = $walletRepository;
        $this->transactionManager = $transactionManager;
    }

    public function setNiggerMode(Wallet $wallet, $mode)
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if ($wallet->balance < self::NIGGERS_MODE_CHANGE_COST) {
            throw new \Exception('Nie stać cię, aby zmienić tryb murzyna');
        }
        if ($mode === 'work') {
            $wallet->niggers_mode = self::NIGGER_WORK;
        } else {
            if ($mode === 'attack') {
                $wallet->niggers_mode = self::NIGGER_ATTACK;
            } else {
                if ($mode === 'defend') {
                    $wallet->niggers_mode = self::NIGGERS_DEFEND;
                } else {
                    throw new \Exception('Nieprawidłowy tryb murzyna');
                }
            }
        }
        $wallet->slave_setup = null;
        $wizard = $this->walletRepository->getWalletForName('wizard');
        $this->transactionManager->sendToWallet(
            $wallet,
            $wizard,
            self::NIGGERS_MODE_CHANGE_COST,
            'transfer',
            'Nigger Mode Change'
        );
        $wallet->save();
        return $wallet;
    }

    public function setNiggerSetup(Wallet $wallet, int $work, int $attack, int $defend)
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if ($wallet->balance < self::NIGGERS_MODE_CHANGE_COST) {
            throw new \Exception('Nie stać cię, aby zmienić tryb murzyna');
        }
        $setup = [
            ['mode' => self::NIGGER_WORK, 'amount' => $work],
            ['mode' => self::NIGGER_ATTACK, 'amount' => $attack],
            ['mode' => self::NIGGERS_DEFEND, 'amount' => $defend]
        ];
        $wallet->slave_setup = $setup;
        $wizard = $this->walletRepository->getWalletForName('wizard');
        $this->transactionManager->sendToWallet(
            $wallet,
            $wizard,
            self::NIGGERS_MODE_CHANGE_COST,
            'transfer',
            'Nigger Mode Change'
        );
        $wallet->save();
        return $wallet;
    }

    public function setChinkSetup(Wallet $wallet, int $work, int $maintenance)
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if ($wallet->balance < self::CHINESE_MODE_CHANGE_COST) {
            throw new \Exception('Nie stać cię, aby zmienić tryb żółtków');
        }
        if ($work + $maintenance > $wallet->chinese) {
            $wallet->syncChinkSetup();
            $wallet->save();
            throw new \Exception('Wrong setup');
        }
        $setup = [
            ['mode' => self::CHINESE_WORK, 'amount' => $work],
            ['mode' => self::CHINESE_MAINTAIN, 'amount' => $maintenance],
        ];
        $wallet->chinese_setup = $setup;
        $wizard = $this->walletRepository->getWalletForName('wizard');
        $this->transactionManager->sendToWallet(
            $wallet,
            $wizard,
            self::CHINESE_MODE_CHANGE_COST,
            'transfer',
            'Chink Mode Change'
        );
        $wallet->save();
        return $wallet;
    }


    /**
     * @param Wallet $wallet
     * @param int    $price
     *
     * @return int
     * @throws TransactionException
     */
    public function hireNigger(Wallet $wallet, int $price, int $amount): SlaveTransport
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        $this->validateHiredAt($wallet);

        $wizardWallet = Wallet::where('name', 'wizard')->first();
        if ($this->countNiggers() >= $this->getMaxNiggers()) {
            throw new TransactionException(
                'Limit importu murzynów wyczerpał się. Skorzystaj z `wizard slave-trades` :kotel_euiv: '
            );
        }
        $this->walletRepository->deductFromWalletBalance($wallet, $price);
        $this->walletRepository->addToWalletBalance($wizardWallet, $price);
        $this->transactionRepository->createTransaction($wallet, $wizardWallet, $price);
        $transport = new SlaveTransport();
        $transport->wallet_id = $wallet->id;
        $transport->amount = $amount;
        $transport->type = 'niggers';
        $transport->save();
        return $transport;
    }

    /**
     * @param Wallet $wallet
     * @param int    $price
     *
     * @return SlaveTransport
     * @throws TransactionException
     */
    public function hireChink(Wallet $wallet, int $price, int $amount): SlaveTransport
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        $this->validateHiredAt($wallet);
        $wizardWallet = Wallet::where('name', 'wizard')->first();
        if ($this->countChinks() >= $this->getMaxChinks()) {
            throw new TransactionException(
                'Limit importu żółtków wyczerpał się. Skorzystaj z `wizard slave-trades` :chink: '
            );
        }
        $this->walletRepository->deductFromWalletBalance($wallet, $price);
        $this->walletRepository->addToWalletBalance($wizardWallet, $price);
        $this->transactionRepository->createTransaction($wallet, $wizardWallet, $price);

        $transport = new SlaveTransport();
        $transport->wallet_id = $wallet->id;
        $transport->amount = $amount;
        $transport->type = 'chinese';
        $transport->save();
        return $transport;
    }


    /**
     * @param Wallet $wallet
     *
     * @return int
     * @throws TransactionException
     */
    public function killNigger(Wallet $wallet, bool $unhide = true): int
    {
        if ($wallet->is_hidden && $unhide) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if ($wallet->niggers >= 1) {
            --$wallet->niggers;
            $this->adjustSlaveSetup($wallet, 1, 'remove');
            $wallet->save();
            return $wallet->niggers;
        }
        throw new TransactionException('Nie posiadasz murzynów :kotel_euiv: ');
    }

    /**
     * @param Wallet $wallet
     *
     * @return int
     * @throws TransactionException
     */
    public function killChink(Wallet $wallet, bool $unhide = true): int
    {
        if ($wallet->is_hidden && $unhide) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if ($wallet->chinese >= 1) {
            --$wallet->chinese;
            $this->adjustChinkSetup($wallet, 1, 'remove');
            $wallet->save();
            return $wallet->chinese;
        }
        throw new TransactionException('Nie posiadasz żółtków :kotel_euiv: ');
    }

    /**
     * @param Wallet      $wallet
     * @param int         $amount
     * @param int         $price
     * @param Wallet|null $buyer
     *
     * @return SlaveTrade
     * @throws TransactionException
     */
    public function sellNigger(Wallet $wallet, int $amount, int $price, Wallet $buyer = null): SlaveTrade
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if (!$wallet->niggers || $wallet->niggers < $amount) {
            throw new TransactionException('Nie posiadasz wystarczająco dużo murzynów :kotel_euiv: ');
        }
        if ($amount <= 0 || $price < 0) {
            throw new TransactionException('Nie da rady.');
        }
        $offer = new SlaveTrade();
        $offer->seller_id = $wallet->id;
        $offer->amount = $amount;
        $offer->price = $price;
        $offer->type = 'nigger';
        if ($buyer) {
            $offer->buyer_id = $buyer->id;
        }
        $offer->save();
        $wallet->niggers -= $amount;
        $this->adjustSlaveSetup($wallet, $amount, 'remove');
        $wallet->save();
        return $offer;
    }

    /**
     * @param Wallet      $wallet
     * @param int         $amount
     * @param int         $price
     * @param Wallet|null $buyer
     *
     * @return SlaveTrade
     * @throws TransactionException
     */
    public function sellChink(Wallet $wallet, int $amount, int $price, Wallet $buyer = null): SlaveTrade
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if (!$wallet->chinese || $wallet->chinese < $amount) {
            throw new TransactionException('Nie posiadasz wystarczająco dużo żółtków :kotel_euiv: ');
        }
        if ($amount <= 0 || $price < 0) {
            throw new TransactionException('Nie da rady.');
        }
        $offer = new SlaveTrade();
        $offer->seller_id = $wallet->id;
        $offer->amount = $amount;
        $offer->price = $price;
        $offer->type = 'chinese';
        if ($buyer) {
            $offer->buyer_id = $buyer->id;
        }
        $offer->save();
        $wallet->chinese -= $amount;
        $this->adjustChinkSetup($wallet, $amount, 'remove');
        $wallet->save();
        return $offer;
    }

    /**
     * @param Wallet $wallet
     * @param int    $offerId
     *
     * @throws TransactionException
     * @throws \Exception
     */
    public function cancelSellSlave(Wallet $wallet, int $offerId, string $type = 'nigger')
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        /** @var SlaveTrade $offer */
        $offer = SlaveTrade::where('id', $offerId)->first();
        if (!$offer) {
            throw new TransactionException('Oferta nie istnieje :kotel_euiv: ');
        }
        if ($offer->is_closed) {
            throw new TransactionException('Ta oferta jest sfinalizowana :kotel_euiv: ');
        }
        if ($offer->seller_id !== $wallet->id) {
            throw new TransactionException('Możesz anulować tylko własne oferty :kotel_euiv: ');
        }
        if ($type === 'nigger') {
            $wallet->niggers += $offer->amount;
            $this->adjustSlaveSetup($wallet, $offer->amount, 'add');
        }
        if ($type === 'chinese') {
            $wallet->chinese += $offer->amount;
            $this->adjustChinkSetup($wallet, $offer->amount, 'add');
        }

        $wallet->save();
        $offer->delete();
    }

    /**
     * @param Wallet $wallet
     * @param int    $offerId
     *
     * @return SlaveTrade
     * @throws TransactionException
     */
    public function buySlave(Wallet $wallet, int $offerId, string $type = 'nigger'): SlaveTrade
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        /** @var SlaveTrade $offer */
        $offer = SlaveTrade::where('id', $offerId)->where('is_closed', false)->where('type', $type)->first();
        if (!$offer) {
            throw new TransactionException('Oferta nie istnieje');
        }
        if ($wallet->balance < $offer->price) {
            throw new TransactionException('Nie stać cię :kotel_euiv: . Potrzebne '
                                           . $offer->price . ' :bmc: , posiadasz ' . $wallet->balance . '');
        }
        if ($offer->buyer_id && $offer->buyer_id !== $wallet->id) {
            throw new TransactionException('Ta oferta jest zarezerwowana dla @' . $offer->buyer->name);
        }
        $this->walletRepository->deductFromWalletBalance($wallet, $offer->price);
        $this->walletRepository->addToWalletBalance($offer->seller, $offer->price);
        $this->transactionRepository->createTransaction(
            $wallet,
            $offer->seller,
            $offer->price,
            'transfer',
            'Slave Trade - Oferta nr. ' . $offer->id
        );
        if ($type === 'nigger') {
            if (!$wallet->niggers) {
                $wallet->niggers = $offer->amount;
            } else {
                $wallet->niggers += $offer->amount;
            }
            $this->adjustSlaveSetup($wallet, $offer->amount, 'add');
        }
        if ($type === 'chinese') {
            if (!$wallet->chinese) {
                $wallet->chinese = $offer->amount;
            } else {
                $wallet->chinese += $offer->amount;
            }
            $this->adjustChinkSetup($wallet, $offer->amount, 'add');
        }
        $wallet->save();
        $offer->buyer_id = $wallet->id;
        $offer->is_closed = true;
        $offer->save();

        return $offer;
    }


    /**
     * @param Wallet $wallet
     * @param int    $price
     *
     * @throws TransactionException
     */
    public function hireJew(Wallet $wallet, int $price)
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if (BMCGameEvents::areJewsDead()) {
            throw new TransactionException('Wszyscy żydzi nie żyją! :fireworks:');
        }
        if ($wallet->has_jew) {
            throw new TransactionException('Już posiadasz żyda!');
        }
        $wizardWallet = Wallet::where('name', 'wizard')->first();
        $this->walletRepository->deductFromWalletBalance($wallet, $price);
        $this->walletRepository->addToWalletBalance($wizardWallet, $price);
        $this->transactionRepository->createTransaction($wallet, $wizardWallet, $price);
        $wallet->has_jew = true;
        $wallet->save();
    }

    /**
     * @param Wallet $wallet
     * @param int    $price
     *
     * @throws TransactionException
     */
    public function gasJew(Wallet $wallet, int $price, int $amount = 1, bool $validateJew = true): Transaction
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if (BMCGameEvents::areJewsDead()) {
            throw new TransactionException('Wszyscy żydzi nie żyją! :fireworks:');
        }
        if (!$wallet->has_jew && $validateJew) {
            throw new TransactionException('Nie posiadasz żyda!');
        }
        /** @var Wallet $wizardWallet */
        $wizardWallet = Wallet::where('name', 'wizard')->first();
        $returnPrice = $this->calculateJewReturnPrice($wallet);
        $this->walletRepository->deductFromWalletBalance($wizardWallet, $returnPrice);
        $this->walletRepository->addToWalletBalance($wallet, $returnPrice);
        $transaction = $this->transactionRepository->createTransaction($wizardWallet, $wallet, $returnPrice);
        $wallet->has_jew = false;
        if ($wallet->gassed_jews == null) {
            $wallet->gassed_jews = $amount;
        } else {
            $wallet->gassed_jews += $amount;
        }
        $wallet->save();

        return $transaction;
    }

    /**
     * @param Wallet $wallet
     */
    public function gainFromNiggers(Wallet $wallet): int
    {
        $wizardWallet = Wallet::where('name', 'wizard')->first();
        $settings = Settings::instance();
        $niggerIncome = $settings->get('nigger_income', 5);
        $income = $niggerIncome * $wallet->getWorkPower();
        $this->walletRepository->deductFromWalletBalance($wizardWallet, $income);
        $this->walletRepository->addToWalletBalance($wallet, $income);
        $this->transactionRepository->createTransaction(
            $wizardWallet,
            $wallet,
            $income,
            'slavery',
            'Income z niewolników'
        );
        return $income;
    }

    /**
     * @param Wallet $wallet
     */
    public function gainFromJew(Wallet $wallet): int
    {
        if (BMCGameEvents::areJewsDead()) {
            throw new TransactionException('Wszyscy żydzi nie żyją! :fireworks:');
        }
        $wizardWallet = Wallet::where('name', 'wizard')->first();

        $jewIncome = $this->calculateJewIncome($wallet);
        $income = floor($wallet->balance * ($jewIncome / 100));
        $this->walletRepository->deductFromWalletBalance($wizardWallet, $income);
        $this->walletRepository->addToWalletBalance($wallet, $income);
        $this->transactionRepository->createTransaction($wizardWallet, $wallet, $income, 'slavery', 'Income z żyda');

        return $income;
    }

    /**
     * @return int
     */
    public function countNiggers(): int
    {
        $walletNiggers = Wallet::where('is_blacklisted', false)->where('niggers', '>', 0)->sum('niggers');
        $offerNiggers = SlaveTrade::where('is_closed', false)->sum('amount');
        $transportNiggers = SlaveTransport::where('type', 'niggers')->sum('amount');

        return $walletNiggers + $offerNiggers + $transportNiggers;
    }

    /**
     * @param Wallet $wallet
     *
     * @return string
     */
    public function getSlaveStatus(Wallet $wallet): string
    {
        // todo move to SlaveryManager
        $settings = Settings::instance();
        $niggerIncome = $settings->get('nigger_income', 5);
        $jewIncome = $settings->get('jew_income', 2);
        // todo decide?
        $additionalJewIncome = $this->calculateJewIncome($wallet);
        $jewIncome += $additionalJewIncome;
        $jewPrice = $settings->get('jew_price', 1000);
        $niggerPrice = $settings->get('nigger_price', 200);
        $chinkPrice = $settings->get('chink_price', 200);
        $message = 'Posiadasz ' . $wallet->niggers . ' murzynów :kotel_euiv: oraz ' . ($wallet->chinese ?? 0)
                   . ' żółtków, dostarczających ci codziennie '
                   . $niggerIncome * $wallet->getWorkPower() . ' :bmc:' . PHP_EOL;
        $message .= 'Twój setup to: '
                    . $wallet->getAttackPower() . ' AP, '
                    . $wallet->getDefence()
                    . ' DEF, ' . $wallet->getWorkPower() . ' WP, ' . $wallet->getMaintenancePower() . ' MP' . PHP_EOL;

        if ($wallet->gassed_jews) {
            $message .= 'Zgazowałeś ' . $wallet->gassed_jews . ', jesteś bohaterem!' . PHP_EOL;
        }
        if ($wallet->has_jew) {
            $message .= 'Posiadasz też żyda, który codziennie zwiększa twój majątek o '
                        . $jewIncome . '%, ale sprawia że przegrywasz.' . PHP_EOL;
        } else {
            $message .= 'Nie posiadasz żyda.' . PHP_EOL;
        }
        $totalNigs = $this->countNiggers();
        $totalChinks = $this->countChinks();
        $maxChinks = $this->getMaxChinks();
        $maxNigs = $this->getMaxNiggers();

        $message .= PHP_EOL . 'Aktualne ceny:' . PHP_EOL
                    . 'Murzyn: ' . $niggerPrice . ':bmc: (' . $niggerIncome . '+ :bmc: /day) - Dostępnych: '
                    . ($maxNigs - $totalNigs) . PHP_EOL;

        $message .= 'Żółtek: ' . $chinkPrice . ':bmc: (' . $niggerIncome * 2 . '+ :bmc: /day) - Dostępnych: '
                    . ($maxChinks - $totalChinks) . PHP_EOL;
        if (!BMCGameEvents::areJewsDead()) {
            $message .= 'Żyd: ' . $jewPrice . ' :bmc: (' . $jewIncome . '%+ :bmc: /day)';
        }

        return $message;
    }

    private function getMaxNiggers()
    {
        $settings = Settings::instance();
        return $settings->get('max_niggers', self::MAX_NIGGERS);
    }

    public function adjustSlaveSetup(Wallet $wallet, int $amount, string $mode): Wallet
    {
        if (!$wallet->slave_setup) {
            $wallet->syncSlaveSetup();
        }
        $slaveSetup = $wallet->slave_setup;
        $foundWorkers = false;
        if ($mode === 'add') {
            foreach ($slaveSetup as $key => $entry) {
                if ($entry['mode'] === self::NIGGER_WORK) {
                    $foundWorkers = true;
                    $slaveSetup[$key]['amount'] = $entry['amount'] + $amount;
                }
            }
            if (!$foundWorkers) {
                $slaveSetup[] = [
                    'mode'   => self::NIGGER_WORK,
                    'amount' => $amount
                ];
            }
        }
        if ($mode === 'remove') {
            foreach ($slaveSetup as $key => $entry) {
                if ($amount > 0) {
                    if ($entry['amount'] >= $amount) {
                        $slaveSetup[$key]['amount'] = $entry['amount'] - $amount;
                        $amount = 0;
                    } else {
                        $slaveSetup[$key]['amount'] = 0;
                        $amount -= $entry['amount'];
                    }
                }
            }
        }
        $wallet->slave_setup = $slaveSetup;
        $wallet->save();
        return $wallet;
    }

    private function calculateJewIncome(Wallet $wallet)
    {
        return $wallet->calculateJewIncome();
    }

    private function calculateJewReturnPrice(Wallet $wallet)
    {
        return $wallet->calculateJewReturnPrice();
    }

    public function countChinks()
    {
        $walletChinks = Wallet::where('is_blacklisted', false)->where('chinese', '>', 0)->sum('chinese');
        $offerChinks = SlaveTrade::where('is_closed', false)->sum('amount');
        $transportChinks = SlaveTransport::where('type', 'chinese')->sum('amount');
        return $walletChinks + $offerChinks + $transportChinks;
    }

    public function getMaxChinks()
    {
        $settings = Settings::instance();
        return $settings->get('max_chinese', self::MAX_CHINESE);
    }


    public function adjustChinkSetup(Wallet $wallet, int $amount, string $mode)
    {
        if (!$wallet->chinese_setup) {
            $wallet->syncChinkSetup();
        }
        $slaveSetup = $wallet->chinese_setup;
        $foundWorkers = false;
        if ($mode === 'add') {
            foreach ($slaveSetup as $key => $entry) {
                if ($entry['mode'] === self::NIGGER_WORK) {
                    $foundWorkers = true;
                    $slaveSetup[$key]['amount'] = $entry['amount'] + $amount;
                }
            }
            if (!$foundWorkers) {
                $slaveSetup[] = [
                    'mode'   => self::NIGGER_WORK,
                    'amount' => $amount
                ];
            }
        }
        if ($mode === 'remove') {
            foreach ($slaveSetup as $key => $entry) {
                if ($amount > 0) {
                    if ($entry['amount'] >= $amount) {
                        $slaveSetup[$key]['amount'] = $entry['amount'] - $amount;
                        $amount = 0;
                    } else {
                        $slaveSetup[$key]['amount'] = 0;
                        $amount -= $entry['amount'];
                    }
                }
            }
        }
        $wallet->chinese_setup = $slaveSetup;
        $wallet->save();
        return $wallet;
    }

    public function gainFromBuildings(Wallet $wallet)
    {
        $wizardWallet = Wallet::where('name', 'wizard')->first();
        $income = 0;
        foreach ($wallet->buildings as $building) {
            if ($wallet->isBuildingActive($building)) {
                $income += $building->income;
            }
        }
        if ($income > 0) {
            $wallet->balance += $income;
            $this->transactionRepository->createTransaction(
                $wizardWallet,
                $wallet,
                $income,
                'slavery',
                'Income z budynków'
            );
        }
        if ($income < 0) {
            $income = abs($income);
            $wallet->balance -= $income;
            $this->transactionRepository->createTransaction(
                $wallet,
                $wizardWallet,
                $income,
                'slavery',
                'Koszta budynków'
            );
        }
        return $income;
    }

    private function convertToHoursMins(int $minutes): string
    {
        $hours = intdiv($minutes, 60);
        $minutes = $minutes % 60;

        // Return as H:i format
        return sprintf('%02dh %02dmin', $hours, $minutes);
    }

    private function validateHiredAt(Wallet $wallet)
    {
        $this->validateWalletHiredAt($wallet, 'self');
        if ($wallet->parent) {
            $this->validateWalletHiredAt($wallet->parent, 'bot');
        }
        foreach ($wallet->subaccounts as $child) {
            $this->validateWalletHiredAt($child, 'main');
        }
    }

    private function validateWalletHiredAt(Wallet $wallet, string $type)
    {
        $hiredAt = $wallet->hired_at;
        if ($hiredAt) {
            $diff = self::HIRE_CD - $hiredAt->diffInMinutes(Carbon::now());
            if ($diff > 0) {
                $message = 'Masz aktywną dostawę w drodze. Jeszcze ';
                if ($type === 'bot') {
                    $message = 'Twoje główne konto ma dostawę w drodze. Jeszcze ';
                }
                if ($type === 'main') {
                    $message = 'Twoje subkonto ma dostawę w drodze. Jeszcze ';
                }
                throw new \Exception($message . $this->convertToHoursMins($diff));
            }
        }
    }
}
