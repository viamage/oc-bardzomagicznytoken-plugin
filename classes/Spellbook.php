<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use Carbon\Carbon;
use JZ\BardzoMagicznyCoin\Exceptions\SpellException;
use JZ\BardzoMagicznyCoin\Models\MagicAction;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use Random\RandomException;

/**
 *
 */
class Spellbook
{
    /**
     *
     */
    public const SPELLS_TIMEOUT = 3;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @param TransactionManager $transactionManager
     */
    public function __construct(TransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
    }
    private const REWARD_1 = 5;
    private const REWARD_2 = 10;
    private const REWARD_3 = 20;
    /**
     * @param Wallet $wallet
     *
     * @return string
     * @throws \Exception
     */
    public function castSpell(Wallet $wallet)
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        $spell = random_int(1, 10);
        $boost = random_int(1, 100) > 90;
        $message = 'Klątwa już leci' . $wallet->name . '! :sigilize:';
        $wizard = $this->transactionManager->walletRepository->getWalletForName('wizard');
        if ($wizard->balance < 0) {
            $wizard->balance += 1000000000000;
            $wizard->save();
            return 'Wyczarowałem sobie 1kkkk BMC! :sigilize:';
        }
        switch ($spell) {
            case 1:
                $message = $this->coinsToSelf($wallet, 5, $boost);
                break;
            case 2:
                $message = $this->coinsToSelf($wallet, 10, $boost);
                break;
            case 3:
            case 4:
            case 5:
                $message = $this->coinsToAll(3, $wallet->name, $boost);
                break;
            case 6:
                $message = $this->coinsToSelf($wallet, 20, $boost);
                break;
            case 7:
                $message = $this->coinsToAll(5, $wallet->name, $boost);
                break;
            case 8:
                $message = $this->coinsLost($wallet, 3);
                break;
            case 9:
                $message = $this->coinsLost($wallet, 1);
                break;
            case 10:
                $message = $this->coinsToSelf($wallet, 15, $boost);
                break;
        }

        $magicAction = new MagicAction();
        $magicAction->wallet_id = $wallet->id;
        $magicAction->created_at = Carbon::now();
        $magicAction->save();
        return $message;
    }

    /**
     * @param Wallet $wallet
     * @param int    $coins
     *
     * @return string
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     * @throws RandomException
     */
    public function coinsToSelf(Wallet $wallet, int $coins, bool $boost = false): string
    {
        if ($boost) {
            $coins += random_int(100, 300);
        }
        \Log::info('Czaruje coiny dla ' . $wallet->name . ' ' . $coins);
        $source = $this->transactionManager->walletRepository->getWalletForName('wizard');
        $this->transactionManager->sendToWallet($source, $wallet, $coins, 'magic');

        return 'Wyczarowałem ci ' . $coins . ' ' . CoinLabelHelper::generate($coins) . ' ' . $wallet->name . '!';
    }

    /**
     * @param int    $coins
     * @param string $spellcaster
     *
     * @return string
     */
    public function coinsToAll(int $coins, string $spellcaster = '', bool $boost = false): string
    {
        if ($boost) {
            $coins += random_int(100, 300);
        }
        \Log::info('Czaruje coiny dla all ' . $coins);
        $source = $this->transactionManager->walletRepository->getWalletForName('wizard');
        $this->transactionManager->sendToAllWallets($source, $coins, 'magic', null, false);

        return 'Wyczarowałem wszystkim ' . $coins . ' ' . CoinLabelHelper::generate($coins) . ' ' . $spellcaster . '!';
    }

    /**
     * @param Wallet $source
     * @param int    $coins
     *
     * @return string
     * @throws \JZ\BardzoMagicznyCoin\Exceptions\TransactionException
     */
    public function coinsLost(Wallet $source, int $coins): string
    {
        \Log::info('Czaruje coiny lost ' . $source->name . ' ' . $coins);
        $target = $this->transactionManager->walletRepository->getWalletForName('wizard');
        $this->transactionManager->sendToWallet($source, $target, $coins, 'magic');

        return 'Wyczarowałem, że tracisz ' . $coins . ' '
               . CoinLabelHelper::generate($coins) . ' ' . $source->name . '!';
    }

    /**
     * @param Wallet $wallet
     *
     * @throws SpellException
     */
    public function checkSpellCD(Wallet $wallet): void
    {
        $limitDate = (Carbon::now())->subHours(self::SPELLS_TIMEOUT);
        if ($magicAction = MagicAction::where('wallet_id', $wallet->id)
                                      ->where('created_at', '>', $limitDate)->first()) {
            $minutesRemaining = (self::SPELLS_TIMEOUT * 60) - (Carbon::now())->diffInMinutes($magicAction->created_at);
            $hours = floor($minutesRemaining / 60);
            $minutes = $minutesRemaining % 60;
            if (strlen($minutes) === 1) {
                $minutes = '0' . $minutes;
            }
            throw new SpellException('Możesz tylko raz na '
                                     . self::SPELLS_TIMEOUT
                                     . ' godziny! Zostało ' . $hours . 'h ' . $minutes . 'min...');
        }
    }

    public function checkIfMagician(?Wallet $wallet)
    {
        if (!$wallet) {
            throw new SpellException('Nie masz portfela! Użyj `wizard create wallet ' . request()->get('name') . '`');
        }
        if ($wallet->is_bot) {
            throw new SpellException('Boty nie umieją czarować wcale.');
        }
    }
}
