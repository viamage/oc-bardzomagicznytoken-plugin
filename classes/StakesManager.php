<?php

namespace JZ\BardzoMagicznyCoin\Classes;

use JZ\BardzoMagicznyCoin\Exceptions\TransactionException;
use JZ\BardzoMagicznyCoin\Models\PlayerStake;
use JZ\BardzoMagicznyCoin\Models\Stake;
use JZ\BardzoMagicznyCoin\Models\Wallet;
use JZ\BardzoMagicznyCoin\ValueObjects\StakeResults;

/**
 * Stakes are bets that players can create and other players can vote for one of their option.
 */
class StakesManager
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @param TransactionManager $transactionManager
     */
    public function __construct(TransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
    }

    /**
     * @param Wallet $wallet
     * @param string $label
     * @param array  $options
     *
     * @return Stake
     * @throws TransactionException
     */
    public function createStake(Wallet $wallet, string $label, array $options): Stake
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        $exists = Stake::where('is_open', true)->where('wallet_id', $wallet->id)->count();
        if ($exists > 0) {
            throw new TransactionException('There is already an open stake for this wallet');
        }
        $stake = new Stake();
        $stake->label = $label;
        $stake->options = $options;
        $stake->is_open = true;
        $stake->is_finished = false;
        $stake->wallet_id = $wallet->id;
        $stake->save();

        return $stake;
    }

    /**
     * @param Stake $stake
     */
    public function closeStake(Stake $stake): void
    {
        if ($stake->wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($stake->wallet);
        }
        $stake->is_open = false;
        $stake->save();
    }

    /**
     * @param Stake $stake
     */
    public function openStake(Stake $stake): void
    {
        if ($stake->wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($stake->wallet);
        }
        $stake->is_open = true;
        $stake->save();
    }


    /**
     * @param Stake $stake
     * @param int   $winOption
     *
     * @return int
     * @throws TransactionException
     */
    public function finishStake(Stake $stake, int $winOption): int
    {
        if ($stake->wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($stake->wallet);
        }
        $stake->is_open = false;
        $wonCount = 0;
        $pool = 0;
        $winners = [];
        foreach ($stake->playerStakes as $playerStake) {
            $option = (int)$playerStake->option;
            if ($option === $winOption) {
                $winners[] = $playerStake;
                ++$wonCount;
            } else {
                $pool += $playerStake->amount;
            }
        }
        $this->distributeWinnings($stake, $pool, $winners);
        $stake->win_option = $winOption;
        $stake->is_finished = true;
        $stake->save();
        return $wonCount;
    }

    public function countWinners(Stake $stake): StakeResults
    {
        $results = new StakeResults();
        if ($stake->is_finished && $stake->win_option) {
            foreach ($stake->playerStakes as $playerStake) {
                $option = (int)$playerStake->option;
                if ($option === $stake->win_option) {
                    $winners[] = $playerStake;
                    ++$results->winners;
                } else {
                    ++$results->losers;
                    $results->pool += $playerStake->amount;
                }
            }
        }
        return $results;
    }

    /**
     * Method to distribute pool to winning players, proportionally to their stakes
     *
     * @param Stake         $stake
     * @param int           $pool
     * @param PlayerStake[] $winners
     *
     * @return void
     * @throws TransactionException
     */
    public function distributeWinnings(Stake $stake, int $pool, array $winners): void
    {
        $wizard = Wallet::where('name', 'wizard')->firstOrFail();
        $totalBet = 0;
        foreach ($winners as $winner) {
            $totalBet += $winner->amount;
        }
        foreach ($winners as $winner) {
            $percentage = $winner->amount / $totalBet;
            $winner->win_amount = (int)($winner->amount + $pool * $percentage);
            $winner->save();
            $this->transactionManager->sendToWallet(
                $wizard,
                $winner->wallet,
                $winner->win_amount,
                'stake',
                'Stake ' . $stake->id . ' win'
            );
        }
    }

    /**
     * @param Wallet $wallet
     * @param Stake  $stake
     * @param int    $option
     * @param int    $amount
     *
     * @return PlayerStake
     * @throws TransactionException
     */
    public function voteForStake(Wallet $wallet, Stake $stake, int $option, int $amount): PlayerStake
    {
        if ($wallet->is_hidden) {
            BMCGameEvents::hiddenStatusRemoved($wallet);
        }
        if ($stake->is_finished || !$stake->is_open) {
            throw new TransactionException('Voting for this stake not possible right now.');
        }
        if ($wallet->balance < $amount) {
            throw new TransactionException('Not enough funds');
        }
        if ($wallet->is_bot) {
            throw new TransactionException('Bots cannot play');
        }
        $wizard = Wallet::where('name', 'wizard')->firstOrFail();
        $this->transactionManager->sendToWallet($wallet, $wizard, $amount, 'stake', $stake->id . ' ' . $option);

        $playerStake = new PlayerStake();
        $playerStake->wallet_id = $wallet->id;
        $playerStake->stake_id = $stake->id;
        $playerStake->option = $option;
        $playerStake->amount = $amount;
        $playerStake->save();

        return $playerStake;
    }

    /**
     * @param Stake $stake
     * @param bool  $labels
     *
     * @return array
     */
    public function calculateStakeRates(Stake $stake, bool $labels = true): array
    {
        $totalAmountForEachOption = [];
        foreach ($stake->options as $option) {
            if ($labels) {
                $optionId = $option['label'] . ' (' . $option['id'] . ')';
            } else {
                $optionId = $option['id'];
            }
            $totalAmountForEachOption[$optionId] = $stake->playerStakes()
                                                         ->where('option', $option['id'])
                                                         ->sum('amount');
        }
        return $totalAmountForEachOption;
    }
}
