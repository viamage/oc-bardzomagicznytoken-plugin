<?php

namespace JZ\BardzoMagicznyCoin\Classes;

class CoinLabelHelper
{
    public static function generate(int $amount): string
    {
        if ($amount === 1) {
            return 'Bardzo Magiczny Coin';
        }
        $lastDigit = substr($amount, -1);
        $lastDigits = substr($amount, -2);
        if (in_array($lastDigit, [2, 3, 4], false) && !in_array($lastDigits, [12, 13, 14], false)) {
            return 'Bardzo Magiczne Coiny';
        }

        return 'Bardzo Magicznych Coinów';
    }
}
